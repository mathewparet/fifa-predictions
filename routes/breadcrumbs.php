<?php

Breadcrumbs::for('home', function ($trail) {
    $trail->push('Home', route('welcome'));
});

Breadcrumbs::for('rules', function ($trail) {
    $trail->parent('home');
    $trail->push('Rules for the Game', route('rules'));
});

Breadcrumbs::for('register', function ($trail) {
	$trail->parent('home');
    $trail->push('Create a new Account', route('register'));
});

Breadcrumbs::for('feedback.index', function ($trail) {
    $trail->parent('home');
    $trail->push('What people said', route('feedback.index'));
});

Breadcrumbs::for('login', function ($trail) {
	$trail->parent('home');
    $trail->push('Login', route('login'));
});

Breadcrumbs::for('password', function ($trail) {
	$trail->parent('home');
    $trail->push('Request Password Reset', route('password.request'));
});

Breadcrumbs::for('reset', function ($trail) {
	$trail->parent('home');
    $trail->push('Reet Password', route('password.reset'));
});

Breadcrumbs::for('dashboard', function ($trail) {
	$trail->parent('home');
    $trail->push('Dashboard', route('home'));
});

Breadcrumbs::for('leaderboard', function ($trail) {
    $trail->parent('home');
    $trail->push('Leaderboard', route('leaderboard'));
});

Breadcrumbs::for('users.index', function ($trail) {
	$trail->parent('home');
    if(auth()->user()->can('user_view'))
        $trail->push('Users', route('users.index'));
    else
        $trail->push('Users');
});

Breadcrumbs::for('users.stats', function($trail) {
    $trail->parent('users.index');
    $trail->push('Statistics', route('users.stats'));
});

Breadcrumbs::for('users.show', function ($trail, $user) {
	$trail->parent('users.index');
    $trail->push($user->name, route('users.show',['user'=>$user->id]));
});

Breadcrumbs::for('users.edit', function ($trail, $user) {
	$trail->parent('users.show',$user);
    $trail->push('Update', route('users.edit',['user'=>$user->id]));
});

Breadcrumbs::for('users.points.index', function ($trail, $user) {
    $trail->parent('users.show',$user);
    $trail->push('Points', route('users.points.index',['user'=>$user->id]));
});

Breadcrumbs::for('users.points.buy', function ($trail, $user) {
    $trail->parent('users.points.index',$user);
    $trail->push('Get More', route('users.points.buy.form',['user'=>$user->id]));
});

Breadcrumbs::for('roles.index', function ($trail) {
	$trail->parent('home');
    $trail->push('Roles', route('roles.index'));
});

Breadcrumbs::for('roles.show', function ($trail, $role) {
	$trail->parent('roles.index');
    $trail->push($role->name, route('roles.show',['role'=>$role->name]));
});

Breadcrumbs::for('roles.edit', function ($trail, $role) {
	$trail->parent('roles.show',$role);
    $trail->push('Update', route('roles.edit',['role'=>$role->name]));
});

Breadcrumbs::for('roles.create', function ($trail) {
	$trail->parent('roles.index');
    $trail->push('Create new Role', route('roles.create'));
});

Breadcrumbs::for('invitations.index', function($trail) {
    $trail->parent('home');
    $trail->push('Invitations', route('invitations.index'));
});

Breadcrumbs::for('invitations.create', function($trail) {
    $trail->parent('invitations.index');
    $trail->push('Invite Someone', route('invitations.create'));
});

Breadcrumbs::for('teams.index', function($trail) {
    $trail->parent('home');
    $trail->push('Teams', route('teams.index'));
});

Breadcrumbs::for('teams.create', function($trail) {
    $trail->parent('teams.index');
    $trail->push('Add Teams', route('teams.create'));
});

Breadcrumbs::for('teams.show', function($trail, $team) {
    $trail->parent('teams.index');
    $trail->push($team->name);
});

Breadcrumbs::for('teams.edit', function($trail, $team) {
    $trail->parent('teams.show', $team);
    $trail->push('Edit');
});

Breadcrumbs::for('games.index', function($trail) {
    $trail->parent('home');
    $trail->push('Games', route('games.index'));
});

Breadcrumbs::for('games.create', function($trail) {
    $trail->parent('games.index');
    $trail->push('Create Game', route('games.create'));
});

Breadcrumbs::for('games.show', function($trail, $game) {
    $trail->parent('games.index');
    $trail->push($game->teams[0]->name.' vs '.$game->teams[1]->name);
});

Breadcrumbs::for('games.edit', function($trail, $game) {
    $trail->parent('games.show', $game);
    $trail->push('Edit Schedule', route('games.edit', ['game'=>$game->id]));
});

Breadcrumbs::for('games.result', function($trail, $game) {
    $trail->parent('games.show', $game);
    $trail->push('Result', route('games.result', ['game'=>$game->id]));
});

Breadcrumbs::for('games.predictions.create', function($trail, $game) {
    $trail->parent('games.show', $game);
    $trail->push('Predict', route('games.predictions.create', ['game'=>$game->id]));
});

Breadcrumbs::for('games.predictions.edit', function($trail, $prediction) {
    $trail->parent('games.show', $prediction->game);
    $trail->push('Predict', route('games.predictions.edit', ['game'=>$prediction->game->id, 'prediction'=>$prediction->id]));
});

Breadcrumbs::for('points.bulkAdd', function($trail) {
    $trail->parent('home');
    $trail->push('Bulk Add Points', route('points.bulkAdd'));
});