<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// })->name('welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@welcome')->name('welcome');

// start role routes
Route::resource('/roles','RoleController');
Route::view('/rules','rules')->name('rules');
// end role routes

// start user routes
Route::get('/users/stop','UserController@origUser')->name('users.orig');
Route::get('/users/stats','UserController@stats')->name('users.stats');
Route::get('/users/list','UserController@list')->name('users.list');
Route::resource('/users','UserController',['except'=>['create']]);

Route::group(['prefix'=>'users/{user}/', 'as'=>'users.'], function() {
	Route::get('verify/{email_key}', 'Auth\RegisterController@verifyUser')->name('verify');
	Route::resource('points','PointController', ['only'=>['index']]);
	Route::group(['prefix'=>'points', 'as'=>'points.'], function(){
		Route::get('buy','PointController@buyForm')->name('buy.form');
		Route::post('buy','PointController@buyStore')->name('buy');
	});
});

Route::group(['prefix'=>'points','as'=>'points.'], function() {
	Route::get('bulkAdd','PointController@bulkAdd')->name('bulkAdd');
	Route::post('bulkAdd','PointController@bulkStore')->name('bulkStore');
});

// end user routes

// switch user
Route::get('/users/{user}/switch','UserController@switchUser')->name('users.switch')->middleware('can:switchUser,App\User');
// end switch user

Route::post('/invitations/{invitation}/resend', 'InvitationController@resend')->name('invitations.resend');
Route::resource('/invitations','InvitationController');

Route::get('/teams/list', 'TeamController@list')->name('teams.list');
Route::resource('/teams', 'TeamController');
Route::resource('/games', 'GameController');
Route::get('/games/generate/{group}', 'GameController@generate')->name('games.generate');

Route::group(['prefix'=>'games/{game}', 'as'=>'games.'], function() {
	Route::resource('predictions', 'PredictionController');	
	Route::get('result', 'GameController@getResults')->name('result');	
	Route::put('result', 'GameController@saveResults')->name('result.save');	
});

Route::get('leaderboard','RankController@leaderboard')->name('leaderboard');

Route::get('/users/{user}/deleteAvatar', 'UserController@deleteAvatar')->name('users.deleteAvatar');

Route::resource('/feedback','FeedbackController',['only'=>['store','index']]);