@servers(['production' => 'serverpilot@missingpersons.org.in', 'mash'=>'serverpilot@worldcup.glibix.com'])

@setup
    $repository = 'git@bitbucket.org:codemeglibix/fifa-predictions.git';
    $app_dir = '/srv/users/serverpilot/apps/fifa';
@endsetup

@story('deploy',['parallel'=>true])
	enable_maintenance_mode
    clone_repository
    run_composer
    run_migrations
    cache_app
    manage_queue
    disable_maintenance_mode
@endstory

@task('clone_repository',['parallel'=>true])
    echo 'Updating website'
    cd {{ $app_dir }}
    git pull
    git checkout master
@endtask

@task('run_composer',['parallel'=>true])
    echo "Starting deployment ({{ $release }})"
    cd {{ $app_dir }}
    composer install -q
@endtask

@task('run_migrations',['parallel'=>true])
	echo "Running migrations"
    cd {{ $app_dir }}
	php artisan migrate --force
@endtask

@task('cache_app',['parallel'=>true])
	echo "Caching application"
    cd {{ $app_dir }}
    php artisan cache:clear
	php artisan config:cache
	php artisan view:cache
@endtask

@task('manage_queue',['parallel'=>true])
	echo "Manage queues"
    cd {{ $app_dir }}
	php artisan queue:restart
@endtask

@task('enable_maintenance_mode',['parallel'=>true])
	echo "Putting application in maintenance mode"
    cd {{ $app_dir }}
	php artisan down
@endtask

@task('disable_maintenance_mode',['parallel'=>true])
	echo "Activating application"
    cd {{ $app_dir }}
	php artisan up
@endtask

@task('go_to_app',['parallel'=>true])
    cd {{ $app_dir }}
@endtask