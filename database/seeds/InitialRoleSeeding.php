<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;
use App\Permission;

class InitialRoleSeeding extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		// define roles
		$customer = Role::create(['name'=>'customer', 'label'=>'Customer']);
		$staff = Role::create(['name'=>'readonly_staff', 'label'=>'Staff Readonly Access']);

        $admin_menu = Permission::create(['name'=>'admin_menu','label'=>"Access Admin Menu", 'module' => 'admin']);


		// user permissions permissions
        $user_view = Permission::create(['name'=>'user_view','label'=>"Vew Users", 'module' => 'user']);

        Permission::addModulePermission('user', [
            'suspend' => "Suspend or unsuspend a user account",
            'verify' => "Mark user account as verified",
            'update_profile' => "Update user's profile",
            'update_role' => "Add or remove user's roles",
            'delete' => "Delete a user account",
            'impersonate' => "Login as another user",
        ]);

        // Roles
        Permission::addModulePermission('role', 'manage', 'Manage user roles');
                
        // permissions
        $permission_view = Permission::addModulePermission('permission','view','View Permissions');
    	
    	
    	// // define basic staff permissions
    	$staff->givePermission([$user_view->id, $admin_menu->id, $permission_view->id]);
    }
}
