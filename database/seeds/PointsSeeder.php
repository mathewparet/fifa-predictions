<?php

use Illuminate\Database\Seeder;

use App\Point;
use App\User;

use App\Notifications\InitialPointsNotification;

class PointsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::get()->each(function($user) {
        	$user->addPoints(new Point(['points'=>config('app.fifa.points',1000)
        								, 'comments'=>'Initial points from point bank'])
        					);
            $user->notify(new InitialPointsNotification($user));
        });
    }
}
