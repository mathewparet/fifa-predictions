<?php

use Faker\Generator as Faker;

$factory->define(App\Team::class, function (Faker $faker) {
    return [
        'code' => $faker->countryCode
        , 'name' => $faker->country
        , 'group' => $faker->randomElement(['A','B','C','D','E','F','G','H'])
    ];
});
