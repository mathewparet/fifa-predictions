<?php

use Faker\Generator as Faker;

$factory->define(App\Point::class, function (Faker $faker) {
    return [
        'points' => $faker->randomNumber(3)
        , 'multiplier' => $faker->randomElement([1,-1])
        , 'comments' => $faker->sentence(4)
    ];
});
