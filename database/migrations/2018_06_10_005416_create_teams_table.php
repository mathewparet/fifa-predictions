<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Permission;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->string('code', 2)->primary();
            $table->timestamps();
            $table->string('name');
            $table->string('group', 1);
        });

        Permission::addModulePermission('team',[
            'view' => 'View all teams'
            , 'manage' => 'Manage teams'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::removeModulePermission('team');

        Schema::dropIfExists('teams');
    }
}
