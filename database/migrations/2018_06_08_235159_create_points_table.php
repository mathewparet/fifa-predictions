<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Permission;

class CreatePointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('points', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('points');
            $table->integer('multiplier'); // 1 or -1
            $table->integer('user_id')->unsigned();
            $table->string('comments');
        });

        Schema::table('points', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });

        Artisan::call('db:seed',['--class'=>'PointsSeeder','--force'=>true]);

        Permission::addModulePermission('point',[
            'manage' => 'Manage points for anyone'
            , 'view' => 'Have a readonly view of anyones points'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::removeModulePermission('point');

        Schema::table('points', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });

        Schema::dropIfExists('points');
    }
}
