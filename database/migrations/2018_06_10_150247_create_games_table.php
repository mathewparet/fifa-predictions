<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Permission;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->datetime('scheduled_at')->nullable();
            $table->enum('type',['Group','Knockout']);
        });

        Schema::create('game_team', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('game_id')->unsigned();
            $table->string('team_code',2)->index();
        });

        Schema::create('game_results', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('game_id')->unsigned();
            $table->string('team_code',2)->index();
            $table->enum('win_type', ['Normal Time', 'Extra Time', 'Penalty Shootout', 'Draw']);
        });

        Schema::table('game_team', function(Blueprint $table) {
            $table->foreign('game_id')->references('id')->on('games')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('team_code')->references('code')->on('teams')->onUpdate('CASCADE')->onDelete('CASCADE');
        });

        Schema::table('game_results', function(Blueprint $table) {
            $table->foreign('game_id')->references('id')->on('games')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('team_code')->references('code')->on('teams')->onUpdate('CASCADE')->onDelete('CASCADE');
        });

        Permission::addModulePermission('game',['view'=>'View Game'
            , 'manage'=>'Manage game'
            , 'delete'=>'Delete game'
            , 'update_result'=>'Manage results'
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::removeModulePermission('game');

        Schema::table('game_team', function(Blueprint $table) {
            $table->dropForeign(['game_id']);
            $table->dropForeign(['team_code']);
        });

        Schema::table('game_results', function(Blueprint $table) {
            $table->dropForeign(['game_id']);
            $table->dropForeign(['team_code']);
        });

        Schema::dropIfExists('game_results');
        Schema::dropIfExists('game_team');
        Schema::dropIfExists('games');
    }
}
