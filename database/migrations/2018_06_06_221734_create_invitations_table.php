<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Permission;

class CreateInvitationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invitations', function (Blueprint $table) {
            $table->string('email')->primary();
            $table->string('invitation_code');
            $table->timestamps();
        });

        $invitation_code = App\Invitation::create(['email'=>'mathew.paret@accenture.com']);

        echo "Invitation Code is: ".$invitation_code->invitation_code."\n";

        Permission::addModulePermission('invitation',['create'=>'Create Invitations']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::removeModulePermission('invitation');
        Schema::dropIfExists('invitations');
    }
}
