<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Game;
use App\Jobs\OneTimeJobSaveWinsToDatabase;

class ModifyPredictionsAddWins extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('predictions', function (Blueprint $table) {
            $table->integer('wins')->default(0);
        });

        Game::each(function($game) { 
            OneTimeJobSaveWinsToDatabase::dispatch($game); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('predictions', function (Blueprint $table) {
            $table->dropColumn('wins');
        });
    }
}
