<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyGameResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('points', function (Blueprint $table) {
            $table->dropForeign(['prediction_id']);
        });

        Schema::table('points', function(Blueprint $table) {
            $table->dropColumn('prediction_id');
        });

        Schema::table('predictions', function (Blueprint $table) {
            $table->dropForeign(['game_id']);
            $table->dropForeign(['user_id']);
            $table->dropForeign(['team_code']);
        });
        
        Schema::dropIfExists('predictions');

        Schema::create('predictions', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('game_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('team_code',2)->index()->nullable();
            $table->enum('type',['Win','Draw']);
            $table->enum('time',['Normal Time','Extra Time','Penalty Shootout'])->nullable();
        });

        Schema::table('points', function(Blueprint $table) {
            $table->integer('prediction_id')->unsigned()->nullable();
            $table->integer('result_id')->unsigned()->nullable();
        });


        Schema::table('predictions', function (Blueprint $table) {
            $table->foreign('game_id')->references('id')->on('games')->onDelete('CASCADE');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('CASCADE');
            $table->foreign('team_code')->references('code')->on('teams')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        Schema::table('game_results', function(Blueprint $table) {
            $table->dropForeign(['game_id']);
            $table->dropForeign(['team_code']);
        });

        Schema::dropIfExists('game_results');

        Schema::create('results', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('game_id')->unsigned();
            $table->string('team_code',2)->index()->nullable();
            $table->enum('type',['Win','Draw']);
            $table->enum('time',['Normal Time','Extra Time','Penalty Shootout'])->nullable();
        });

        Schema::table('results', function(Blueprint $table) {
            $table->foreign('game_id')->references('id')->on('games')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('team_code')->references('code')->on('teams')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        Schema::table('points', function(Blueprint $table) {
            $table->foreign('prediction_id')->references('id')->on('predictions')->onDelete('CASCADE');
            $table->foreign('result_id')->references('id')->on('results')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('points', function (Blueprint $table) {
            $table->dropForeign(['prediction_id']);
            $table->dropForeign(['result_id']);
        });

        Schema::table('points', function(Blueprint $table) {
            $table->dropColumn('prediction_id');
            $table->dropColumn('result_id');
        });
        
        Schema::table('results', function(Blueprint $table) {
            $table->dropForeign(['game_id']);
            $table->dropForeign(['team_code']);
        });

        Schema::dropIfExists('results');

        Schema::create('game_results', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('game_id')->unsigned();
            $table->string('team_code',2)->index();
            $table->enum('type', ['Win','Draw','Normal Time','Extra Time','Penalty Shootout']);
        });

        Schema::table('game_results', function(Blueprint $table) {
            $table->foreign('game_id')->references('id')->on('games')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('team_code')->references('code')->on('teams')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        Schema::table('predictions', function (Blueprint $table) {
            $table->dropForeign(['game_id']);
            $table->dropForeign(['user_id']);
            $table->dropForeign(['team_code']);
        });
        
        Schema::dropIfExists('predictions');

        Schema::create('predictions', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('game_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('team_code',2)->index()->nullable();
            $table->enum('type',['Win','Draw']);
            $table->enum('time',['Normal Time','Extra Time','Penalty Shootout'])->defualt('Normal');
        });

        Schema::table('predictions', function (Blueprint $table) {
            $table->foreign('game_id')->references('id')->on('games')->onDelete('CASCADE');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('CASCADE');
            $table->foreign('team_code')->references('code')->on('teams')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        Schema::table('points', function(Blueprint $table) {
            $table->integer('prediction_id')->unsigned()->nullable();
        });

        Schema::table('points', function(Blueprint $table) {
            $table->foreign('prediction_id')->references('id')->on('predictions')->onDelete('CASCADE');
        });
    }
}
