<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyTeamsAndTablesUseFlagAndTeamId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER Table teams add id INTEGER unsigned NOT NULL UNIQUE AUTO_INCREMENT;');

        Schema::table('teams', function(Blueprint $table) {
            $table->string('flag');
        });

        // results table
        Schema::table('results', function(Blueprint $table) {
            $table->integer('team_id')->unsigned()->nullable();
        });

        DB::statement('UPDATE results r SET team_id=(select id from teams where code=r.team_code) where team_code is not null;');

        Schema::table('results', function(Blueprint $table) {
            $table->foreign('team_id')->references('id')->on('teams')->onUpdate('CASCADE');
            $table->dropForeign(['team_code']);
        });

        // predictions
        Schema::table('predictions', function(Blueprint $table) {
            $table->integer('team_id')->unsigned()->nullable();
        });

        DB::statement('UPDATE predictions p SET team_id=(select id from teams where code=p.team_code) where team_code is not null;');

        Schema::table('predictions', function(Blueprint $table) {
            $table->foreign('team_id')->references('id')->on('teams')->onUpdate('CASCADE');
            $table->dropForeign(['team_code']);
        });

        // game_team
        Schema::table('game_team', function(Blueprint $table) {
            $table->integer('team_id')->unsigned()->nullable();
            $table->string('team_code',2)->nullable()->change();
        });

        DB::statement('UPDATE game_team gt SET team_id=(select id from teams where code=gt.team_code) where team_code is not null;');

        Schema::table('game_team', function(Blueprint $table) {
            $table->integer('team_id')->unsigned()->nullable(false)->change();
            $table->foreign('team_id')->references('id')->on('teams')->onUpdate('CASCADE');
            $table->dropForeign(['team_code']);
        });

        // finally set id as primary key
        Schema::table('teams', function(Blueprint $table) {
            $table->dropPrimary();
            $table->primary('id');
            $table->dropIndex('id');
            $table->unique('code');
        });

        Schema::table('teams', function(Blueprint $table) {
            $table->string('code',2)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // game_team
        Schema::table('game_team', function(Blueprint $table) {
            $table->foreign('team_code')->references('code')->on('teams')->onUpdate('CASCADE');
            $table->dropForeign(['team_id']);
        });

        Schema::table('game_team', function(Blueprint $table) {
            $table->dropColumn('team_id');
            $table->string('team_code',2)->nullable(false)->change();
        });

        // predictions
        Schema::table('predictions', function(Blueprint $table) {
            $table->foreign('team_code')->references('code')->on('teams')->onUpdate('CASCADE');
            $table->dropForeign(['team_id']);
        });

        Schema::table('predictions', function(Blueprint $table) {
            $table->dropColumn('team_id');
        });

        // results
        Schema::table('results', function(Blueprint $table) {
            $table->foreign('team_code')->references('code')->on('teams')->onUpdate('CASCADE');
            $table->dropForeign(['team_id']);
        });

        Schema::table('results', function(Blueprint $table) {
            $table->dropColumn('team_id');
        });

        // finally

        Schema::table('teams', function(Blueprint $table) {
            $table->index('id');
            $table->dropPrimary();
            $table->primary('code');
        });

        Schema::table('teams', function(Blueprint $table) {
            $table->dropColumn('id');
            $table->dropColumn('flag');
        });

        DB::statement('alter table `teams` drop index `teams_code_unique`;');

    }
}
