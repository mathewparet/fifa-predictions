<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Permission;

class CreateExchangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exchanges', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('user_id')->unsigned();
            $table->integer('wins_sold');
        });

        Schema::table('exchanges', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });

        Schema::table('points', function(Blueprint $table) {
            $table->integer('exchange_id')->unsigned()->nullable();
        });

        Schema::table('points', function(Blueprint $table) {
            $table->foreign('exchange_id')->references('id')->on('exchanges')->onDelete('CASCADE');
        });

        Permission::addModulePermission('point',['exchange'=>'Exchange wins for Points']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::removeModulePermission('point');

        Schema::table('points', function(Blueprint $table) {
            $table->dropForeign(['exchange_id']);
        });

        Schema::table('points', function(Blueprint $table) {
            $table->dropColumn('exchange_id');
        });

        Schema::table('exchanges', function(Blueprint $table) {
            $table->dropForeign(['user_id']);
        });

        Schema::dropIfExists('exchanges');
    }
}
