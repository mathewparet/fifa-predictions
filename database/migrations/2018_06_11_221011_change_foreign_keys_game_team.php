<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeForeignKeysGameTeam extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('game_team', function(Blueprint $table) {
            $table->dropForeign(['team_code']);
            $table->foreign('team_code')->references('code')->on('teams')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        Schema::table('game_results', function(Blueprint $table) {
            $table->dropForeign(['team_code']);
            $table->foreign('team_code')->references('code')->on('teams')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('game_team', function(Blueprint $table) {
            $table->dropForeign(['team_code']);
            $table->foreign('team_code')->references('code')->on('teams')->onUpdate('CASCADE')->onDelete('CASCADE');
        });

        Schema::table('game_results', function(Blueprint $table) {
            $table->dropForeign(['team_code']);
            $table->foreign('team_code')->references('code')->on('teams')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }
}
