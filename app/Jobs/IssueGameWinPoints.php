<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Game;
use App\Point;
use App\Prediction;
use App\Notifications\GameResultNotification;
use DB;
use Cache;

use App\Http\Controllers\RankController;

class IssueGameWinPoints implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $game;
    public $timeout = 80;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Game $game)
    {
        $this->game = $game;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        DB::transaction(function(){

            $this->game->predictions->each(function($prediction) 
            {    
                $point = $this->savePoints($prediction);

                $this->saveWins($prediction);

                $prediction->user->notify(new GameResultNotification($this->game, $point, $prediction));
    
                Cache::forget('user-prediction_form_'.$prediction->user->id);
            });

            Cache::forget('leaderboard-rank_list');
            RankController::generate();

        });
    }

    protected function savePoints(Prediction $prediction)
    {
        $point = Point::firstOrNew(['result_id'=>$this->game->result->id, 'user_id'=>$prediction->user->id]);
        $point->points = $this->calculatePoints($prediction);
        $point->comments = __('Prize point for prediction of :game',['game'=>$this->game->title]);
        $point->result()->associate($this->game->result);
        
        if($point->user_id)
        {
            $point->save();
            return $point;
        }
        else
            return $prediction->user->addPoints($point);
    }

    private function calculatePoints(Prediction $prediction)
    {
        return ceil($prediction->point->points * $this->getPointsMultiplier($prediction));
    }

    private function getPointsMultiplier(Prediction $prediction)
    {
        switch ($this->game->type) 
        {
            case 'Group':
                return $prediction->hasCorrectTeam() ? config('app.fifa.wins.multiplier.Group.full') : 0;
                break;
            
            case 'Knockout':
                if(!blank($prediction->time))
                {
                    // user has predicted time? then continue
                    if($prediction->hasCorrectTeam() && $prediction->hasCorrectTime())
                        return config('app.fifa.wins.multiplier.Knockout.both');
                    elseif($prediction->hasCorrectTeam() || $prediction->hasCorrectTime())
                        return config('app.fifa.wins.multiplier.Knockout.either');
                    else
                        return 0;
                }
                else
                    return $prediction->hasCorrectTeam() ? config('app.fifa.wins.multiplier.Knockout.match') : 0;
                break;
        }
    }

    protected function saveWins(Prediction $prediction)
    {
        $prediction->wins = $this->getWinsCount($prediction);
        return $prediction->save();
    }


    private function getWinsCount(Prediction $prediction)
    {
        if(!$this->game->result)
            return 0;

        switch ($this->game->type) 
        {
            case 'Group':
                return $prediction->hasCorrectTeam() ? 1 : 0;
                break;
            
            case 'Knockout':
                if($prediction->hasCorrectTeam() && $prediction->hasCorrectTime())
                    return 2;
                elseif ($prediction->hasCorrectTeam() || $prediction->hasCorrectTime())
                    return 1;
                else
                    return 0;
                break;
        }
    }
}
