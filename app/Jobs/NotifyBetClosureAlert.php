<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\User;
use App\Game;
use App\Notifications\BetClosureNotification;

class NotifyBetClosureAlert implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Game::with("teams")->inOneHour()->notificationPending()->each(function($game) {
            User::verified()->active()->each(function($user) use($game) {
                $user->notify(new BetClosureNotification($game));
            });
            $game->markAsNotified();
        });
    }
}
