<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;

use DB;
use App\User;
use App\Point;
use App\Role;

use App\Notifications\PointsAwardedNotification;

class BulkAlotPoints implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $users;
    private $comments;
    private $points;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->users = $this->getRequestedUsers($request);
        $this->comments = $request->comments;
        $this->points = $request->points;
    }

    private function getRequestedUsers(Request $request)
    {
        $users =  collect($request->contestants['user_id'])->unique()->filter();
        $users = $users->merge($this->getRequestedRoleUsers($request));
        return $users;
    }

    private function getRequestedRoleUsers(Request $request)
    {
        $roles = collect($request->contestants['role_name'])->unique()->filter();
        $users = collect([]);
        if($roles->count() > 0)
        {
            $users = $users->merge(User::whereId(1)->pluck('id')->all());
        }
        foreach(Role::whereIn('name', $roles)->get() as $role) {
            $users = $users->merge($role->users->pluck('id')->all());
        }

        return $users;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        DB::transaction(function() {
            User::findMany($this->users)->each(function($user) {
                $user->addPoints(new Point(['comments'=>$this->comments, 'points'=>$this->points]));
                $user->notify(new PointsAwardedNotification($this->points, $this->comments));
            });
        });
    }
}
