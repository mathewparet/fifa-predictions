<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Notifications\LeaderboardMail;

use App\Http\Controllers\RankController;
use App\User;

use Notification;

class SendLeaderboadMailDaily implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $rank_list = RankController::generate();
        $users = User::verified()->active()->get();
        $not_played = User::doesntHave('predictions')->get();
        Notification::send($users, new LeaderboardMail($rank_list, $not_played));
    }
}
