<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Game;
use App\Point;
use App\Prediction;
use DB;
use Cache;
use App\Http\Controllers\RankController;


class OneTimeJobSaveWinsToDatabase extends IssueGameWinPoints implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        DB::transaction(function(){

            $this->game->predictions->each(function($prediction) 
            {    
                $this->saveWins($prediction);
            });

            Cache::forget('leaderboard-rank_list');
            RankController::generate();

        });
    }
}
