<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
	protected $fillable = ['name', 'group', 'code', 'flag'];

    public function predictions()
    {
        return $this->hasMany(Prediction::class);
    }

    public function games()
    {
        return $this->belongsToMany(Game::class);
    }

    public function scopeGroup($query, $group)
    {
        return $query->whereGroup($group);
    }

    // public function getFlagAttribute()
    // {
    // 	return sprintf("http://www.countryflags.io/%s/shiny/%s.png", $this->code, config('app.fifa.flag.small',32));
    // }

    public function getCodeAttribute()
    {
    	return strtolower($this->attributes['code']);
    }

    public function setCodeAttribute($value)
    {
    	$this->attributes['code'] = strtolower($value);
    }

    public function setGroupAttribute($value)
    {
    	$this->attributes['group'] = strtoupper($value);
    }

    public function getGroupAttribute()
    {
    	return strtoupper($this->attributes['group']);
    }
}
