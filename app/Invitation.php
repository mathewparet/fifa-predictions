<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Invitation extends Model
{
    use Notifiable;
    
    protected $fillable = ['email'];
    protected $hidden = ['invitation_code'];
	
	public $incrementing = false;
    protected $primaryKey = 'email';
    
    public function setInvitationCodeAttribute($value)
    {
    	$this->attributes['invitation_code'] = encrypt($value);
    }

    public function getInvitationCodeAttribute()
    {
    	return decrypt($this->attributes['invitation_code']);
    }

    public function save(array $options = [])
    {
        $this->invitation_code = str_random(25);
        return parent::save($options);
    }

    public function isValid($code)
    {
    	return $code === $this->invitation_code;
    }
}
