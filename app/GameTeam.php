<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameTeam extends Model
{
	protected $table = 'game_team';
    public function scopeMatchBetween($query,$teams)
    {
    	return $query->whereIn('team_id',$teams);
    }
}
