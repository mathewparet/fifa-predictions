<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class IsAccentureEmail implements Rule
{
    private $domain;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->domain = config('app.fifa.domain_restriction', false);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        try 
        {
            $email_parts = explode("@", $value);
            return $this->domain === false ? true :  $email_parts[1] === $this->domain;
        }
        catch(\Exception $e)
        {
            return;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Email should be @'.$this->domain;
    }
}
