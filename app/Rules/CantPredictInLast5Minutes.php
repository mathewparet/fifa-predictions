<?php

namespace App\Rules;

use App\Game;

use Illuminate\Contracts\Validation\Rule;

class CantPredictInLast5Minutes implements Rule
{
    private $game;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(Game $game)
    {
        $this->game = $game;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->game->scheduled_at > now()->addMinutes(config('app.fifa.prediction.cutoff',5));
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Predictions closed for this game';
    }
}
