<?php

namespace App\Policies;

use App\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;


    private function isSelf(User $user)
    {
        return request()->user()->id == $user->id;
    }

    public function before($user,$ability) 
    {
        return $user->isSuperAdmin()?:null;
    }

    public function switchUser(User $user)
    {
        return $user->can('user_impersonate');
    }

    public function origUser(User $user)
    {
        return true;
    }

    public function index(User $user)
    {
        return $user->can('user_view');
    }

    public function stats(User $user)
    {
        return $user->can('user_stats');
    }


    /**
     * Determine whether the user can view the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $user
     * @return mixed
     */
    public function view(User $user, User $target)
    {
        return $user->can('user_view')||$this->isSelf($target);
    }

    private function protectSuperUser()
    {
        if(request()->user instanceof User)
            if(request()->user->isSuperAdmin() && !$user->isSuperAdmin())
                return false;

        return true;
    }

    /**
     * Determine whether the user can update the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $user
     * @return mixed
     */
    public function update(User $user, User $target)
    {
        return ($user->can('user_update_profile') || $this->isSelf($target)) && $this->protectSuperUser();
    }

    /**
     * Determine whether the user can delete the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $user
     * @return mixed
     */
    public function delete(User $user, User $target)
    {
        return $user->can('user_delete') && $this->protectSuperUser();
    }
}
