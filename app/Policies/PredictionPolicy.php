<?php

namespace App\Policies;

use App\User;
use App\Prediction;
use Illuminate\Auth\Access\HandlesAuthorization;

class PredictionPolicy
{
    use HandlesAuthorization;

    public function update(User $user, Prediction $prediction)
    {
        return $user->owns($prediction);
    }

    public function create(User $user)
    {
        
    }
}
