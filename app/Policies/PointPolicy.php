<?php

namespace App\Policies;

use App\User;
use App\Point;
use Illuminate\Auth\Access\HandlesAuthorization;

class PointPolicy
{
    use HandlesAuthorization;

    public function before($user,$ability) 
    {
        return $user->isSuperAdmin()?:null;
    }

    /**
     * Determine whether the user can view the point.
     *
     * @param  \App\User  $user
     * @param  \App\Point  $point
     * @return mixed
     */
    public function show(User $user)
    {
        return $user->can('point_view') || $user->isOwnRequest();
    }

    public function add(User $user)
    {
        return $user->can('point_manage');
    }

    public function buy(User $user)
    {
        return $user->isOwnRequest() && $user->can('point_exchange') && !Point::exchangeTimedOut();
    }

    public function exchange(User $user)
    {
        return $user->can('point_exchange') && !Point::exchangeTimedOut();
    }

}
