<?php

namespace App\Policies;

use App\User;
use App\Game;
use Illuminate\Auth\Access\HandlesAuthorization;

class GamePolicy
{
    use HandlesAuthorization;

    public function predict(User $user, Game $game)
    {
        return $game->isPredictable;
    }

    public function update_result(User $user, $game = null)
    {
        return $user->can('game_update_result') 
            && $game ? $game->isOver : true;
    }

    public function generate(User $user)
    {
        return $user->can('game_manage');
    }

    public function view(User $user)
    {
        return $user->can('game_view');
    }

    public function manage(User $user)
    {
        return $user->can('game_manage');
    }

    public function update(User $user)
    {
        return $user->can('game_manage');
    }

    public function create(User $user)
    {
        return $user->can('game_manage');
    }

    public function delete(User $user)
    {
        return $user->can('game_delete');
    }
}
