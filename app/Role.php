<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable=['name','label'];

    public function permissions()
    {
    	return $this->belongsToMany(Permission::class);
    }

    public function givePermission($permission)
    {
        if($permission)
            $permission = is_array($permission)?$permission:[$permission->id];
    	
    	return $this->permissions()->sync($permission);
    }

    public function addPermission($permission)
    {
        return $this->permissions()->attach($permission);
    }

    public function users()
    {
    	return $this->belongsToMany(User::class);
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = strtolower($value);
    }

    public function getNameAttribute($value)
    {
        return strtolower($value);
    }

    public function getRouteKeyName()
    {
        return 'name';
    }

}
