<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class Point extends Model
{
    protected $fillable = ['points', 'comments'];

    public function prediction()
    {
        return $this->belongsTo(Prediction::class);
    }

    public function exchange()
    {
        return $this->belongsTo(Exchange::class);
    }

    public function result()
    {
        return $this->belongsTo(Result::class);
    }

    public static function exchangeTimedOut()
    {
        return Game::orderBy('scheduled_at','desc')->first()->scheduled_at->subMinutes(config('app.fifa.exchange.cutoff')) <= now();
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function getTransactionTypeAttribute()
    {
    	return $this->attributes['multiplier'] == -1 ? 'Debit' : 'Credit';
    }

    public function getCreditAttribute()
    {
    	return $this->multiplier == 1 ? $this->points : 0;
    }

    public function getDebitAttribute()
    {
    	return $this->multiplier == -1 ? (-1 * $this->points) : 0;
    }

    public function scopeCredit($query)
    {
        return $query->whereMultiplier(1);
    }

    public function scopeDebit($query)
    {
        return $query->whereMultiplier(-1);
    }

    public function getValueAttribute()
    {
        return $this->multiplier*$this->points;
    }

    public static function exchangeForWins($wins, User $user)
    {
        return DB::transaction(function() use($wins, $user){
            $exchange = new Exchange(['wins_sold' => $wins]);

            $user->makeExchange($exchange);

            $point = $user->addPoints(
                    new Point(['points'=>Exchange::getPointValue($wins)
                        , 'comments'=>__(":wins wins exchanged for :points points", ['wins'=>$wins, 'points'=>Exchange::getPointValue($wins)])
                    ])
                );

            $point->exchange()->associate($exchange);
            $point->save();

            return $point;
        });
    }

}
