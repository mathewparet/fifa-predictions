<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exchange extends Model
{
    protected $fillable = ['wins_sold'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public static function getPointValue($wins=1)
    {
    	return $wins * config('app.fifa.exchange.rate');
    }

    public function point()
    {
    	return $this->hasOne(Point::class);
    }
}
