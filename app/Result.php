<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
	protected $fillable = ['type','time'];
    
    public function team()
    {
    	return $this->belongsTo(Team::class);
    }

    public function game()
    {
    	return $this->belongsTo(Game::class);
    }
}
