<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Http\Request;
use Carbon\Carbon;

use DB;

class Game extends Model
{
    protected $fillable = ['scheduled_at', 'type', 'closure_notification'];
    protected $dates = ['scheduled_at'];

    public function result()
    {
        return $this->hasOne(Result::class);
    }

    public function getTeamIdsAttribute()
    {
        $winner_ids = $this->teams->pluck('id')->all();
        if($this->type == 'Group')
            $winner_ids[] = 'Draw';

        return $winner_ids;
    }

    public static function groupGameExists($teams)
    {
        return count(DB::select(
            DB::raw(
                "select g.id
                from game_team gt
                    , games g
                where gt.team_id in ('".implode("', '", $teams)."')
                    and gt.game_id = g.id
                    and g.type='Group'
                group by g.id
                having count(g.id) = 2"
            )
        )) > 0;
    }

    public function saveResult(Request $request)
    {
        $result = Result::firstOrNew(['game_id'=>$this->id]);

        $result->team_id = $request->winner == 'Draw' ? null : $request->winner;
        $result->type = $request->winner == 'Draw' ? 'Draw' : 'Win';
        $result->time = $this->type == 'Group' ? null : ($request->time == 'null' ? null : $request->time);
        $result->game_id = $this->id;
        return $result->save();
    }

    public function scopeNotificationPending($query,$status=true)
    {
        return $query->whereClosureNotification(!$status);
    }

    public function markAsNotified()
    {
        return $this->update(['closure_notification'=>true]);
    }

    public function scopeInOneHour($query)
    {
        return $query->whereBetween('scheduled_at', [now(), now()->addHours(1)]);
    }

    public function scopeToday($query)
    {
        return $query->whereBetween('scheduled_at', [now(), now()->addHours(24)]);
    }

    public function teams()
    {
    	return $this->belongsToMany(Team::class);
    }

    public function addTeam(Team $team)
    {
        return $this->teams()->attach($team);
    }

    public function setScheduledAtAttribute($value)
    {
        $this->attributes['scheduled_at'] = Carbon::createFromFormat('Y-m-d\TH:i', $value);
    }

    public function predictions()
    {
        return $this->hasMany(Prediction::class);
    }

    public function savePrediction(Prediction $prediction)
    {
        return $this->predictions()->save($prediction);
    }

    public function getIsPredictableAttribute()
    {
        return $this->scheduled_at > now()->addMinutes(config('app.fifa.prediction.cutoff',5));
    }

    public function getIsOverAttribute()
    {
        if($this->scheduled_at === null)
            return false;
        
        return $this->scheduled_at < now()->subMinutes(config('app.fifa.result.cutoff',105));
    }

    public function getTitleAttribute()
    {
        return __(':type Game - :team_a vs :team_b',['type'=>$this->type
            , 'team_a'=>$this->teams[0]->name
            , 'team_b'=>$this->teams[1]->name
        ]);
    }
}
