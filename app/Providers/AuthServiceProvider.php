<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

use App\Permission;

use App\User;
use App\Point;
use App\Prediction;
use App\Game;
use App\Policies\GamePolicy;
use App\Policies\PredictionPolicy;
use App\Policies\PointPolicy;
use App\Policies\UserPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        User::class => UserPolicy::class,
        Point::class => PointPolicy::class,
        Prediction::class => PredictionPolicy::class,
        Game::class => GamePolicy::class,
    ];
    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        foreach ($this->getPermissions() as $permission) 
        {
            Gate::define($permission->name, function($user) use ($permission) {
                return $user->hasRole($permission->roles);
            });
        }
    }

    protected function getPermissions()
    {
        try // this block is to ensure this call doesn't fail during initial installation of roles and permissions since table wouldn't exist
        {
            return Permission::with('roles')->get();
        }
        catch(\Illuminate\Database\QueryException $e)
        {
            // do nothing
            return array();
        }
    }
}
