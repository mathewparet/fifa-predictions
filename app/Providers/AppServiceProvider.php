<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Illuminate\Support\Facades\Validator;
use App\Validators\MaxWordsValidator;
use App\Http\Controllers\UserController;

use App;
use Cookie;
use Crypt;
use DB;

use Laravel\Dusk\DuskServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('not_exist',function($attribute, $value, $parameters, $validator){
            return ! DB::table($parameters[0])->where($parameters[1], $value)->count();
        });

        Validator::extend('verify_user',UserController::class.'@verifyUser');
        Validator::extend('has_role',UserController::class.'@hasRole');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment('local', 'testing')) {
            $this->app->register(DuskServiceProvider::class);
    }
    }
}
