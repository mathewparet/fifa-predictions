<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $fillable = [
        'name',  'label', 'module'
    ];

    public function roles()
    {
    	return $this->belongsToMany(Role::class);
    }

    public function users()
    {
        $user_list =collect([]);
        
        foreach($this->roles()->get() as $role)
        {
            foreach($role->users()->get() as $user)
            {
                $user_list->push($user);
            }
        }

        return $user_list;
    }

    public static function addModulePermission($module, $permission, $label = null)
    {
    	if(is_array($permission))
            foreach ($permission as $perm => $label) {
                self::addModulePermission($module, $perm, $label);
            }
        else
            return self::create(['name' => $module."_".$permission, 'label' => $label, 'module'=>$module]);
    }

    public static function removeModulePermission($module,$permissions=null)
    {
    	if(!$permissions)
            return self::whereModule($module)->delete();
        else
            return self::whereModule($module)->whereIn('name',$permissions)->delete();
    }

}
