<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

use Intervention\Image\ImageManagerStatic as Image;


use Illuminate\Http\File;

use Storage;

use DB;
use Sms;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function notifications(User $user)
    {
        $user->unreadNotifications->markAsRead();
        return redirect()->back();
    }

    public function list()
    {
        $users = User::where('name','like','%'.strtolower(request('term')).'%')->orWhere('email',request('term'))->get();
        $roles = Role::withCount('users')->where('name','like','%'.strtolower(request('term')).'%')->orWhere('label',request('term'))->get();

        $results=array();

        foreach($users as $user)
            $results[] = array(
                                'label'=>'User: '.$user->name.' ('.$user->email.')'
                                , 'value'=>$user->email
                                , 'avatar' => $user->avatar
                                , 'user_id' => $user->id
                                , 'type' => 'User'
                            );

        foreach($roles as $role)
            $results[] = array(
                                'label'=>'Role: '.$role->label.' ('.$role->name.' / '.($role->users_count+1).' '.str_plural('user',$role->users_count+1).')'
                                , 'value'=>$role->name
                                , 'role_name'=>$role->name
                                , 'type' => 'Role'
                                , 'avatar' => '//www.gravatar.com/avatar/'.md5("no-reply@fifa.glibix.com").'.png?s=140'
                            );

        return $results;
    }

    public function stats(User $user)
    {
        $this->authorize(User::class);

        return view('users.stats');
    }

    public function switchUser(User $user)
    {
        $this->authorize($user);
        $action_by = auth()->user()->email;
        session()->put('orig_user',auth()->user());
        auth()->login($user);
        Log::info("Switched to different user",['user'=>$user->email, 'action_by'=>$action_by, 'session'=>session()->getId()]);
        return redirect()->route('home');
    }

    public function origUser(User $user)
    {
        $old_user = auth()->user()->email;
        auth()->login(session()->pull('orig_user'));
        Log::info('User switched back',['from_user'=>$old_user]);
        return redirect()->back();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $this->authorize(auth()->user());

        $users=$user->orderBy('name')->paginate(config('app.page_size'));

        return view('users.index',compact('users'));
    }

    public function show(User $user)
    {
        $this->authorize('view',$user);
        return view('users.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $this->authorize('update',$user);

        return view('users.edit',compact('user'));
    }

    public function verifyUser($attribute, $value, $parameters, $validator)
    {
        return Hash::check($value, auth()->user()->password);
    }

    public function hasRole($attribute, $value, $parameters, $validator)
    {
        return auth()->user()->hasRole($parameters[0]);
    }

    public function deleteAvatar(User $user)
    {
        $this->authorize('update', $user);
        if(Storage::disk("public")->exists($user->avatar))
            Storage::disk("public")->delete($user->avatar);
        $user->avatar=NULL;
        $user->save();
        return redirect()->back();
    }

    private function resizeAvatar($image_file)
    {
        $resized = Image::make($image_file);
        $resized->resize(140,140);
        $resized->save($image_file);
    }

    private function saveAvatar(User $user, $avatar)
    {
        $file_temp_path = Storage::path("tmp.avatar.".uniqid().'.jpg');

        file_put_contents($file_temp_path, base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $avatar)));

        $this->resizeAvatar($file_temp_path);

        $file_save_path = Storage::disk("public")->putFile("avatars", new File($file_temp_path));

        unlink($file_temp_path);

        if($user->avatar)
            $this->deleteAvatar($user);

        return $file_save_path;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $this->authorize($user);

        $conditions = collect([ 'password'=>'nullable|string|min:6|confirmed'
                                , 'name'=>'required|string'
                                , 'current_password'=>'required|verify_user'
                                , 'suspended'=>'sometimes|has_role:user_suspend'
                            ]);


        $this->validate($request, $conditions->all());

        DB::transaction(function() use($request, $user){

            if($request->filled("avatar-data"))
                $user->avatar = $this->saveAvatar($user, $request->input("avatar-data"));

            if($request->filled('password'))
            {
                $user->password = Hash::make(request('password'));
                $user->api_token = str_random(60);
            }

            if($request->has('suspended'))
                if($user->isSelf() || $user->isSuperAdmin())
                    abort(403,'Unauthorized access');

            if(auth()->user()->can('user_update_role') && !$user->isSelf())
                $user->assignRole($request->role);
            
            $user->name = $request->name;

            Log::debug('User profile update',['old'=>$user->mobile,'new'=>$request->mobile]);

            if(Auth()->user()->hasRole('user_suspend'))
                $user->suspended = $request->suspended?:false;
            
            if(Auth()->user()->hasRole('user_verify') && $request->filled('verified'))
                $user->verify();

            $user->save();

        });

        Log::info('User account updated',['user'=>$user, 'action_by'=>auth()->user()->email]);

        session()->flash('status.user.show',['success'=>'User profile Successfully updated']);

        return redirect()->route('users.show', ['user'=>$user->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $this->authorize('delete',$user);

        $this->validate(request(),['delete_password'=>'required|verify_user']);

        if($user->isSuperAdmin())
        {
            session()->flash('status.user.delete',['danger'=>'Cannot delete super admin account: '.$user->email]);
            return redirect()->back();
        }

        $user->delete();
        
        Log::info('User account deleted: ',['user'=>$user, 'action_by'=>auth()->user()->email]);
        session()->flash('status.general',['success'=>'Successfully deleted account: '.$user->email]);
        
        return redirect()->route('home');
    }
}
