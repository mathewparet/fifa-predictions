<?php

namespace App\Http\Controllers;

use App\Prediction;
use App\Point;
use App\Game;
use Illuminate\Http\Request;
use DB;
use Log;

use App\Rules\CantPredictInLast5Minutes;

class PredictionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Game $game)
    {
        $this->authorize('predict', $game);

        if(auth()->user()->hasPredicted($game))
            return redirect()->route('games.predictions.edit', ['game'=>$game->id, 'prediction'=>auth()->user()->getPredictionFor($game)->id]);

        return view('games.predictions.create', compact('game'));
    }

    private function createPredictionObject(Request $request, Game $game)
    {
        return new Prediction([
            'user_id'=>auth()->user()->id
            , 'type' => $request->team_id == 'Draw' ? 'Draw' : 'Win'
            , 'team_id' =>$request->team_id == 'Draw' ? null : $request->team_id
            , 'time' => $game->type == 'Group' ? null : ($request->time == 'null' ? null : $request->time)
        ]);
    }

    private function getRules(Game $game, $points = 0)
    { 
        $rules['points'] = ['required'
                ,'numeric'
                ,'min:0'
                ,'max:'.(auth()->user()->wallet_points + $points)
                , new CantPredictInLast5Minutes($game)];
        $rules['team_id'] = "required|in:".implode(",", $game->team_ids);

        if($game->type == 'Knockout')
            $rules['time'] = 'in:Normal Time,Extra Time,Penalty Shootout,null';

        return $rules;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Game $game, Request $request)
    {
        if(auth()->user()->hasPredicted($game))
            return abort(403, 'Unauthorized activity');

        $request->validate($this->getRules($game));

        $prediction = $this->createPredictionObject($request, $game);

        try
        {
            DB::transaction(function() use($request, $game, &$prediction) {
                // save if everything is good, else don't save anything and rollback
                $prediction = $game->savePrediction($prediction);
                
                $point = auth()->user()->deductPoints(
                    new Point(['points'=>$request->points
                        , 'comments'=>__('Bet: Game #:game_id - :title'
                                            , ['game_id'=>$game->id
                                                , 'title'=>$game->title
                                                ]
                                        )
                    ])
                );
                
                $point->prediction()->associate($prediction);
                $point->save();
            });
        }
        catch(\Exception $e)
        {
            return redirect()->back()->with('status.games.predictions.create',['danger'=>'Could not save your prediction. No points have been deducted']);
        }

        $message = __('You have predicted that the match :match will be a :type', ['match'=>$game->title, 'type'=>$prediction->type]);
        if($prediction->type == 'Win')
            $message.=__(' for :team',['team'=>$prediction->team->name]);

        Log::info("Game predicted",['game'=>$game, 'action_by'=>auth()->user()->email, 'prediction'=>$prediction]);

        return redirect()->route('games.index')->with('status.games.index',['success'=>$message]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Prediction  $prediction
     * @return \Illuminate\Http\Response
     */
    public function show(Prediction $prediction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Prediction  $prediction
     * @return \Illuminate\Http\Response
     */
    public function edit(Game $game, Prediction $prediction)
    {
        $this->authorize($prediction);
        $this->authorize('predict', $game);

        return view('games.predictions.edit', compact('game','prediction'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Prediction  $prediction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Game $game, Prediction $prediction)
    {
        $this->authorize($prediction);

        $request->validate($this->getRules($game, $prediction->point->points));

        try
        {
            DB::transaction(function() use($request, $game, &$prediction) {

                $prediction->type = $request->team_id == 'Draw' ? 'Draw' : 'Win';
                $prediction->team_id = $request->team_id != 'Draw' ? $request->team_id : null;
                $prediction->point->update(['points'=>$request->points]);
                $prediction->time = $request->time=='null' ? null : $request->time;
                $prediction->save();
            });
        }
        catch(\Exception $e)
        {
            return redirect()->back()->with('status.games.predictions.create',['danger'=>'Could not save your prediction. No points have been deducted']);
        }

        $message = __('You have predicted that the match :match will be a :type', ['match'=>$game->title, 'type'=>$prediction->type]);
        if($prediction->type == 'Win')
            $message.=__(' for :team',['team'=>$prediction->team->name]);

        return redirect()->route('games.index')->with('status.games.index',['success'=>$message]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Prediction  $prediction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Prediction $prediction)
    {
        //
    }
}
