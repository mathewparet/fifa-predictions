<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Invitation;
use App\Point;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Rules\IsAccentureEmail;
use App\Rules\RecaptchaValidate;
use App\Rules\ValidInvitationCode;
use Illuminate\Auth\Events\Registered;

USE DB;
use Log;
use App\Notifications\SendEmailVerificationNotification;
use App\Notifications\InitialPointsNotification;
use App\MobileVerification;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => ['required'
                        , 'string'
                        , 'email'
                        , 'max:255'
                        , 'unique:users'
                        , new IsAccentureEmail()
                        ],
            'gender' => 'required|in:male,female',
            'invitation_code' => ['required'
                                  ,'string'
                                  , new ValidInvitationCode($data['email'])],
            'password' => 'required|string|min:6|confirmed',
            'g-recaptcha-response'=> [new RecaptchaValidate],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return DB::transaction(function() use($data){
            $user = new User([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                'gender' => $data['gender']
            ]);

            $user->email_key = str_random(25);
            $user->api_token = str_random(60);

            $user->save();

            $user->assignRoleById(1);

            Invitation::find($user->email)->delete();

            return $user;
        });
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }

    protected function registered(Request $request, $user)
    {
        $user->notify(new SendEmailVerificationNotification($user));

        Log::info('New user registration',[$user]);

        session()->flash('status.general',['success'=>'User account successfully created. Please check your email on further steps to activate your account. You will have to do this only once.']);
    }

    public function verifyUser(User $user, $email_key = null)
    {
        if($user->email_key == $email_key)
        {
          DB::transaction(function() use($user){
            $user->verify();

            $user->addPoints(new Point(['points'=>config('app.fifa.points')
                        , 'comments'=>'Initial points from point bank']));

            $user->notify(new InitialPointsNotification($user));

          });

          return redirect()->route('login')->with('status.general',['success'=>'Email address successfully verified. You may login now.']);
        }
        else
            abort(403, 'Unauthorized verification');
    }
}
