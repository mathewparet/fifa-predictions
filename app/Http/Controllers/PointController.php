<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Schema;
use App\Point;
use App\User;
use App\Exchange;
use Illuminate\Http\Request;

use DB;
use Log;

use App\Jobs\BulkAlotPoints;

class PointController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function buyForm(User $user)
    {
        $this->authorize('buy', Point::class);

        return view('users.points.form', compact('user'));
    }

    public function buyStore(Request $request, User $user)
    {
        $this->authorize('buy', Point::class);
        $request->validate(['winsSelling' => 'required|numeric|min:1|max:'.$user->total_wins]);

        try
        {
            $point = Point::exchangeForWins($request->winsSelling, $user);
        }
        catch(\Exception $e)
        {
            Log::error('Error alloting points',['user'=>$user, 'exception'=>$e]);
            return redirect()->back()->with('status.users.points.buy.form',['danger'=>'Could allot points. Please try again later.']);
        }

        Log::info("Wins exchanged for points",['user'=>$user, 'exchange'=>$point->exchange]);

        return redirect()->route('home')->with('status.general',['success'=>$point->points.' points alloted successfully']);
    }

    private function getTransactions($user=null)
    {
        $whereClause = $user ? "where user_id = $user->id" : '';

        DB::unprepared("
            set @rt:=0;
            create temporary table transaction_tmp as
            select  *
                , case 
                    when multiplier = 1 then points
                        else 0
                  end as credit
                , case 
                    when multiplier = -1 then points 
                        else 0
                  end as debit
            from (
                select *
                    , (@rt:=multiplier*points + @rt) as balance
                from (select *
                      from points
                      $whereClause
                      order by updated_at asc
                        , id asc) transactions
                 ) transactions
            order by updated_at desc
                , id desc;
        ");

        $results = DB::table("transaction_tmp")
                    ->orderBy('updated_at','desc')
                    ->orderBy('id', 'desc')
                    ->skip(config('app.page_size',50)*(request('page')-1))
                    ->take(config('app.page_size',50))->get();

        $transactions = Point::hydrate($results->all());
        
        $transactions = new \Illuminate\Pagination\LengthAwarePaginator($transactions, DB::table("transaction_tmp")->count(), config('app.page_size',50));

        $transactions->setPath('points');

        return $transactions;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $this->authorize('show', Point::class);
        $points = $this->getTransactions($user);
        return view('users.points.index',compact('points','user'));
    }

    public function bulkAdd()
    {
        $this->authorize('add', Point::class);

        return view('points.bulk');
    }

    public function bulkStore(Request $request)
    {
        $this->authorize('add', Point::class);

        $request->validate([
            'contestants.type.*'=>'in:Role,User|required'
            , 'contestants.user_id.*'=>'distinct|nullable|exists:users,id|required_if:contestants.type.*,User'
            , 'contestants.role_name.*'=>'distinct|nullable|exists:roles,name|required_if:contestants.type.*,Role'
            , 'comments'=>'required|string|min:10'
            , 'points'=>'required|numeric|min:0'
            , 'confirmation'=>'accepted'
        ]);

        BulkAlotPoints::dispatch($request);

        return redirect()->route('home')->with('status.general', ['success'=>'Points will be added momentarly. Users will receive a credit notification soon.']);
    }
}
