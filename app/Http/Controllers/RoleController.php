<?php

namespace App\Http\Controllers;

use App\Role;
use App\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:permission_view');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Role $role)
    {
        $roles = $role->paginate(config('app.page_size'));
        return view('roles.index',compact('roles'));
    }

    public function show(Role $role)
    {
        $role = Role::with('permissions')->findorfail($role)->first();
        return view('roles.show',compact('role'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('role_manage');

        $permissions = Permission::orderBy('name')->get();

        return view('roles.create',compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('role_manage');

        $this->validate($request, ['name'=>'required|alpha_dash|unique:roles,name'
                                    ,'label'=>'string|nullable'
                                    ,'permissions'=>'array|exists:permissions,id']);

        $role = Role::create(request(['name','label']));

        $role->givePermission($request->permissions);

        session()->flash('status.roles.show',['success'=>__(':Type :data Successfully created', ['Type'=>__('Role'),'data'=>$role->name])]);

        return redirect()->route('roles.show',['role'=>$role->name]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        $this->authorize('role_manage');

        $role = Role::with('permissions')->findorfail($role)->first();
        
        $permissions = Permission::orderBy('name')->get();

        return view('roles.edit',compact('role','permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        $this->authorize('role_manage');

        $this->validate($request, ['name'=>'required|alpha_dash'
                                    ,'label'=>'string|nullable'
                                    ,'permissions'=>'array|exists:permissions,id']);

        $role->givePermission($request->permissions);

        $role->fill(request(['name','label']));
        $role->Save();

        session()->flash('status.roles.show',['success'=>__(':Type :data Successfully updated', ['Type'=>__('Role'),'data'=>$role->name])]);

        return redirect()->route('roles.show',['role'=>$role->name]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $this->authorize('role_manage');

        if($role->id==1)
            abort(403,'Cannot delete customer role');

        $role->delete();

        Log::info('Role deleted.',['role'=>$role]);
        session()->flash('status.roles.index',['success'=>__(':Type :data Successfully deleted', ['Type'=>__('Role'),'data'=>$role->name])]);
        return redirect()->route('roles.index');
    }
}
