<?php

namespace App\Http\Controllers;

use App\Team;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;

use DB;
use Cache;
use Storage;

use Illuminate\Http\File;

class TeamController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = Cache::rememberForever('teams.index-groups', function() {
            $teams = Team::orderBy('group')->get();
            return $teams->groupBy('group');
        });

        return view('teams.index', compact('groups')); 
    }

    public function list()
    {
        $teams = Team::where('name','like','%'.strtolower(request('term')).'%')->where('id',"<>",request('not',0))->get();

        $results=array();

        foreach($teams as $team)
            $results[] = array(
                                'label'=>$team->name
                                , 'value'=>$team->name
                                , 'name'=>$team->name
                                , 'flag' => Storage::url($team->flag)
                                , 'id' => $team->id
                            );

        return $results;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('team_manage');

        return view('teams.create');
    }

    private function groupRuleDefinition()
    {
        return 'required|string|size:1|alpha';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('team_manage');

        $request->validate(['group'=> $this->groupRuleDefinition()
            , 'team.name.*'=>'nullable|distinct|required_with:team.id.*'
        ]);

        $total_teams = 0;

        DB::transaction(function() use($request, &$total_teams) {
            foreach($request->input('team.name') as $index => $team){    
                if($request->team['name'][$index] != null)
                {
                    $total_teams++;

                    $flag = $this->saveFlag($request->input('team.flag.'.$index));

                    Team::create(['name'=>$request->team['name'][$index]
                        , 'group'=>$request->group
                        , 'flag'=>$flag
                    ]);
                }

            }
        });

        Cache::forget('teams.index-groups');

        return redirect()->route('teams.index')->with('status.teams.index', ['success'=>$total_teams.' teams added to Group '.$request->group]);

    }

    private function saveFlag($flag, $team=null)
    {
        $file_temp_path = Storage::path("tmp.flag.".uniqid().'.jpg');

        file_put_contents($file_temp_path, base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $flag)));

        $file_save_path = Storage::disk("public")->putFile("flags", new File($file_temp_path));

        unlink($file_temp_path);

        if($team)
        {
            if(Storage::disk("public")->exists($team->flag))
                Storage::disk("public")->delete($team->flag);
        }

        return $file_save_path;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function edit(Team $team)
    {
        $this->authorize('team_manage');
        
        return view('teams.edit', compact('team'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Team $team)
    {
        $this->authorize('team_manage');

        $request->validate(['group'=>$this->groupRuleDefinition()
            , 'name'=>'required'
        ]);

        DB::transaction(function() use($request, $team) {
            $flag = $this->saveFlag($request->input('flag'), $team);
            $team->update(collect($request->only(['name','group']))->merge(['flag'=>$flag])->all());

            Cache::forget('teams.index-groups');
        });

        return redirect()->route('teams.index')->with('status.teams.index', ['success'=>'Team '.$team->name.' updated successfully.']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function destroy(Team $team)
    {
        $this->authorize('team_manage');
        try
        {
            $team->delete();

            if(Storage::disk("public")->exists($team->flag))
                Storage::disk("public")->delete($team->flag);

            Cache::forget('teams.index-groups');      
        }
        catch(\Exception $e)
        {
            return redirect()->route('teams.index')->with('status.teams.index', ['danger'=>'Team '.$team->name.' could not be deleted. Please check if a game has already been scehduled for this. You will need to delete any scheduled games for this team, before the team can be deleted.']);
        }

        return redirect()->route('teams.index')->with('status.teams.index', ['success'=>'Team '.$team->name.' deleted successfully.']);

    }
}
