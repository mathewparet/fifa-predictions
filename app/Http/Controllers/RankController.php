<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Prediction;
use App\Exchange;
use App\Point;
use App\User;
use DB;
use Cache;

class RankController extends Controller
{
	public function __construct()
    {
        // $this->middleware('auth');
    }

	public static function getRankByNumberOfWins()
	{
		return Prediction::hydrate(DB::select(
			DB::raw("
				select predictions.user_id
					, coalesce(predictions.total_wins - coalesce(exchanges.wins_sold,0), 0) wins
				from
				(
					select user_id, sum(wins) total_wins from predictions group by user_id
				) predictions
				left outer join
				(
					select user_id, sum(wins_sold) wins_sold from exchanges group by user_id
				)
				exchanges
				on exchanges.user_id = predictions.user_id
				group by predictions.user_id
			")
		));
	}

	public static function mergePointRanksWithNumberOfWinsAndSortInOrderOfRank($wins, $ranks)
	{
		$result = collect();

		$ranks->each(function($rank) use($wins, $result) {
			$result_element = ['user_id'=>$rank->user_id, 'points'=>$rank->total_points, 'wins'=>0];
			$wins->each(function($win) use($rank, &$result_element) {
				if($win['user_id'] == $rank->user_id) { 
					$result_element['wins'] = $win['wins'];
				}
			});
			$result->push($result_element);
		});

		return $result->sortByDesc('wins')->groupBy('points')->sortKeysDesc()->flatten(1);
	}

    public static function getTotalPointsForRanking()
    {
    	return DB::select(
    		DB::raw(
				"
				select balance_points.user_id, (balance_points+coalesce(invested_points,0)) as total_points
				from (	select p.user_id
							, sum(p.multiplier * p.points) as balance_points
						from points p
							, users u
						where p.user_id = u.id
							and exists (select 1 from predictions where user_id=u.id)
						group by p.user_id
					) 
				as balance_points
				left outer join 
				(
					select u.id as user_id
						, sum(p.points) as invested_points 
					from points p
				 		, games g
						, predictions pr
						, users u
					where pr.game_id = g.id
						and p.prediction_id = pr.id
						and pr.user_id = u.id
						and g.scheduled_at > now()
					group by u.id
				) as 
				invested_points
				on balance_points.user_id = invested_points.user_id
				order by total_points desc
				"
    		)
    	);
    }

    public static function generate()
    {
    	return Cache::rememberForever("leaderboard-rank_list", function() 
    	{
    		$wins  = self::getRankByNumberOfWins();

			$balance_points = collect(self::getTotalPointsForRanking());

			$result = self::mergePointRanksWithNumberOfWinsAndSortInOrderOfRank($wins, $balance_points);

			$final_result = collect();

			$normal_ranks = collect();

			$result->each(function($winner) use(&$final_result, &$normal_ranks) {
				static $rank_value = 0;

				static $last_person_points=0;
				static $last_person_wins=0;

				if($winner['points'] != $last_person_points || $winner['wins'] != $last_person_wins)
					$rank_value++;

				$last_person_wins = $winner['wins'];
				$last_person_points = $winner['points'];
				$win_user = User::find($winner['user_id']);

				if(!$final_result->get('king') && $win_user->gender == 'male')
					$final_result = $final_result->merge(self::makeArrayOfResult('king', $win_user, $winner));
				
				elseif(!$final_result->get('queen') && $win_user->gender == 'female')
					$final_result = $final_result->merge(self::makeArrayOfResult('queen', $win_user, $winner));
				
				else
				{
					$normal_ranks->push(['rank'=>$rank_value
						, 'user' => $win_user
						, 'total_points' => $winner['points']
						, 'total_wins' => $winner['wins']
					]);
				}
				
			});

			$jack = self::getTopPredictor($normal_ranks);

			$jack_user = $jack->pluck('user')->first();

			$jack_rank = $normal_ranks->where('user.id',$jack_user->id)->first()['rank'];

			$normal_ranks = $normal_ranks->reject(function($value, $key) use($jack_rank) {
				return $value['rank'] == $jack_rank;
			});	

			$final_result = $final_result->merge(['others'=>$normal_ranks->all()]);

			
			$final_result = $final_result->merge(['jack'=>
													[ 'rank'=>'jack'
														, 'user'=>$jack_user
														, 'total_points' => $jack_user->total_points
														, 'total_wins' => $jack_user->total_wins ]
												]);
			$final_result = $final_result->merge(['last_refreshed'=>now()]);
			return $final_result;
    	});
    }

    private static function getTopPredictor($results)
    {
    	$others = $results;

    	return $others->sortByDesc('total_points')
						->where('total_wins', $others
												->max('total_wins')
						);
    }

    private static function makeArrayOfResult($key, User $user, $winner)
    {
    	return 	[ $key =>	[	'rank' => $key
    							, 'user'	=>	$user
    							, 'total_points' => $winner['points']
    							, 'total_wins' => $winner['wins']
    						]
    			];
    }

    public function leaderboard()
    {
    	if(!request()->ajax())
    		return view('leaderboard');
    	else
    	{
	   		$rank_list = $this->generate();

	   		$not_played = Cache::remember("leaderboard-not_played",1, function() {
	   			return User::doesntHave('predictions')->get();
	   		});

	    	$fresh_user = User::whereNotIn('id',$not_played->pluck('id')->all())->get()->keyBy('id');

	    	$updated_at = $rank_list['last_refreshed']->diffInDays()>1?$rank_list['last_refreshed']->format(config('app.long_date_format')):$rank_list['last_refreshed']->diffForHumans();
    		
    		return response()->json(compact('rank_list', 'not_played', 'updated_at', 'fresh_user'));
    	}
    }
}
