<?php

namespace App\Http\Controllers;

use App\Feedback;
use Illuminate\Http\Request;
use Input;
use Log;

class FeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $feedback = Feedback::latest()->paginate(config('app.page_size'));

        return view('feedback.index',compact('feedback'));
    }

    public function store(Request $request)
    {
        $data = json_decode($request->getContent());
        Log::debug("Decoded Data",[$data]);
        $id = $data->form_response->token;
        foreach ($data->form_response->answers as $answer)
        {
            Log::debug("In Loop",[$answer]);
            if($answer->field->id==config('app.feedback.fields.name'))
                $name = $answer->text;
            if($answer->field->id==config('app.feedback.fields.initiative_rating'))
                $intiative_rating = $answer->number;
            if($answer->field->id==config('app.feedback.fields.portal_rating'))
                $portal_rating = $answer->number;
            if($answer->field->id==config('app.feedback.fields.comments'))
                $comments = $answer->text;
        }

        Log::info("Response received",compact('name','intiative_rating','portal_rating','comments','id'));

        Feedback::create(compact('name','intiative_rating','portal_rating','comments','id'));

        return response("Data Saved",200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function destroy(Feedback $feedback)
    {
        $feedback->delete();

        session()->flash('status.general',['success'=>__('Feedback from :name deleted successfully',['name'=>$feedback->name])]);
        
        return redirect()->route('feedback.index');
    }
}
