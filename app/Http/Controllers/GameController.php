<?php

namespace App\Http\Controllers;

use App\Result;
use App\Game;
use App\Team;
use App\Prediction;
use Illuminate\Http\Request;

use Log;
use DB;

use App\Jobs\IssueGameWinPoints;

class GameController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function generate($group)
    {
        $this->authorize('generate', Game::class);
        
        $teams = Team::group($group)->get()->pluck('id');

        $already_mapped = collect([]);
        $games = collect([]);

        $teams->each(function($name, $key) use($teams, $already_mapped, $games) {
            $already_mapped->push($name);
            $games->push(collect([$name])->crossJoin($teams->diff($already_mapped))); 
        });

        $game_teams = $games->flatten(1);

        foreach($game_teams as $teams)
        {
            if(Game::groupGameExists($teams))
                continue;

            $game = Game::create(['type'=>'group']);
            
            foreach($teams as $team)
                $game->addTeam(Team::find($team));
        }

        return redirect()->route('games.index')->with('status.games.index',['success'=>count($game_teams).' games created. These wont be visible to the users until schedule dates are updated.']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('view', Game::class);
        
        if(!request()->ajax())
        {
            return view('games.index');
        }
        else
        {
            $games =  Game::with('teams', 'result')
                        ->selectRaw("*, case when scheduled_at > now() then 'Future' when scheduled_at <= now() then 'Past' else 'Draft' end as block")
                        ->orderBy('scheduled_at','asc')
                        ->get()
                        ->groupBy('block');

            $predictions = auth()->user()->predictions()->with('point','team')->get();
            return compact('predictions', 'games');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('game_manage', Game::class);

        $teams = Team::orderBy('name')->get();

        return view("games.create", compact('teams'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('game_manage', Game::class);
        
        $request->validate([
            'scheduled_at'=>'date|required'
            , 'type' => 'required|in:Group,Knockout'
            , 'team.*.id' => 'required|exists:teams,id|distinct'
        ]);

        $game = null;

        DB::transaction(function() use($request, &$game) {
            $game = Game::create(['type'=>$request->type, 'scheduled_at'=> $request->scheduled_at]);

            $game->addTeam(Team::find($request->team['a']['id']));
            $game->addTeam(Team::find($request->team['b']['id']));

        });

        if($game->id)
            return redirect()
                ->route('games.index')
                ->with('status.games.index'
                        ,['success'=>__('New :type match between :team_a and :team_b has been updated.'
                                            , ['type'=>$request->type
                                                , 'team_a'=>$game->teams[0]->name
                                                , 'team_b'=>$game->teams[1]->name]
                                        )]
                );
        else
            return redirect()
                    ->route('games.index')
                    ->with('status.games.index'
                            ,['danger'=>"An error occured trying to add the game."]
                    );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function show(Game $game)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function edit(Game $game)
    {
        $this->authorize('game_manage');

        return view('games.edit', compact('game'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Game $game)
    {
        $this->authorize($game);
        
        $request->validate([
            'scheduled_at'=>'date|required'
        ]);

        $game->update($request->only('scheduled_at'));

        return redirect()->route('games.index')->with('status.games.index',['success'=>sprintf('Schedule for the match between %s and %s has been updated.', $game->teams[0]->name, $game->teams[1]->name)]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function destroy(Game $game)
    {
        $this->authorize('delete', $game);
        
        $team_a = $game->teams[0]->name;
        $team_b = $game->teams[1]->name;

        try
        {
            $game->delete();
        }
        catch(\Exception $e)
        {
            Log::error('Game delete error',[$e]);
            return redirect()->route('games.index')->with('status.games.index',['danger'=>sprintf('Match between %s and %s could not be deleted. Please check whether bets have already been placed for this game. ', $team_a, $team_b)]);
        }
        
        return redirect()->route('games.index')->with('status.games.index',['success'=>sprintf('Match between %s and %s has been deleted.', $team_a, $team_b)]);

    }

    public function getResults(Game $game)
    {
        $this->authorize('update_result',$game);

        $result = Result::firstOrNew(['game_id'=>$game->id]);
        
        return view('games.result',compact('game', 'result'));
    }

    private function getRules(Game $game)
    {
        $rules['winner'] = 'required|in:'.implode(',',$game->team_ids);
        
        if($game->type == 'Knockout')
            $rules['time'] = 'required|in:Normal Time,Extra Time,Penalty Shootout';

        return $rules;
    }

    public function saveResults(Request $request, Game $game)
    {
        $this->authorize('update_result',$game);

        $request->validate($this->getRules($game));

        Log::info("Game results saved", ['game'=>$game, 'action_by'=>auth()->user()->email]);

        $game->saveResult($request);

        IssueGameWinPoints::dispatch($game);

        return redirect()->route('games.index')->with('status.games.index',['success'=>__('Game results updated for :game',['game'=>$game->title])]);
    }
}
