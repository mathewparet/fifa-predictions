<?php

namespace App\Http\Controllers;

use App\Invitation;
use App\Rules\IsAccentureEmail;
use Illuminate\Http\Request;

use App\Notifications\SendInvitations;

use Log;

class InvitationController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:invitation_create');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invitations = Invitation::latest()->paginate(config('app.page_size'));
        return view('invitations.index',compact('invitations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('invitations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['email.*' => ['nullable'
                                                , 'distinct'
                                                , 'string'
                                                , 'email'
                                                , 'max:255'
                                                , 'unique:users,email'
                                                , 'unique:invitations,email'
                                                , new IsAccentureEmail()]]);
        
        $numInvites = 0;

        foreach ($request->email as $email) {
            if(strlen($email) > 0)
            {
                $numInvites++;
                $invitation = Invitation::create(['email'=>$email]);
                $invitation->notify(new SendInvitations($invitation));
                Log::info('Invitation generated',['invitation_for'=>$invitation->email, 'action_by'=>auth()->user()]);
            }
        }


        return redirect()->route('invitations.index')->with('status.invitations.index',['success'=>$numInvites.' '.str_plural('invitation',$numInvites).' successfully generated']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Invitation  $invitation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invitation $invitation)
    {
        $invitation->delete();
        Log::info('Invitation deleted',['invitation_for'=>$invitation->email, 'action_by'=>auth()->user()]);
        return redirect()->route('invitations.index')->with('status.invitations.index',['success'=>'Invitation for '.$invitation->email.' successfully deleted']);
    }

    public function resend(Invitation $invitation)
    {
        $invitation->notify(new SendInvitations($invitation));

        return redirect()->route('invitations.index')->with('status.invitations.index',['success'=>'Invitation resend to '.$invitation->email.' successfully']);
    }
}
