<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Mail;
use DB;
use Cache;

use App\Http\Controllers\RankController;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'email_key', 'suspended', 'api_token'
    ];

    protected $fillable = [
        'email', 'name',  'password', 'email_key', 'mobile', 'gender', 'avatar'
    ];

    protected $casts = [
        'suspended' => 'boolean',
    ];

    protected $appends = ['prediction_form', 'avatar'];

    public function makeExchange(Exchange $exchange)
    {
        return $this->exchanges()->save($exchange);
    }

    public function exchanges()
    {
        return $this->hasMany(Exchange::class);
    }

    public function avatar()
    {
        return $this->avatar?\Storage::url($this->avatar):asset('avatar.jpg');
    }

    public function getAvatarAttribute()
    {
        return !blank($this->attributes['avatar']) ? \Storage::url($this->attributes['avatar']):asset('avatar.jpg');
    }
    
    public function predictions()
    {
        return $this->hasMany(Prediction::class);
    }

    public function getNameAttribute()
    {
        return title_case($this->attributes['name']);
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = title_case($value);
    }

    public function setEmailKeyAttribute($value)
    {
        $this->attributes['email_key'] = $value ? encrypt($value) : null;
    }

    public function getEmailKeyAttribute()
    {
        return $this->attributes['email_key'] ? decrypt($this->attributes['email_key']) : null;
    }
    public function setEmailAttribute($value)
    {
        return $this->attributes['email'] = strtolower($value);
    }

    public function getVerifiedAttribute()
    {
        return blank($this->email_key);
    }

    public function scopeVerified($query)
    {
        return $query->whereNull('email_key');
    }

    public function scopePending($query)
    {
        return $query->whereNotNull('email_key');
    }

    public function scopeSuspended($query)
    {
        return $query->whereSuspended(true);
    }
    
    public function scopeActive($query)
    {
        return $query->whereSuspended(false);
    }

    public function owns($relation)
    {
        return $relation->user_id === $this->id ;
    }

    public function isSelf()
    {
        return $this->id === auth()->user()->id;
    }

    public function isOwnRequest()
    {
        return $this->id === request()->user->id;
    }

    public function verify()
    {
        DB::transaction(function() {
            $this->email_key = null;
            $this->save();
        });
    }


    public function isSuperAdmin() 
    {
        return $this->id===1?:false;
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function points()
    {
        return $this->hasMany(Point::Class);
    }

    public function addPoints(Point $point)
    {
        $point->multiplier = 1;
        $point->points = ceil($point->points);
        return $this->points()->save($point);
    }

    public function deductPoints(Point $point)
    {
        $point->multiplier = -1;
        $point->points = ceil($point->points);
        return $this->points()->save($point);
    }

    public function assignRole($role)
    {
        return $this->roles()->sync($role);
    }
    
    public function assignRoleById($role)
    {
        return $this->assignRole(Role::find($role));
    }

    public function hasRole($role)
    {
        if($this->isSuperAdmin())
            return true;

        if(is_string($role)) {
            return $this->roles->contains('name',$role);
        }

        return !! $role->intersect($this->roles)->count();
    }

    public function getWalletPointsAttribute()
    {
        return $this->points->sum(function($points) { return $points['multiplier'] * $points['points']; });
    }

    public function getPointsInPredictionAttribute()
    {
        return $this->total_points - $this->wallet_points;
    }

    public function getTotalPointsAttribute()
    {
        $points_for_all = RankController::getTotalPointsForRanking();
        $user_total_points = collect($points_for_all)->where('user_id',$this->id)->pluck('total_points')->first();
        return $user_total_points;
    }

    public function getPastPredictionsAttribute()
    {
        return DB::select(DB::raw(__("
            select count(*) num_predictions
                from predictions p
                    , games g
                where p.user_id=:user_id
                    and g.id=p.game_id
                    and g.scheduled_at < now();
        ",['user_id'=>$this->id])))[0]->num_predictions;
    }

    public function getPastTimePredictionsAttribute()
    {
        return DB::select(DB::raw(__("
            select count(*) num_predictions
                from predictions p
                    , games g
                where p.user_id=:user_id
                    and g.id=p.game_id
                    and g.scheduled_at < now()
                    and p.time is not null;
        ",['user_id'=>$this->id])))[0]->num_predictions;
    }

    public function getPredictionFormAttribute()
    {
        return Cache::rememberForever('user-prediction_form_'.$this->id, function() {
            if(!($this->past_predictions + $this->past_time_predictions))
                return 0;
            return round(($this->earned_wins / ($this->past_predictions + $this->past_time_predictions)) * 100);
        });
    }

    public function getAverageLossAttribute()
    {
        return $this->points()->debit()->get()->avg('points');
    }
    
    public function getAverageGainAttribute()
    {
        return $this->points()->credit()->get()->avg('points');
    }

    public function getTotalLossAttribute()
    {
        return $this->points()->debit()->get()->sum('points');
    }
    
    public function getTotalGainAttribute()
    {
        return $this->points()->credit()->get()->sum('points');
    }

    public function getRankAttribute()
    {
        $ranks = RankController::generate();
        return $ranks->where('user.id', $this->id)->first()['rank'] ? 
                : collect($ranks->get('others'))->where('user.id',$this->id)->first()['rank'];
    }

    public function getEarnedWinsAttribute()
    {
        return $this->predictions->sum('wins');
    }

    public function getWinsExchangedAttribute()
    {
        return $this->exchanges->sum('wins_sold');
    }

    public function getTotalWinsAttribute()
    {
        return $this->earned_wins - $this->wins_exchanged;
    }

    public function getPredictionFor(Game $game)
    {
        return $this->predictions()->game($game);
    }

    public function hasPredicted(Game $game)
    {
        return $this->getPredictionFor($game)->count();
    }

}
