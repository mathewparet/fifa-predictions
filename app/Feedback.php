<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    public $incrementing = false;

    protected $fillable = ['name','intiative_rating','portal_rating','comments','id'];

}
