<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use App\User;
use App;

class SendEmailVerificationNotification extends Notification implements ShouldQueue
{
    use Queueable;

    protected $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        
        return (new MailMessage)->subject(config('app.name').' user email verification')
                                ->greeting('Hi '.$this->user->name)
                                ->salutation('Team '.config('app.name'))
                                ->line('You are receiving this email because you tried to registed for an account with us.')
                                ->line('Before you can start placing bets we need to verify this email address. This verification link expires in 24 hours. If you do not verify by this time, you will have to re-register your user account.')
                                ->action('Verify Email Address', route("users.verify",['user'=>$this->user->id, 'email_key'=>$this->user->email_key]))
                                ->line('Thank you for registering with us.');

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'user' => $this->user,
        ];
    }
}
