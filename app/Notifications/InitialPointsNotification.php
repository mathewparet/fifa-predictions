<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use App\User;

class InitialPointsNotification extends Notification implements ShouldQueue
{
    use Queueable;

    protected $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        
        return (new MailMessage)->subject('Your have been awarded an initial set of '.config('app.fifa.points').' points')
                                ->greeting('Hi '.$this->user->name)
                                ->salutation('Team '.config('app.name'))
                                ->line('Welcome to the team!')
                                ->line('To start with your betting, we have awarded your account with '.config('app.fifa.points').' points. Use your points wisely to bet on the right team, and with some luck, you could earn more points.')
                                ->action('See my Points Passbook', route("users.points.index",['user'=>$this->user->id]))
                                ->line('Happy predictions!');

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'user' => $this->user,
        ];
    }
}
