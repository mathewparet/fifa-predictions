<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PointsAwardedNotification extends Notification implements ShouldQueue
{
    use Queueable;

    protected $points;
    protected $comments;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($points, $comments)
    {
        $this->points = $points;
        $this->comments = $comments;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($user)
    {
        return (new MailMessage)->subject(__('Yay! you got :points points more, enjoy predicting',['points'=>$this->points]))
                                ->greeting('Hi '.$user->name)
                                ->salutation('Team '.config('app.name'))
                                ->line(__('You have been given :points points for ":comments".', ['points'=>$this->points, 'comments'=>$this->comments]))
                                ->line(__('Your current point wallet balance is :total', ['total'=>$user->wallet_points]))
                                ->line('Use your points wisely to bet on the right team, and with some luck, you could earn more points.')
                                ->action('See my Points Passbook', route("users.points.index",['user'=>$user->id]))
                                ->line('Happy predictions!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($user)
    {
        return [
            'user'=>$user
        ];
    }
}
