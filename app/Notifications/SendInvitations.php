<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use App\Invitation;

class SendInvitations extends Notification implements ShouldQueue
{
    use Queueable;

    protected $invitation;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Invitation $invitation)
    {
        $this->invitation = $invitation;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        
        return (new MailMessage)->subject('Invitation to '.config('app.name'))
                                ->greeting('Hello There!')
                                ->salutation('Team '.config('app.name'))
                                ->line('You have been invited to play '.config('app.name').' with us.')
                                ->line('To register at our portal you will need an invitation, which you can find below. This invitation code is exclusively for you and will work with only this email address. If you do not wish to play, you may simply ignore this email. If you change your decision later, you can come back and accept the invitation to register for your account.')
                                ->action('Accept Invitation & Register', route("register",['email'=>$this->invitation->email, 'invitation_code'=>$this->invitation->invitation_code]))
                                ->line('Happy Predictions!');

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'invitation' => $this->invitation,
        ];
    }
}
