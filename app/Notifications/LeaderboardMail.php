<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class LeaderboardMail extends Notification implements ShouldQueue
{
    use Queueable;

    public $rank_list;
    public $not_played;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($rank_list, $not_played)
    {
        $this->rank_list = $rank_list;
        $this->not_played = $not_played;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)->subject(config('app.name').' Leaderboard')
            ->markdown('mails.leaderboard',['rank_list'=>$this->rank_list, 'not_played'=>$this->not_played]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
