<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use App\Game;
use App\Prediction;
use App\Point;

class GameResultNotification extends Notification implements ShouldQueue
{
    use Queueable;

    protected $game;
    protected $point;
    protected $prediction;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Game $game, Point $point, Prediction $prediction)
    {
        $this->game = $game;
        $this->point = $point;
        $this->prediction = $prediction;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($user)
    {

        $message = '';
        if($this->game->result->type == 'Win')
            $message = $this->game->result->team->name.' wins';
        else
            $message = 'its a Draw';

        return (new MailMessage)->subject(__("Results for the :game are out - :message",['game'=>$this->game->title, 'message'=>$message]))
                                ->greeting('Hi '.$user->name)
                                ->salutation('Team '.config('app.name'))
                                ->line(__("Results for the :game are out - :message",['game'=>$this->game->title, 'message'=>$message]))
                                ->line(__('You bet on :team and won :points for this match', ['team'=>$this->prediction->type == 'Draw' ? 'Draw' : $this->prediction->team->name, 'points'=>$this->point->points]))
                                ->action('See my Points Passbook', route("users.points.index",['user'=>$user->id]))
                                ->line('Happy predictions!');

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'user' => $this->user,
        ];
    }
}
