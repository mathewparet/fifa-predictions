<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use App\Game;

class BetClosureNotification extends Notification implements ShouldQueue
{
    use Queueable;
    private $game;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Game $game)
    {
        $this->game = $game;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($user)
    {
        $prediction_status = '';

        if($user->hasPredicted($this->game))
        {
            $prediction_status = 'You have predicted that the game would be a '.($user->getPredictionFor($this->game)->type);
            if($user->getPredictionFor($this->game)->type == 'Win')
                $prediction_status.=' for '.$user->getPredictionFor($this->game)->team->name.'.';
            else
                $prediction_status.='. ';

            $prediction_status.='You have put '.$user->getPredictionFor($this->game)->point->points.' points on this prediction.';
        }
        else
            $prediction_status = 'You have not made any predictions for this game yet.';

        return (new MailMessage)->subject(__("Your last chance to predict today's :game", ["game"=>$this->game->title]))
                                ->greeting('Hi '.$user->name)
                                ->salutation('Team '.config('app.name'))
                                ->line(__(":game is scheduled to start @ :time", ["game"=>$this->game->title,"time"=>$this->game->scheduled_at]))
                                ->line(__("Predictions will be closed :mins minutes before the game starts.",['mins'=>config('app.fifa.prediction.cutoff')]))
                                ->line($prediction_status)
                                ->line("To predict, OR, if you wish to change your prediction for this game, this will be your last chance.")
                                ->action('Predict now!', route("games.predictions.create",['game'=>$this->game->id]))
                                ->line('Happy predictions!');

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($user)
    {
        return [
            'user' => $user,
        ];
    }
}
