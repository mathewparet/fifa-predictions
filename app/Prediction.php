<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Prediction extends Model
{
    use Notifiable;
    
    protected $fillable = ['point_id', 'team_id', 'time', 'user_id', 'type'];

    public function team()
    {
    	return $this->belongsTo(Team::class);
    }

    public function game()
    {
    	return $this->belongsTo(Game::class);
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function point()
    {
    	return $this->hasOne(Point::class);
    }

    public function scopeHasGame($query, $game)
    {
        return $query->where('game_id', $game->id)->count() > 0;
    }

    public function scopeGame($query, $game)
    {
        return $query->where('game_id', $game->id)->first();
    }

    public function getHasWonAttribute()
    {
        return $this->wins == 0 ? false : true;
    }

    public function hasCorrectTeam()
    {
        return $this->game->result->team_id === $this->team_id && $this->hasCorrectType(); 
    }

    public function hasCorrectTime()
    {
        return $this->game->result->time === $this->time && $this->hasCorrectType();
    }

    public function hasCorrectType()
    {
        return $this->game->result->type == $this->type;
    }
}
