
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import VueAnimateNumber from 'vue-animate-number';
window.Vue.use(VueAnimateNumber);

import {FacebookLoader} from 'vue-content-loader';
window.FacebookLoader = FacebookLoader;
window.Vue.use(FacebookLoader);

import {BulletListLoader} from 'vue-content-loader';
window.BulletListLoader = BulletListLoader;
window.Vue.use(BulletListLoader);

import {CodeLoader} from 'vue-content-loader';
window.CodeLoader = CodeLoader;
window.Vue.use(CodeLoader);

import moment from 'moment';
window.moment = moment;


window.Cropper = require('cropperjs');
window.PathramCropper = require('./PathramCropper');

window.pointRunner = new Vue({
    el: '#pointsApp'
    , methods: {
        formatter: function(num) {
            return num.toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
    }
});
