
	var inputImage;
	var Cropper;
	var cropper
	var URL;
	var imagePreview;
	var image;
	var options;

	export function init()
	{
		this.options  = { };
		this.URL = window.URL || window.webkitURL;
		this.Cropper = window.Cropper;
	}

	export function setInputImage(inputImage)
	{
		this.inputImage = $(inputImage);

		return this;
	}

	export function setImagePreview(previewElement)
	{
		this.options.preview = previewElement;

		return this;
	}

	export function setCropSpace(crop_space)
	{
		this.image = document.getElementById(crop_space);
		return this;
	}

	export function setAspectRatio(ratio)
	{
		this.options.aspectRatio = ratio;

		return this;
	}

	export function getImage()
	{
		return this.cropper.getCroppedCanvas().toDataURL('image/jpeg');
	}

	export function activate()
	{
		var self = this;
		this.inputImage.change(function(e) {
			var files = this.files;
	        var file;

	        if (files && files.length) 
	        {
	            file = files[0];

	            if (/^image\/\w+/.test(file.type)) 
	            {
	            	URL = window.URL || window.webkitURL;
	                self.image.src = URL.createObjectURL(file);
	                try
	                {
	                    cropper.destroy();
	                }
	                catch(Exception)
	                {
	                    ;
	                }

	                self.cropper = new self.Cropper(self.image, self.options);
	                self.inputImage.value = null;
	            }
	            else 
	            {
	                window.alert('Please choose an image file.');
	            }
	        }
		});
	}

