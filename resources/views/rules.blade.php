@extends('layouts.app')
@section('title','Game Rules')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Rules of the Game</div>

                <div class="card-body">
<h2>Initial Points</h2>
<p>All participants will be awarded 1,000 base points to start with. There will be no additional points allotted at any stage. Your only chance of increasing the points is by predicting the right results.</p>

<h2>Game Structure</h2>
<p>There are a total of 64 matches which will be divided into Phase 1 & Phase 2</p>

<p>Phase 1 will have 48 matches which are played within the groups. Each game can be predicted a win for one of them or a draw.</p>

<p>Phase 2 will have a total of 16 matches which will be a knockout stage. There will be no "draw" option to predict for these games. In this phase you have an additional option to predict the winning "method" - Normal Time, Extra Time, Penalty Shootout. There is NO MANDATE to make this additional prediction.</p>

<h2>Point Calculation - Phase 1</h2>
<ul>
    <li>Each "exact" win gives you {{config('app.fifa.wins.multiplier.Group.full')}} times your invested points.</li>
    <li>You lose any invested points on losing the prediction.</li>
</ul>

<h2>Point Calculation - Phase 2</h2>
<p>If you choose to play only the main prediction - choosing who will win the match:</p>
<ul>
    <li>Each win gives you {{config('app.fifa.wins.multiplier.Knockout.match')}} times your invested points.</li>
    <li>You lose any invested points on losing the prediction.</li>
</ul>
<p>If you choose to play both the main prediction and the win method prediction:</p>
<ul>
    <li>On winning both the predictions on the game, you get {{config('app.fifa.wins.multiplier.Knockout.both')}} times your invested points.</li>
    <li>On winning any one of the predictions on the game, you get {{config('app.fifa.wins.multiplier.Knockout.either')}} times of your invested points. If this value ends up to be a fraction, it will be rounded up to the next highest whole number.</li>
    <li>You lose any invested points on losing both predictions on the game.</li>
</ul>
<h2>Common Rules</h2>
<ul>
    <li>You can invest a minimum of 0 (see section on "Prizes" to know why) points to the maximum available points. You cannot invest in fractions. It needs to be a whole number.</li>
    <li>These points will be immediately deducted from your point wallet.</li>
    <li>On winning, you get points as explained in the points calculation section (above)</li>
    <li>On losing, you lose the invested points.</li>
    <li>There is NO negative point for skipping a game.</li>
    <li>Predictions can be done up-to 5 minutes before the scheduled start of the game</li>
    <li>You can modify your predictions any number of times up-to before 5 minutes of the scheduled game start.</li>
</ul>
<p>For example: for the matche between Russia & Saudi Arabia, the possible predictions are:</p>
<ol>
    <li>Russia Wins</li>
    <li>Saudi Arabia Wins</li>
    <li>Draw</li>
</ol>

<p>Predicting one of the above scenarios that match "exactly" with the game results will earn points as described above.</p>

<h2>Example #1:</h2>
<p>If you predict Russia to win the game, but:</p>
<ul>
    <li>it results in a draw - you lose the invested points.</li>
    <li>Saudi Arabia wins - you lose the invested points.</li>
    <li>Russia Wins - you earn points as described above.</li>
</ul>

<h2>Example #2:</h2>
<p>If you predict the game to be a draw, but:</p>
<ul>
    <li>Saudi Arabia wins - you lose the invested points.</li>
    <li>Russia wins - you lose the invested points.</li>
    <li>it's a draw - you earn points as described above.</li>
</ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('breadcrumbs', Breadcrumbs::render('rules'))
