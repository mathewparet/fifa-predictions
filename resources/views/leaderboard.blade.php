@extends('layouts.authenticated')
@section('title')
Leaderboard
@endsection
@section('main')
<div class="container" id="leaderboardApp">
    <div class="row" v-if="lastRefreshed">
        <div class="col">
            @component('layouts.components.alert')
                @slot('type','info')
                   The below leaderboard was last refreshed @{{lastRefreshed}}
            @endcomponent
        </div>
    </div>
    <div class="row">
        <div class="col mb-3">
            <div class="card border-primary">
                <div class="card-body">
                    <div class="card-title text-center align-items-center align-text-center">
                        <h3>Leader <i class="text-warning fas fa-trophy"></i> Board</h3>
                    </div>
                    <div class="card-text d-block d-md-none">
                        @component('layouts.components.alert')
                            @slot('type','info')
                            Can't see points or wins? Just tilt you phone :). If you still can't see, try an even bigger screen :)
                        @endcomponent
                    </div>
                    <div class="row align-contents-center">
                        <div class="col">
                            <div class="card-deck">
                                <div class="card mb-3 border-success" style="cursor: pointer;" @click.prevent="userProfile(rankList.king)">
                                    <div class="card-header text-white bg-success"><h5><i class="fas fa-chess-king"></i> - King Predictor</h5></div>
                                    <div class="card-body">
                                        <div class="card-text">
                                            <content-loader v-if="!rankList.king.user.id" :speed="2" :animate="true">
                                            </content-loader>
                                            <div class="media" v-if="rankList.king.user.id">
                                              <img height="100" class="mr-3 rounded-circle" v-bind:src="fresh_user[rankList.king.user.id].avatar">
                                              <div class="media-body">
                                                <h5 class="mt-0">@{{fresh_user[rankList.king.user.id].name}}</h5>
                                                <div class="row">
                                                    <div class="col">
                                                        <dl class="dl-horizontal">
                                                            <dt>Wins</dt><dd>@{{rankList.king.total_wins}}</dd>
                                                        </dl>
                                                    </div>
                                                    <div class="col">
                                                        <dl class="dl-horizontal">
                                                            <dt>Points</dt><dd>@{{rankList.king.total_points}}</dd>
                                                        </dl>
                                                    </div>
                                                    <div class="col">
                                                        <dl class="dl-horizontal">
                                                            <dt>Form</dt><dd>@{{rankList.king.user.prediction_form}}%</dd>
                                                        </dl>
                                                    </div>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card mb-3 border-success" style="cursor: pointer;" @@click.prevent="userProfile(rankList.queen)">
                                    <div class="card-header text-white bg-success"><h5><i class="fas fa-chess-queen"></i> - Queen Predictor</h5></div>
                                    <div class="card-body">
                                        <div class="card-text">
                                            <content-loader v-if="!rankList.queen.user.id" :speed="2" :animate="true">
                                            </content-loader>
                                            <div class="media" v-if="rankList.queen.user.id">
                                              <img height="100" class="mr-3 rounded-circle" v-bind:src="fresh_user[rankList.queen.user.id].avatar">
                                              <div class="media-body">
                                                <h5 class="mt-0">@{{fresh_user[rankList.queen.user.id].name}}</h5>
                                                <div class="row">
                                                    <div class="col">
                                                        <dl class="dl-horizontal">
                                                            <dt>Wins</dt><dd>@{{rankList.queen.total_wins}}</dd>
                                                        </dl>
                                                    </div>
                                                    <div class="col">
                                                        <dl class="dl-horizontal">
                                                            <dt>Points</dt><dd>@{{rankList.queen.total_points}}</dd>
                                                        </dl>
                                                    </div>
                                                    <div class="col">
                                                        <dl class="dl-horizontal">
                                                            <dt>Form</dt><dd>@{{rankList.queen.user.prediction_form}}%</dd>
                                                        </dl>
                                                    </div>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="card mb-3 border-secondary" style="cursor: pointer;" @click.prevent="userProfile(rankList.jack)">
                                <div class="card-header text-white bg-secondary"><h5>Top Predictor</h5></div>
                                <div class="card-body">
                                    <div class="card-text">
                                        <content-loader v-if="!rankList.jack.user.id" :speed="2" :animate="true">
                                            </content-loader>
                                        <div class="media" v-if="rankList.jack.user.id">
                                          <img height="75" class="mr-3 rounded-circle" v-bind:src="fresh_user[rankList.jack.user.id].avatar">
                                          <div class="media-body">
                                            <h5 class="mt-0">@{{fresh_user[rankList.jack.user.id].name}}</h5>
                                            <div class="row">
                                                <div class="col">
                                                    <dl class="dl-horizontal">
                                                        <dt>Wins</dt><dd>@{{rankList.jack.total_wins}}</dd>
                                                    </dl>
                                                </div>
                                                <div class="col">
                                                    <dl class="dl-horizontal">
                                                        <dt>Points</dt><dd>@{{rankList.jack.total_points}}</dd>
                                                    </dl>
                                                </div>
                                                <div class="col">
                                                    <dl class="dl-horizontal">
                                                        <dt>Form</dt><dd>@{{rankList.jack.user.prediction_form}}%</dd>
                                                    </dl>
                                                </div>
                                            </div>
                                          </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <list-loader v-if="!rankList.others" :speed="2" :animate="true">
                    </list-loader>
                    <table class="table table-hover" v-else>
                        <thead>
                            <tr>
                                <th class="text-right">#</th>
                                <th>Player</th>
                                <th class="text-right d-none d-sm-table-cell">Points</th>
                                <th class="text-right d-none d-md-table-cell">Wins</th>
                                <th class="text-right d-none d-md-table-cell">Form</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr style="cursor: pointer;" v-for="other in rankList.others" @click.prevent="userProfile(other)">
                                <td class="text-right">@{{other.rank}}</td>
                                <td>
                                    <img v-bind:src="fresh_user[other.user.id].avatar" class="rounded-circle" height="32" width="32"> &nbsp; @{{fresh_user[other.user.id].name}}
                                </td>
                                <td class="text-right d-none d-sm-table-cell">
                                    @{{other.total_points}}&nbsp;<span class="fas fa-coins text-warning"></span>
                                </td>
                                <td class="text-right d-none d-md-table-cell">
                                    @{{other.total_wins}}&nbsp;<i class="fas fa-trophy text-warning"></i>
                                </td>
                                <td class="text-right d-none d-md-table-cell">
                                    @{{other.user.prediction_form}}%&nbsp;<i class="fas fa-star text-warning"></i>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row" v-if="not_played.length > 0">
        <div class="col-md mb-3">
            <div class="card border-secondary">
                <div class="card-body">
                    <div class="card-title text-muted">These players have not predicted any game yet and are not considered for ranking: </div>
                    <ol class="text-muted" v-for="player in not_played">
                        <li>@{{player.name}}</li>    
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('breadcrumbs', Breadcrumbs::render('leaderboard'))
@push('bottomJs')
    <script type="text/javascript">
        Vue.config.devtools = true;
        const leaderboardApp = new Vue({
            el: '#leaderboardApp'
            , components: {
                ContentLoader: window.FacebookLoader
                , ListLoader: window.BulletListLoader
            }
            , data: {
                rankList: {
                    king: {
                        user: {
                            id: null
                            , avatar: null
                            , name: null
                        }
                    }
                    , queen: {
                        user: {
                            id: null
                            , avatar: null
                            , name: null
                        }
                    }
                    , jack: {
                        user: {
                            id: null
                            , avatar: null
                            , name: null
                        }
                    }
                    , others: null
                }
                , lastRefreshed: null
                , not_played: []
                , fresh_user: null
            }
            , methods: {
                userProfile: function(other) {
                    location.href="/users/"+other.user.id;
                }
            }
            , mounted () {
                axios
                  .get('{{route("api.leaderboard")}}')
                  .then(response => {
                    this.rankList = response.data.rank_list
                    this.lastRefreshed = response.data.updated_at
                    this.not_played = response.data.not_played
                    this.fresh_user = response.data.fresh_user
                    console.log(response.data);
                  });
              }
        });
    </script>
@endpush
