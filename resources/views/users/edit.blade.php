@extends('layouts.authenticated')
@section('title')
Edit {{$user->name}}
@endsection
@section('main')
<div class="container" id="userProfileUpdate">
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-3">
                <div class="card-header">Edit @{{name}}'s Profile</div>

                <div class="card-body">
                    @include('layouts.partials.status.custom',['panel'=>'user.edit'])
                     <form id="profile-update-form" method="POST" novalidate="novalidate" action="{{ route('users.show',['user'=>$user->id]) }}" enctype="multipart/form-data">
                        @csrf
                        {{ method_field('PUT') }}
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>
                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" v-model="name" value="{{ old('name',$user->name) }}"  autofocus required>
                                        <span class="invalid-feedback">
                                            @if ($errors->has('name'))
                                                <strong>{{ $errors->first('name') }}</strong>
                                            @endif
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">Email</label>

                                    <div class="col-md-6">
                                        <p class="form-control-plaintext">{{$user->email}}</p>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">Gender</label>

                                    <div class="col-md-6">
                                        <p class="form-control-plaintext">{{$user->gender}}</p>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="avatar" class="col-md-4 col-form-label text-md-right">{{__('Avatar')}}</label>
                                    <div class="col-md-6">
                                        <input type="file" name="avatar" id="avatar" accept="image/*">
                                        <span class="form-text text-muted">
                                            {{__('Required only if you wish to change current profile picture.')}} 
                                            @if ($errors->has('avatar'))
                                                <strong>{{ $errors->first('avatar') }}</strong>
                                            @endif
                                        </span>
                                        <img src="" id="avatar-crop-space" width="100" />
                                        <input type="hidden" name="avatar-data" id="avatar-data">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password" class="col-md-4 col-form-label text-md-right">New Password</label>

                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" v-model="password" >
                                        <span class="form-text text-muted">Leave blank if you don't want to change password</span>
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row" v-if="passwordChanged">
                                    <label for="password_confirmation" class="col-md-4 col-form-label text-md-right">Confirm Password</label>

                                    <div class="col-md-6">
                                        <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" placeholder="{{__('Leave blank if you don\'t want to change password')}}" >
                                        @if ($errors->has('password_confirmation'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                @if($user->id!=auth()->user()->id && !$user->isSuperAdmin())
                                    <fieldset>
                                        <legend>Admin</legend>
                                        @can('user_suspend')
                                            <div class="form-group row">
                                                <div class="col-sm-offset-4 col-sm-8">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" name="suspended" value="1" @if($user->suspended) checked @endif> Suspended
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        @endcan
                                        @can('user_verify')
                                            <div class="form-group row">
                                                <div class="col-sm-offset-4 col-sm-8">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" name="verified" value="1" @if($user->verified) checked disabled @endif> Verified
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        @endcan
                                    </fieldset>
                                    @can('user_update_role')
                                        <fieldset>
                                            <legend>Roles</legend>
                                            <div class="form-group row">
                                                <div class="col-sm-offset-4 col-sm-8">
                                                    @foreach(App\Role::get() as $role)
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" name="role[]" value="{{$role->id}}" @if($user->hasRole($role->name)) checked @endif> {{$role->label}}
                                                        </label>
                                                    </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </fieldset>
                                    @endcan
                                    <hr>
                                @endif
                                <div class="form-group row">
                                    <label for="current_password" class="col-md-4 col-form-label text-md-right">Current Password</label>

                                    <div class="col-md-6">
                                        <input id="current_password" type="password" class="form-control{{ $errors->has('current_password') ? ' is-invalid' : '' }}" name="current_password" required placeholder="{{__('Required for saving changes to your profile.')}}">
                                        @if ($errors->has('current_password'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('current_password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-8 offset-md-4">
                                        <button type="submit" id="save-profile"   class="btn btn-primary">
                                            Update Profile
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 d-none d-md-block">
                                <div>
                                    <div class="avatar-preview rounded-circle" style="overflow: hidden; width: 140px; height: 140px; background: url('//via.placeholder.com/140x140/ffffff/?text=Preview')"></div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
           </div>
           @can('delete', $user)
               <div class="card mt-3 border border-danger ">
                   <div class="card-header bg-danger text-white">Delete Account</div>
                   <div class="card-body">
                       <p class="card-text">
                        @include('layouts.partials.status.custom',['panel'=>'user.delete'])
                            <h1 class="text-danger">This is a distructive action!!</h1>
                            <p class="text-info">This action will remove your user account with {{config('app.name')}}.</p>
                            <p class="text-muted">Though your account will be deleted, your bets and any other transactiosn will still be recorded but anonymous. Once you delete, you will <u>not</u> be able to link it back again.</p>
                        </p>
                            <p>To delete your user account, please enter your password below:</p>
                        </p>
                        <form class="form-horizontal" novalidate="novalidate" method="post" action="{{ route('users.destroy',['user'=>$user->id]) }}">
                            @csrf
                            {{ method_field('DELETE') }}
                            <div class="form-group row">
                                <label for="delete_password" class="col-md-4 col-form-label text-md-right">Password</label>

                                <div class="col-md-6">
                                    <input id="delete_password" type="password" class="form-control{{ $errors->has('delete_password') ? ' is-invalid' : '' }}" name="delete_password" required>
                                    @if ($errors->has('delete_password'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('delete_password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <p class="form-text text-danger">
                                There will not be another confirmation
                            </p>
                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-danger">
                                        Delete Account
                                    </button>
                                </div>
                            </div>
                        </form>
                   </div>
               </div>
            @endcan
        </div>
    </div>
</div>
@endsection
@push('bottomJs')
    <script type="text/javascript">
        const userProfileUpdate = new Vue({
            el: '#userProfileUpdate'
            , data: {
                 password: ''
                , name: "{{old('name', $user->name)}}"
            }
            , computed: {
                passwordChanged: function() {
                    return this.password.length;
                }
            }
        });
    </script>
@endpush
@section('breadcrumbs', Breadcrumbs::render('users.edit', $user))
@push('topCss')
    <link rel="stylesheet" type="text/css" href="{{ mix('css/cropperjs.css') }}">
@endpush

@push('bottomJs')
    <script type="text/javascript">

        $(document).ready(function(){

            PathramCropper.init();
            PathramCropper.setInputImage('#avatar');
            PathramCropper.setAspectRatio(1 / 1);
            PathramCropper.setImagePreview('.avatar-preview');
            PathramCropper.setCropSpace('avatar-crop-space');
            PathramCropper.activate();

            $("#save-profile").click(function(e)
            {
                e.preventDefault();

                $("#avatar").val(''); // i don't need the file anymore

                try
                {
                    $("#avatar-data").val(PathramCropper.getImage());                
                }
                catch(Exception)
                {
                    ;
                }
                $("#profile-update-form").submit();
            });

        });

    </script>
@endpush