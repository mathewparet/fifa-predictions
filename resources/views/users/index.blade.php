@extends('layouts.authenticated')
@section('title')
User Profiles
@endsection
@section('main')
<div class="container">
    <div class="row">
        <div class="col-md">
            <div class="card">
                <div class="card-header">User Profiles</div>

                <div class="card-body">
                    <div class="card-text d-block d-md-none">
                        @component('layouts.components.alert')
                            @slot('type','info')
                            Want to see more info? Just rotate your screen to ladnscape :)
                        @endcomponent
                    </div>
                     <table class="table table-hover">
                         <thead>
                            <tr>
                                <th>Name</th>
                                <th class="d-none d-md-table-cell">Email</th>
                                <th class="d-none d-lg-table-cell">Status</th>
                                <th class="d-none d-lg-table-cell">Created</th>
                                <th class="d-none d-md-table-cell">Actions</th>
                            </tr>
                        </thead>
                     	<tbody>
                     		@forelse($users as $user)
                                <tr>
                                    <td>
                                        <img src="{{$user->avatar}}" class="rounded-circle" height="32" width="32"> &nbsp; <a href="{{ route('users.show',['user'=>$user->id]) }}" title="{{$user->name}}">{{ str_limit($user->name,10) }}</a></td>
                                    <td class="d-none d-md-table-cell">
                                        <a href="mailto:{{$user->email}}" title="{{$user->email}}">{{str_limit($user->email, 10)}}</a>
                                    </td>
                                    <td class="d-none d-lg-table-cell">
                                        <span class="badge @if($user->suspended == true) badge-danger @else badge-success @endif">@if($user->suspended == true) Suspended @else Active @endif</span>&nbsp; <span class="badge @if($user->verified == true) badge-success @else badge-danger @endif">@if($user->verified == true) Verified @else Pending @endif</span>
                                    </td>
                                    <td class="d-none d-lg-table-cell">
                                        {{$user->created_at->diffInDays()>1?$user->created_at->format(config('app.long_date_format')):$user->created_at->diffForHumans()}}
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <div class="btn-group">
                                            @if(!$user->isSuperAdmin())
                                                @can('update',$user)
                                                    <a class="btn btn-sm btn-secondary" href="{{route('users.edit',['user'=>$user->id])}}">
                                                        <span class="fas fa-user-edit"></span>
                                                    </a>
                                                @endcan
                                                @can('user_impersonate')
                                                    <a class="btn btn-sm btn-secondary" href="{{route('users.switch',['user'=>$user->id])}}" title="Switch User">
                                                        <span class="fas fa-users"></span>
                                                    </a>
                                                @endcan
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan='6' class="text-center">No users</td>
                                </tr>
                            @endforelse
                     	</tbody>
                     </table>
                     <div class="text-center">
                         {{ $users->links() }}
                     </div>
                </div>
                @can('user_stats')
                    <div class="card-footer">
                        [ <a href="{{route('users.stats')}}">Statistics</a> ]
                    </div>
                @endcan
           </div>
        </div>
    </div>
</div>
@endsection
@section('breadcrumbs', Breadcrumbs::render('users.index'))
