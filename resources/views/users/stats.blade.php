@extends('layouts.authenticated')
@section('title')
User Stats
@endsection
@section('main')
<div class="container">
    <div class="row" id="pointsApp">
        <div class="col-md mb-3">
            <div class="card text-white bg-primary mb-3">
                <div class="card-header">Total Users</div>
                <div class="card-body">
                    <h3>
                        <animate-number
                          from="0" 
                          to="{{App\User::count()}}" 
                          duration="1000" 
                          easing="easeOutQuad"
                          :formatter="formatter"
                        >                                                
                        </animate-number>
                    </h3>
                </div>
            </div>
        </div>
        <div class="col-md mb-3">
            <div class="card text-white bg-success mb-3">
                <div class="card-header">Verified Users</div>
                <div class="card-body">
                    <h3>
                        <animate-number
                          from="0" 
                          to="{{App\User::verified()->count()}}" 
                          duration="1000" 
                          easing="easeOutQuad"
                          :formatter="formatter"
                        >                                                
                        </animate-number>
                    </h3>
                </div>
            </div>
        </div>
        <div class="col-md mb-3">
            <div class="card text-white bg-danger mb-3">
                <div class="card-header">Pending Verification</div>
                <div class="card-body">
                    <h3>
                        <animate-number
                          from="0" 
                          to="{{App\User::pending()->count()}}" 
                          duration="1000" 
                          easing="easeOutQuad"
                          :formatter="formatter"
                        >                                                
                        </animate-number>
                    </h3>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('breadcrumbs', Breadcrumbs::render('users.stats'))
