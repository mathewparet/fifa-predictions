@extends('layouts.authenticated')
@section('title',$user->name)
@section('main')
<div class="container">
    <div class="row">
        <div class="col-md">
            <div class="card">
                <div class="card-header">User Accout Settings</div>
                <div class="card-body">

                     <!-- Let's load the table -->
                     <div class="row">
                        <div class="col-md">
                            @include('layouts.partials.status.custom',['panel'=>'user.show'])
                            <dl class="dl-horizontal">
                                <div class="row mb-3">
                                    <div class="col-md">
                                      <div class="text-center"><img src="{{$user->avatar}}" class="rounded-circle" height="140" width="140">@if($user->avatar)<br><a href="{{route('users.deleteAvatar', ['user'=>$user->id])}}">[ {{__('Delete')}} ]</a>@endif</div>
                                    </div>
                                    <div class="col-md">
                                        <dt>Name</dt>
                                        <dd>{{$user->name}} / {{$user->gender}}</dd>
                                        <dt>{{__('Email')}}</dt>
                                        <dd><a href="mailto:{{$user->email}}" title="{{$user->email}}">{{str_limit($user->email, 20)}}</a></dd>
                                    </div>
                                    <div class="col-md">
                                        <dt>{{__('Created')}}</dt>
                                        <dd>{{$user->created_at->diffInDays()>1?$user->created_at->format(config('app.long_date_format')):$user->created_at->diffForHumans()}}</dd>
                                        <dt>{{__('Last Updated')}}</dt>
                                        <dd>{{$user->updated_at->diffInDays()>1?$user->updated_at->format(config('app.long_date_format')):$user->updated_at->diffForHumans()}}</dd>
                                    </div>
                                </div>
                                <div class="row mb-3" id="pointsApp">
                                    <div class="col-6 col-sm">
                                        <h3><span class="fas fa-coins text-warning">&nbsp;</span> 
                                            <animate-number
                                              from="0" 
                                              to="{{$user->points_in_prediction}}" 
                                              duration="1000" 
                                              easing="easeOutQuad"
                                              :formatter="formatter"
                                            >                                                
                                            </animate-number>
                                        </h3>
                                    </div>
                                    <div class="col-6 col-sm">
                                        <h3><span class="fas fa-coins text-primary">&nbsp;</span> 
                                            <animate-number
                                              from="0" 
                                              to="{{$user->wallet_points}}" 
                                              duration="1000" 
                                              easing="easeOutQuad"
                                              :formatter="formatter"
                                            >                                                
                                            </animate-number>
                                        </h3>
                                    </div>
                                    <div class="col-6 col-sm">
                                        <h3><span class="fas fa-coins text-success">&nbsp;</span> 
                                            <animate-number
                                              from="0" 
                                              to="{{$user->total_points}}" 
                                              duration="1000" 
                                              easing="easeOutQuad"
                                              :formatter="formatter"
                                            >                                                
                                            </animate-number>
                                        </h3>
                                    </div>
                                    <div class="col-6 col-sm">
                                        <h3><span class="fas fa-crown text-warning">&nbsp;</span> 
                                          @if(is_numeric($user->rank))
                                                <animate-number
                                                  from="0" 
                                                  to="{{$user->rank}}" 
                                                  duration="1000" 
                                                  easing="easeOutQuad"
                                                  :formatter="formatter"
                                                >                                                
                                                </animate-number>
                                          @else
                                            {{ucfirst($user->rank)}}
                                          @endif
                                        </h3>
                                    </div>
                                    <div class="col-6 col-sm">
                                        <h3><span class="fas fa-trophy text-warning text-warning">&nbsp;</span> 
                                            <animate-number
                                              from="0" 
                                              to="{{$user->total_wins}}" 
                                              duration="1000" 
                                              easing="easeOutQuad"
                                              :formatter="formatter"
                                            >                                                
                                            </animate-number>
                                        </h3>
                                    </div>
                                </div>
                            </dl>
                            @can('user_update_profile')
                                <fieldset><legend>{{__('Admin')}}</legend>
                                    <dl class="dl-horizontal">
                                        <dt>{{__('Status')}}</dt>
                                        <dd><span class="badge @if($user->suspended == true) badge-danger @else badge-success @endif">@if($user->suspended == true) {{__('Suspended')}} @else {{__('Active')}} @endif</span>&nbsp; <span class="badge @if($user->verified == true) badge-success @else badge-danger @endif">@if($user->verified == true) {{__('Verified')}} @else {{__('Pending')}} @endif</span></dd>
                                    </dl>
                                </fieldset>
                                @if($user->roles->count())
                                    <fieldset><legend>{{__('Roles')}}</legend>
                                        <ul>
                                        @foreach($user->roles as $role)
                                            <li>{{$role->label}}</li>
                                        @endforeach
                                        </ul>
                                    </fieldset>
                                @endif
                            @endcan
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    @can('update',$user)
                        <a href="{{route('users.edit', ['user'=>$user->id])}}">[ Update ]</a> 
                    @endcan
                    @can('show', App\Point::class)
                        <a href="{{route('users.points.index', ['user'=>$user->id])}}">[ <span class="fas fa-coins"></span> Points History ]</a> 
                    @endcan
                    @can('user_impersonate')
                      <a href="{{route('users.switch',['user'=>$user->id])}}">[ Switch User ]</a>
                    @endcan
                </div>
           </div>
        </div>
    </div>
</div>
@endsection

@section('breadcrumbs', Breadcrumbs::render('users.show', $user))
