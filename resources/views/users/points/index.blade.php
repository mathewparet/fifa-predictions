@extends('layouts.authenticated')
@section('title')
Points History
@endsection
@section('main')
<div class="container">
    <div class="row">
        <div class="col-md">
            <div class="card">
                <div class="card-header">Points History</div>

                <div class="card-body">
                    <div class="card-text d-block d-sm-none">
                        @component('layouts.components.alert')
                            @slot('type','info')
                            Can't see running balance? Just tilt you phone :)
                        @endcomponent
                    </div>

                     <!-- Let's load the table -->
                     <table class="table table-hover">
                         <thead>
                            <tr>
                                <th class="d-none d-sm-table-cell">Date</th>
                                <th>Comments</th>
                                <th class="text-right">Credit</th>
                                <th class="text-right">Debit</th>
                                <th class="text-right d-none d-sm-table-cell">Balance</th>
                            </tr>
                        </thead>
                     	<tbody>
                     		@forelse($points as $point)
                                <tr>
                                    <td class="d-none d-sm-table-cell">{{$point->updated_at->format(config('app.long_date_format'))}}</td>
                                    <td>{{$point->comments}}</td>
                                    <td class="text-right">{{number_format($point->credit)}}</td>
                                    <td class="text-right">{{number_format($point->debit)}}</td>
                                    <td class="text-right d-none d-sm-table-cell">{{number_format($point->balance)}}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan='6' class="text-center">No point history :(</td>
                                </tr>
                            @endforelse
                     	</tbody>
                     </table>
                     <div class="text-center">
                         {{ $points->links() }}
                     </div>
                </div>
           </div>
        </div>
    </div>
</div>
@endsection
@section('breadcrumbs', Breadcrumbs::render('users.points.index', $user))
