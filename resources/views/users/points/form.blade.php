@extends('layouts.authenticated')
@section('title')
Get More Points
@endsection
@section('main')

    <div class="row" id="grantPointsApp">
        <div class="col-md-12">
            <div class="card mb-3">
                <div class="card-header">Exchange points for wins</div>
                <div class="card-body">
                    @include('layouts.partials.status.custom',['panel'=>'users.points.buy.form'])
                    <form novalidate="novalidate" class="form-horizontal" method="POST" action="{{ route('users.points.buy', ['user'=>$user->id]) }}">
                        @csrf

                        @component('layouts.components.alert')
                            @slot('type','warning')
                            Think deep before exchanging your wins for points, because:
                            <ul>
                                <li>Each wins exchanged gives you one <u>less</u> chance to be the Top Predictor (Top Predictor is based on the number of wins; points do not matter for this title).</li>
                                <li>Wins are used as tie breakers. If more than one contestant has the same points, then the number of wins are used as a tie breaker among them (the higher the number of wins, the higher his rank would be. E.g - if A and B has 1000 points at the end of the game; A has 12 wins and B has 15 wins, then B would hold the better rank.</li>
                            </ul>
                        @endcomponent

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Exchange Rate</label>

                            <div class="col-md-6">
                                <p class="form-control-plaintext">1 <i class="fas fa-trophy text-warning"></i> = @{{exchangeRatePerWin}} <span class="fas fa-coins text-warning"></span></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Sellable Wins</label>

                            <div class="col-md-6">
                                <p class="form-control-plaintext">@{{availableWins}}</p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="winsSelling" class="col-md-4 col-form-label text-md-right">Wins to Exchange</label>

                            <div class="col-md-6">
                                <div class="input-group">
                                    <input id="winsSelling" type="number" class="text-right form-control{{ $errors->has('winsSelling') ? ' is-invalid' : '' }}" name="winsSelling" v-model="winsSelling"  required min=1 step="1" max="{{$user->total_wins}}" autofocus>
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="fas fa-trophy"></i></span>
                                    </div>
                                </div>
                                @if ($errors->has('winsSelling'))
                                    <span class="invalid-feedback" style="display: block;">
                                        <strong>{{ $errors->first('winsSelling') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Points you get</label>

                            <div class="col-md-6">
                                <p class="form-control-plaintext">@{{computedPoints}}</p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Points you have</label>

                            <div class="col-md-6">
                                <p class="form-control-plaintext">@{{pointsYouHave}}</p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Total Points</label>

                            <div class="col-md-6">
                                <p class="form-control-plaintext">@{{totalPoints}}</p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Exchange Now
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
           </div>
        </div>
    </div>

@endsection

@section('breadcrumbs', Breadcrumbs::render('users.points.buy', $user))

@push('bottomJs')
    <script type="text/javascript">
        const grantPointsApp = new Vue({
            el: '#grantPointsApp'
            , data: {
                availableWins: {{$user->total_wins ? : 0}}
                , exchangeRatePerWin: {{config('app.fifa.exchange.rate', 100)}}
                , winsSelling: {{old('winsSelling',0)}}
                , pointsYouHave: {{$user->wallet_points}}
            }
            , computed: {
                computedPoints: function()
                {
                    return this.exchangeRatePerWin * this.winsSelling;
                }
                , totalPoints: function()
                {
                    return this.computedPoints + this.pointsYouHave;
                }
            }
        });
    </script>
@endpush
