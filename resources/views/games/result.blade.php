@extends('layouts.authenticated')
@section('title')
Game Results
@endsection
@section('main')

    <div class="row" id="resultApp">
        <div class="col-md-12">
            <div class="card mb-3">
                <div class="card-header">Game Result</div>
                <div class="card-body">
                    @include('layouts.partials.status.custom',['panel'=>'games.results'])
                    @if(!blank($result->type))
                        @component('layouts.components.alert')
                            @slot('type','warning')
                            The results for this game has were already published on {{$result->updated_at}}. By continuing you will be editing the results and updating the result.
                        @endcomponent
                    @endif
                    <form novalidate class="form-horizontal" method="POST" action="{{ route('games.result.save', ['game'=>$game->id]) }}">
                        {{ method_field('PUT') }}
                        @csrf
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Match Schedule</label>

                            <div class="col-md-6">
                                <p class="form-control-plaintext">
                                    {{$game->scheduled_at->diffInDays()>1?$game->scheduled_at->format(config('app.long_date_format')):$game->scheduled_at->diffForHumans()}}
                                </p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Match Type</label>

                            <div class="col-md-6">
                                <p class="form-control-plaintext">
                                    {{$game->type}}
                                </p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Winner <i class="fas fa-trophy"></i></label>
                            <div class="col-md-6">
                                <span class="{{ $errors->has('winner') ? ' is-invalid' : '' }}">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="winner" id="winner_0" value="{{$game->teams[0]->id}}" v-model="winner">
                                            {{$game->teams[0]->name}} <img src="{{Storage::url($game->teams[0]->flag)}}">
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="winner" id="winner_1" value="{{$game->teams[1]->id}}"  v-model="winner">
                                            {{$game->teams[1]->name}} <img src="{{Storage::url($game->teams[1]->flag)}}">
                                        </label>
                                    </div>

                                    @if($game->type == 'Group')
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="winner" id="winner_draw" value="Draw"  v-model="winner">
                                                Draw <span class="fas fa-handshake"></span>
                                            </label>
                                        </div>
                                    @endif
                                </span>
                                @if ($errors->has('winner'))
                                    <span class="invalid-feedback" style="display: block;">
                                        <strong>{{ $errors->first('winner') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        @if($game->type != 'Group')
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">Winning Time <i class="far fa-clock"></i></label>
                                <div class="col-md-6">
                                    <span class="{{ $errors->has('time') ? ' is-invalid' : '' }}">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="time" id="time_normal" value="Normal Time" v-model="time">
                                                Normal Time
                                            </label>
                                        </div>

                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="time" id="time_extra" value="Extra Time" v-model="time">
                                                Extra Time
                                            </label>
                                        </div>

                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="time" id="time_penalty" value="Penalty Shootout" v-model="time">
                                                Penalty
                                            </label>
                                        </div>
                                    </span>
                                    @if ($errors->has('time'))
                                        <span class="invalid-feedback" style="display: block;">
                                            <strong>{{ $errors->first('time') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        @endif
                        <div class="form-group row">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Save Result
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
           </div>
        </div>
    </div>

@endsection

@section('breadcrumbs', Breadcrumbs::render('games.result', $game))

@push('bottomJs')
    <script type="text/javascript">
        const resultApp = new Vue({
            el: '#resultApp'
            , data: {
                winner: "{{old('winner', $result->type=='Draw' ? 'Draw' : $result->team_id)}}"
                @if($game->type=='Knockout') 
                , time: "{{old('time', $result->time)}}"
                @endif
            }
        });
    </script>
@endpush
