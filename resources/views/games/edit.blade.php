@extends('layouts.authenticated')
@section('title')
Edit Game
@endsection
@section('main')

    <div class="row" id="gameApp">
        <div class="col-md">
            <div class="card mb-3">
                <div class="card-header">Edit Game - {{$game->teams[0]->name}} vs {{$game->teams[1]->name}}</div>

                <div class="card-body">
                     <form class="form-horizontal" method="POST" action="{{ route('games.update',['game'=>$game->id]) }}">
                        @csrf
                        {{ method_field('PUT') }}

                        <div class="form-group row">
                            <label for="type" class="col-md-4 col-form-label text-md-right">Game Type</label>

                            <div class="col-md-6">
                                <p class="form-control-plaintext">{{$game->type}}</p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="type" class="col-md-4 col-form-label text-md-right">Current Schedule</label>

                            <div class="col-md-6">
                                <p class="form-control-plaintext">
                                    @if($game->scheduled_at)
                                        {{$game->scheduled_at->format(config('app.long_date_format'))}}
                                    @else
                                        No Schedule Defined
                                    @endif
                                </p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="scheduled_at" class="col-md-4 col-form-label text-md-right">New Schedule</label>

                            <div class="col-md-6">
                                <input id="scheduled_at" type="datetime-local" class="form-control{{ $errors->has('scheduled_at') ? ' is-invalid' : '' }}" name="scheduled_at">
                                @if ($errors->has('scheduled_at'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('scheduled_at') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                   Update Game
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
           </div>
        </div>
    </div>

@endsection

@section('breadcrumbs', Breadcrumbs::render('games.edit',$game))