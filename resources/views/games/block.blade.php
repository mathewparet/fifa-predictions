<div class="card mb-3" v-bind:class="{'border-success': game.hasPredicted && game.isOver && game.result && hasWon(game), 'border-danger': game.hasPredicted && game.isOver && game.result && !hasWon(game), 'border-warning': game.hasPredicted && game.isOver && !game.result, 'border-primary': game.hasPredicted && !game.isOver}">
    <div class="card-header">
        <h5>
            <span v-if="game.scheduled_at">@{{game.scheduled_at}}</span>
            <span class="badge badge-pill badge-primary" v-else>Draft</span>
        </h5>
    </div>
    <div class="card-body">
        <div class="card-text text-center align-items-center">
            <div class="row">
                <div class="col-12">
                    <span class="badge badge-pill badge-secondary">@{{game.type}}</span>
                </div>
            </div>
            <div class="row">
                <div class="col"><img v-bind:src="'{{Storage::url('')}}'+game.teams[0].flag"></div>
                <div class="col-1"></div>
                <div class="col"><img v-bind:src="'{{Storage::url('')}}'+game.teams[1].flag"></div>
            </div>
            <div class="row">
                <div class="col">@{{game.teams[0].name}}</div>
                <div class="col-1"></div>
                <div class="col">@{{game.teams[1].name}}</div>
            </div>
            <div class="row">
                <div class="col">
                    <i class="fas fa-trophy text-warning" v-if="game.result && game.result.team_id == game.teams[0].id"></i>
                </div>
                <div class="col">
                    <span class="fas fa-handshake text-primary" v-if="game.result && game.result.type == 'Draw'"></span>
                </div>
                <div class="col">
                    <i class="fas fa-trophy text-warning" v-if="game.result && game.result.team_id == game.teams[1].id"></i> 
                </div>
            </div>
            <div class="row" v-if="game.type == 'Knockout' && game.result && game.result.time">
                <div class="col-12">
                    <span class="far fa-clock"></span> @{{game.result.time}}
                </div>
            </div>
        </div>
    </div>
    <div class="card-footer bg-transparent text-center align-items-center">
        <div class="btn-group btn-block">
            <a v-if="hasPredicted(game)" class="btn btn-block " v-bind:class="{ 'btn-outline-primary': game.isPredictable, 'btn-outline-secondary disabled': !game.isPredictable }" href="#" @click.prevent="savePrediction(game)" ><span class="fas fa-coins"></span> @{{game.prediction.point.points}} on 
                <span v-if="getPrediction(game).type == 'Win'">@{{game.prediction.team.name}}</span>
                &nbsp;<span v-if="game.prediction.type != 'Win'" class="fas fa-handshake"></span>
                &nbsp;<span v-if="game.prediction.time" class="far fa-clock" v-bind:title="game.prediction.time"></span>
                </a>
            <a v-else class="btn btn-block " v-bind:class="{ 'btn-outline-primary': game.isPredictable, 'btn-outline-secondary disabled': !game.isPredictable }" href="#" @click.prevent="savePrediction(game)" ><span class="fas fa-coins"></span> Predict</a>
            @can('manage', App\Game::class)
                <button type="button" class="btn btn-outline-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                </button>
                <div class="dropdown-menu">
                    @can('update', App\Game::class)
                        <a class="dropdown-item" href="#" @click.prevent="updateGame(game)"><span class="fas fa-clock"></span> Edit Schedule</a>
                    @endcan
                    @can('update_result', App\Game::class)
                        <a class="dropdown-item" href="#" v-if="game.isOver" @click.prevent="updateResult(game)"><span class="fas fa-trophy"></span> Update Result</a>
                    @endcan
                    @can('delete', App\Game::class)
                        <div class="dropdown-divider"></div>
                        <form v-bind:id="'delete-game-'+game.id" v-bind:action="'games/'+game.id" method="POST" style="display:none;">
                            @csrf
                            {{ method_field('DELETE') }}
                        </form>
                        <a class="dropdown-item" href="#" @click.prevent="deleteGame(game)"><span class="fas fa-trash"></span> Delete</a>
                    @endcan
                </div>
            @endcan
        </div>
    </div>
</div>