@extends('layouts.authenticated')
@section('title')
Add Game
@endsection
@section('main')

    <div class="row" id="gameApp">
        <div class="col-md-12">
            <div class="card mb-3">
                <div class="card-header">Add Game</div>
                <div class="card-body">
                    @include('layouts.partials.status.custom',['panel'=>'games.create'])
                    <form novalidate class="form-horizontal" method="POST" action="{{ route('games.store') }}">
                        @csrf

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <span class="{{ $errors->has('email') ? ' is-invalid' : '' }}">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input required class="form-check-input" @if(old('type') == 'Group') checked @endif type="radio" name="type" id="typeGroup" value="Group" autofocus>
                                             Group
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input required class="form-check-input" @if(old('type') == 'Knockout') checked @endif type="radio" name="type" id="typeKnockout" value="Knockout" >
                                             Knockout
                                        </label>
                                    </div>
                                </span>
                                @if ($errors->has('type'))
                                    <span class="invalid-feedback" style="display: block;">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="team_a" class="col-md-4 col-form-label text-md-right">Team A</label>

                            <div class="col-md-6">
                                <input type="hidden" name="team[a][id]" v-model="teamA.id">
                                <input type="hidden" name="team[a][flag]" v-model="teamA.flag">
                                <div class="input-group">
                                    <input id="teamA" type="text" class="form-control" v-bind:class="{ 'is-invalid': teamA.hasError }" name="team[a][name]" v-model="teamA.name" required>
                                    <div class="input-group-append">
                                        <span class="input-group-text"><img ref='teamAFlag' v-bind:src="teamA.flag"></span>
                                    </div>
                                </div>
                                <span class="invalid-feedback" style="display: block;">
                                    <strong>@{{teamA.errorText}}</strong>
                                </span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="team_b" class="col-md-4 col-form-label text-md-right">Team B</label>

                            <div class="col-md-6">
                                <input type="hidden" name="team[b][id]" v-model="teamB.id">
                                <input type="hidden" name="team[b][flag]" v-model="teamB.flag">
                                <div class="input-group">
                                    <input id="teamB" type="text" class="form-control" v-bind:class="{ 'is-invalid': teamB.hasError }" name="team[b][name]" v-model="teamB.name" required>
                                    <div class="input-group-append">
                                        <span class="input-group-text"><img ref='teamBFlag' v-bind:src="teamB.flag"></span>
                                    </div>
                                </div>
                                <span class="invalid-feedback" style="display: block;">
                                    <strong>@{{teamB.errorText}}</strong>
                                </span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="scheduled_at" class="col-md-4 col-form-label text-md-right">Schedule</label>

                            <div class="col-md-6">
                                <input id="scheduled_at" type="datetime-local" class="form-control{{ $errors->has('scheduled_at') ? ' is-invalid' : '' }}" name="scheduled_at" v-model="scheduled_at">
                                @if ($errors->has('scheduled_at'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('scheduled_at') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Add Game
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
           </div>
        </div>
    </div>

@endsection

@section('breadcrumbs', Breadcrumbs::render('games.create'))
@push('topCss')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />
@endpush
@push('bottomJs')
    <script type="text/javascript">
        Vue.config.devtools = true;
        var gameApp = new Vue({
            el: '#gameApp'
            , data: {
                teamA: {
                        id: {{old('team.a.id','null')}}
                        , name: "{{old('team.a.name',null)}}"
                        , flag: "{{old('team.a.flag',null)}}"
                        , hasError: {{$errors->has('team.a.id') ? : 'false'}}
                        , errorText: "{{$errors->first('team.a.id') ? : null}}"
                    }
                , teamB: {
                        id: {{old('team.b.id','null')}}
                        , name: "{{old('team.b.name',null)}}"
                        , flag: "{{old('team.b.flag',null)}}"
                        , hasError: {{$errors->has('team.b.id') ? : 'false'}}
                        , errorText: "{{$errors->first('team.b.id') ? : null}}"
                    }
                , scheduled_at: "{{old('scheduled_at')}}"
            }
        });
    </script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#teamA").autocomplete({
                source: function(request, response) {
                    $.ajax( {
                        url: '{{route("teams.list")}}'
                        , data: {
                            term: request.term
                            , not: window.gameApp.teamB.id
                        }
                        , success: function(data) {
                            response(data);
                        }
                    });
                }
                , minLength: 3
                , select: function(event, ui) {
                    console.log(ui.item);
                    window.gameApp.teamA.id = ui.item.id;
                    window.gameApp.teamA.name = ui.item.name;
                    window.gameApp.teamA.flag = ui.item.flag;
                }
            });
            $("#teamB").autocomplete({
                source: function(request, response) {
                    $.ajax( {
                        url: '{{route("teams.list")}}'
                        , data: {
                            term: request.term
                            , not: window.gameApp.teamA.id
                        }
                        , success: function(data) {
                            response(data);
                        }
                    });
                }
                , minLength: 3
                , select: function(event, ui) {
                    console.log(ui.item);
                    window.gameApp.teamB.id = ui.item.id;
                    window.gameApp.teamB.name = ui.item.name;
                    window.gameApp.teamB.flag = ui.item.flag;
                }
            });
        });
    </script>
@endpush
