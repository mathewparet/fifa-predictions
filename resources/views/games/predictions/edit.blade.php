@extends('layouts.authenticated')
@section('title')
Predict Results
@endsection
@section('main')

    <div class="row" id="pointCalc">
        <div class="col-md-12">
            <div class="card mb-3">
                <div class="card-header">Predict Results</div>
                <div class="card-body">
                    @include('layouts.partials.status.custom',['panel'=>'games.predictions.create'])
                    <form novalidate class="form-horizontal" method="POST" action="{{ route('games.predictions.update', ['game'=>$game->id, 'prediction'=>$prediction->id]) }}">
                        {{ method_field('PUT') }}
                        @csrf
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Match Schedule</label>

                            <div class="col-md-6">
                                <p class="form-control-plaintext">
                                    @if($game->scheduled_at)
                                        {{$game->scheduled_at->diffInDays()>1?$game->scheduled_at->format(config('app.long_date_format')):$game->scheduled_at->diffForHumans()}}
                                    @else
                                        <span class="badge badge-pill badge-primary">Draft</span>
                                    @endif
                                </p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Current Bet</label>

                            <div class="col-md-6">
                                <p class="form-control-plaintext">
                                    <span class="fas fa-coins text-warning">&nbsp;</span> 
                                        {{$prediction->point->points}}
                                </p>
                            </div>
                        </div>

                        <span>
                            <div class="form-group row">
                                <label for="points" class="col-md-4 col-form-label text-md-right">Points to Bet</label>

                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input id="points" type="number" class="text-right form-control{{ $errors->has('points') ? ' is-invalid' : '' }}" name="points" autofocus required min=0 max="{{auth()->user()->wallet_points + $prediction->point->points}}" step="1" v-model="points">
                                        <div class="input-group-append">
                                            <span class="input-group-text"> out of&nbsp;<span class="fas fa-coins text-primary">&nbsp;</span>&nbsp;@{{available_points}}</span>
                                        </div>
                                    </div>
                                    @if ($errors->has('points'))
                                        <span class="invalid-feedback" style="display: block;">
                                            <strong>{{ $errors->first('points') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">Points After Bet</label>

                                <div class="col-md-6">
                                    <p class="form-control-plaintext">
                                        <span class="fas fa-coins text-warning">&nbsp;</span> 
                                        @{{balance}}
                                    </p>
                                </div>
                            </div>
                        </span>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Predicted Winner</label>
                            <div class="col-md-6">
                                <span class="{{ $errors->has('team_id') ? ' is-invalid' : '' }}">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input class="form-check-input" v-model="team_id" type="radio" name="team_id" id="team_id_0" value="{{$game->teams[0]->id}}">
                                            {{$game->teams[0]->name}} <img src="{{Storage::url($game->teams[0]->flag)}}">
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input class="form-check-input" v-model="team_id" type="radio" name="team_id" id="team_id_1" value="{{$game->teams[1]->id}}" >
                                            {{$game->teams[1]->name}} <img src="{{Storage::url($game->teams[1]->flag)}}">
                                        </label>
                                    </div>
                                    @if($game->type == 'Group')
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" v-model="team_id" type="radio" name="team_id" id="team_id_draw" value="Draw" >
                                                Draw <span class="fas fa-handshake"></span>
                                            </label>
                                        </div>
                                    @endif
                                </span>
                                @if ($errors->has('team_id'))
                                    <span class="invalid-feedback" style="display: block;">
                                        <strong>{{ $errors->first('team_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                         @if($game->type != 'Group')
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">Winning Time <i class="far fa-clock"></i></label>
                                <div class="col-md-6">
                                    <span class="{{ $errors->has('time') ? ' is-invalid' : '' }}">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="time" id="time_normal" value="Normal Time" v-model="time">
                                                Normal Time
                                            </label>
                                        </div>

                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" value="Extra Time" name="time" id="time_extra"  v-model="time">
                                                Extra Time
                                            </label>
                                        </div>

                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" value="Penalty Shootout" name="time" id="time_penalty"  v-model="time">
                                                Penalty
                                            </label>
                                        </div>

                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" value="null" name="time" id="time_none" v-model="time">
                                                N/A
                                            </label>
                                        </div>
                                    </span>
                                    @if ($errors->has('time'))
                                        <span class="invalid-feedback" style="display: block;">
                                            <strong>{{ $errors->first('time') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        @endif

                        <div class="form-group row">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary" :disabled="cantSave">
                                    Update Prediction
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
           </div>
        </div>
    </div>

@endsection

@section('breadcrumbs', Breadcrumbs::render('games.predictions.edit', $prediction))

@push('bottomJs')
    <script type="text/javascript">
        Vue.config.devtools = true;
        const pointCalc = new Vue({
            el: '#pointCalc'
            , data: {
                available_points: {{auth()->user()->wallet_points}}+{{$prediction->point->points}}
                , points: {{old('points',$prediction->point->points)}}
                , team_id: "{{old('team_id', $prediction->type == 'Draw' ? 'Draw' : $prediction->team_id)}}"
                , time: "{{old('time', $prediction->time ?: 'null')}}"
            }
            , computed: {
                balance: function() {
                    return (parseInt(this.available_points) - parseInt(this.points));
                }
                , cantSave: function() {
                    return this.points==="";
                }
            }
        });
    </script>
@endpush
