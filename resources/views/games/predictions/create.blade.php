@extends('layouts.authenticated')
@section('title')
Predict Results
@endsection
@section('main')

    <div class="row">
        <div class="col-md-12">
            <div class="card mb-3">
                <div class="card-header">Predict Results</div>
                <div class="card-body">
                    @include('layouts.partials.status.custom',['panel'=>'games.predictions.create'])
                    <form novalidate class="form-horizontal" method="POST" action="{{ route('games.predictions.store', ['game'=>$game->id]) }}">
                        @csrf
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Match Schedule</label>

                            <div class="col-md-6">
                                <p class="form-control-plaintext">
                                    @if($game->scheduled_at)
                                        {{$game->scheduled_at->diffInDays()>1?$game->scheduled_at->format(config('app.long_date_format')):$game->scheduled_at->diffForHumans()}}
                                    @else
                                        <span class="badge badge-pill badge-primary">Draft</span>
                                    @endif
                                </p>
                            </div>
                        </div>

                        <span id="pointCalc">
                            <div class="form-group row">
                                <label for="points" class="col-md-4 col-form-label text-md-right">Points to Bet</label>

                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input id="points" type="number" class="text-right form-control{{ $errors->has('points') ? ' is-invalid' : '' }}" name="points" autofocus required min=0 max="{{auth()->user()->wallet_points}}" step="1" v-model="points">
                                        <div class="input-group-append">
                                            <span class="input-group-text"> out of&nbsp;<span class="fas fa-coins text-primary">&nbsp;</span>&nbsp;@{{available_points}}</span>
                                        </div>
                                    </div>
                                    @if ($errors->has('points'))
                                        <span class="invalid-feedback" style="display: block;">
                                            <strong>{{ $errors->first('points') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">Points After Bet</label>

                                <div class="col-md-6">
                                    <p class="form-control-plaintext">
                                        <span class="fas fa-coins text-warning">&nbsp;</span> 
                                        @{{balance}}
                                    </p>
                                </div>
                            </div>
                        </span>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Predicted Winner</label>
                            <div class="col-md-6">
                                <span class="{{ $errors->has('team_id') ? ' is-invalid' : '' }}">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input class="form-check-input" @if(old('team_id') == $game->teams[0]->id) checked @endif type="radio" name="team_id" id="team_id_0" value="{{$game->teams[0]->id}}">
                                            {{$game->teams[0]->name}} <img src="{{Storage::url($game->teams[0]->flag)}}">
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input class="form-check-input" @if(old('team_id') == $game->teams[1]->id) checked @endif type="radio" name="team_id" id="team_id_1" value="{{$game->teams[1]->id}}" >
                                            {{$game->teams[1]->name}} <img src="{{Storage::url($game->teams[1]->flag)}}">
                                        </label>
                                    </div>
                                    @if($game->type == 'Group')
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="team_id" id="team_id_draw" value="Draw" >
                                                Draw <span class="fas fa-handshake"></span>
                                            </label>
                                        </div>
                                    @endif
                                </span>
                                @if ($errors->has('team_id'))
                                    <span class="invalid-feedback" style="display: block;">
                                        <strong>{{ $errors->first('team_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        @if($game->type != 'Group')
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">Winning Time <i class="far fa-clock"></i></label>
                                <div class="col-md-6">
                                    <span class="{{ $errors->has('time') ? ' is-invalid' : '' }}">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="time" id="time_normal" value="Normal Time" {{ old('time') == 'Normal Time' ? 'checked' : '' }}>
                                                Normal Time
                                            </label>
                                        </div>

                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="time" id="time_extra" value="Extra Time" {{ old('time') == 'Extra Time' ? 'checked' : '' }}>
                                                Extra Time
                                            </label>
                                        </div>

                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="time" id="time_penalty" value="Penalty Shootout" {{ old('time') == 'Penalty Shootout' ? 'checked' : '' }}>
                                                Penalty
                                            </label>
                                        </div>

                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="time" id="time_penalty" value="null" {{ old('time') == 'null' ? 'checked' : '' }}> N/A
                                            </label>
                                        </div>
                                    </span>
                                    @if ($errors->has('time'))
                                        <span class="invalid-feedback" style="display: block;">
                                            <strong>{{ $errors->first('time') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        @endif

                        <div class="form-group row">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Save Prediction
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
           </div>
        </div>
    </div>

@endsection

@section('breadcrumbs', Breadcrumbs::render('games.predictions.create', $game))

@push('bottomJs')
    <script type="text/javascript">
        Vue.config.devtools = true;
        const pointCalc = new Vue({
            el: '#pointCalc'
            , data: {
                available_points: {{auth()->user()->wallet_points}}
                , points: {{old('points',0)}}
            }
            , computed: {
                balance: function() {
                    return (parseInt(this.available_points) - parseInt(this.points));
                }
            }
        });
    </script>
@endpush
