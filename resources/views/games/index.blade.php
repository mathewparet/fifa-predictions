@extends('layouts.authenticated')
@section('title')
Games
@endsection
@section('main')
<div class="container" id="gameApp">
    <div class="row">
        <div class="col-md">
            @include('layouts.partials.status.custom',['panel'=>'games.index'])
        </div>
    </div>

    <div class="row mb-3" id="group-list">
        <div class="col-md">
            <div class="card">
                <div class="card-body">
                    @can('create', App\Game::class) [ <a href="{{route('games.create')}}">New Game</a> ] @endcan
                    <span v-if="games.Future.length" >[ <a href="#upcoming-games">Upcoming Games <span class="badge badge-pill badge-primary">@{{games.Future.length}}</span> </a> ]</span>
                    <span v-if="games.Past.length" >[ <a href="#past-games">Past / Ongoing Games <span class="badge badge-pill badge-primary">@{{games.Past.length}}</span> </a> ]</span>
                    <span v-if="games.Draft.length" >[ <a href="#draft-games">Draft Games <span class="badge badge-pill badge-primary">@{{games.Draft.length}}</span> </a> ]</span>
                </div>
            </div>
        </div>
    </div>
    <content-loader v-if="!games.Future.length && !games.Past.length && !games.Draft.length" :speed="1" :animate="true">
    </content-loader>
    <span v-else>
        <div class="row" v-if="games.Future.length">
            <div class="col">
                <div class="row">
                    <div class="col">
                        <h4 id="upcoming-games">Upcoming Games</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-lg-4 card-deck ml-0 mr-0 pl-0 pr-0" v-for="game in gamesList.Future">
                        @include("games.block")
                    </div>
                </div>
            </div>
        </div>

        <div class="row" v-if="games.Past.length">
            <div class="col">
                <div class="row">
                    <div class="col">
                        <h4 id="past-games">Past / Ongoing Games</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-lg-4 card-deck ml-0 mr-0 pl-0 pr-0" v-for="game in gamesList.Past">
                        @include("games.block")
                    </div>
                </div>
            </div>
        </div>

        <div class="row" v-if="games.Draft.length">
            <div class="col">
                <div class="row">
                    <div class="col">
                        <h4 id="draft-games">Draft Games</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-lg-4 card-deck ml-0 mr-0 pl-0 pr-0" v-for="game in gamesList.Draft">
                        @include("games.block")
                    </div>
                </div>
            </div>
        </div>
    </span>
</div>
@endsection
@section('breadcrumbs', Breadcrumbs::render('games.index'))
@push('bottomJs')
    <script type="text/javascript">
        Vue.config.devtools = true;
        const gameApp = new Vue({
            el: '#gameApp'
            , data: {
                games: {
                    Past: []
                    , Future: []
                    , Draft: []
                }
                , predictions: []
                , currentTime: 0
            }
            , components: {
                ContentLoader: window.CodeLoader
            }
            , computed: {
                gamesList: function()
                {
                    this.games.Past.forEach((game) => {
                        game.scheduled_at = game.scheduled_at ? window.moment(game.scheduled_at).format('lll') : null
                        game.isOver= this.isOver(game);
                        game.hasPredicted= this.hasPredicted(game);
                        game.prediction = this.getPrediction(game);
                        game.isPredictable = this.isPredictable(game);
                        if(game.scheduled_at)
                        {
                            game.overAt = window.moment(game.scheduled_at).add({{config('app.fifa.result.cutoff',105)}});
                        }
                    });
                    
                    this.games.Future.forEach((game) => {
                        game.scheduled_at = game.scheduled_at ? window.moment(game.scheduled_at).format('lll') : null
                        game.isOver= this.isOver(game);
                        game.hasPredicted= this.hasPredicted(game);
                        game.prediction = this.getPrediction(game);
                        game.isPredictable = this.isPredictable(game);
                        if(game.scheduled_at)
                        {
                            game.overAt = window.moment(game.scheduled_at).add({{config('app.fifa.result.cutoff',105)}});
                        }
                    });
                    
                    this.games.Draft.forEach((game) => {
                        game.scheduled_at = game.scheduled_at ? window.moment(game.scheduled_at).format('lll') : null
                        game.isOver= this.isOver(game);
                        game.hasPredicted= this.hasPredicted(game);
                        game.prediction = this.getPrediction(game);
                        game.isPredictable = this.isPredictable(game);
                        if(game.scheduled_at)
                        {
                            game.overAt = window.moment(game.scheduled_at).add({{config('app.fifa.result.cutoff',105)}});
                        }
                    });
                    return this.games;
                }
            }
            , methods: {
                hasPredicted: function(game)
                {
                    if(this.predictions.find(o => o.game_id === game.id)) 
                        return true;
                    else
                        return false;
                }
                , hasWon: function(game)
                {
                    prediction = this.getPrediction(game);
                    return prediction.wins > 0;
                }
                , getPrediction: function(game)
                {
                    return this.predictions.find(o => o.game_id === game.id);
                }
                , isPredictable: function(game)
                { 
                    if(!game.scheduled_at)
                        return false;

                    return (window.moment(game.scheduled_at) > window.moment(this.getCurrentTimePlus({{config('app.fifa.prediction.cutoff',5)}})));

                }
                , updateGame: function(game)
                {
                    location.href = "/games/"+game.id+"/edit";
                }
                , updateResult: function(game)
                {
                    location.href = "/games/"+game.id+"/result";
                }
                , deleteGame: function(game)
                {
                    $("#delete-game-"+game.id).submit();
                }
                , savePrediction: function(game)
                {
                    if(this.getPrediction(game))
                        location.href = "/games/"+game.id+"/predictions/"+this.getPrediction(game).id+"/edit";
                    else
                        location.href = "/games/"+game.id+"/predictions/create";
                }
                , isOver: function(game)
                {
                    if(!game.scheduled_at)
                        return false;

                    return window.moment(game.scheduled_at) < window.moment(this.getCurrentTimeMinus({{config('app.fifa.result.cutoff',105)}}));
                }
                , getCurrentTimeMinus: function(minutes)
                {
                    momentOfTime = new Date(); // just for example, can be any other time
                    myTimeSpan = minutes*60*1000; // 5 minutes in milliseconds
                    momentOfTime.setTime(momentOfTime.getTime() - myTimeSpan);
                    return momentOfTime;
                }
                , getCurrentTimePlus: function(minutes)
                {
                    momentOfTime = new Date(); // just for example, can be any other time
                    myTimeSpan = minutes*60*1000; // 5 minutes in milliseconds
                    momentOfTime.setTime(momentOfTime.getTime() + myTimeSpan);
                    return momentOfTime;
                }
            }
            , mounted () {
                axios
                      .get('{{route("api.games.index",["api_token"=>auth()->user()->api_token])}}')
                      .then(response => {
                        if(response.data.games.Past)
                            gameApp.games.Past = response.data.games.Past;
                        if(response.data.games.Future)
                            gameApp.games.Future = response.data.games.Future;
                        if(response.data.games.Draft)
                            gameApp.games.Draft = response.data.games.Draft;
                        if(response.data.predictions)
                            gameApp.predictions = response.data.predictions;
                      });
            }
        });
    </script>
@endpush