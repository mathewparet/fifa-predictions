@component('mail::message')
# {{config('app.name')}} - Leaderboard

@component('mail::table')
| Rank       | Name         | Points  |  Wins  |
| :-------------: | :------------- |-------------:| --------:|
{{is_numeric($rank_list->get('king')['rank']) ? $rank_list->get('king')['rank'] : ucfirst($rank_list->get('king')['rank'])}} | <img src="{{asset(App\User::find($rank_list->get('king')['user']->id)->avatar)}}" style="vertical-align: middle; margin: 0; border: 0; padding: 0; border-radius: 50%" width="35" height="35"> &nbsp;{{$rank_list->get('king')['user']['name']}} | {{number_format($rank_list->get('king')['total_points'])}} | {{number_format($rank_list->get('king')['total_wins'])}} |
{{is_numeric($rank_list->get('queen')['rank']) ? $rank_list->get('queen')['rank'] : ucfirst($rank_list->get('queen')['rank'])}} | <img src="{{asset(App\User::find($rank_list->get('queen')['user']->id)->avatar)}}" style="vertical-align: middle; margin: 0; border: 0; padding: 0; border-radius: 50%" width="35" height="35"> &nbsp;{{$rank_list->get('queen')['user']['name']}} | {{number_format($rank_list->get('queen')['total_points'])}} | {{number_format($rank_list->get('queen')['total_wins'])}} |
{{is_numeric($rank_list->get('jack')['rank']) ? $rank_list->get('jack')['rank'] : ucfirst($rank_list->get('jack')['rank'])}} | <img src="{{asset(App\User::find($rank_list->get('jack')['user']->id)->avatar)}}" style="vertical-align: middle; margin: 0; border: 0; padding: 0; border-radius: 50%" width="35" height="35"> &nbsp;{{$rank_list->get('jack')['user']['name']}} | {{number_format($rank_list->get('jack')['total_points'])}} | {{number_format($rank_list->get('jack')['total_wins'])}} |
@foreach($rank_list->get("others") as $title => $rank_holder)
| {{is_numeric($rank_holder['rank']) ? $rank_holder['rank'] : ucfirst($rank_holder['rank'])}} | <img src="{{asset(App\User::find($rank_holder['user']->id)->avatar)}}" style="vertical-align: middle; margin: 0; border: 0; padding: 0; border-radius: 50%" width="35" height="35"> &nbsp;{{$rank_holder['user']->name}} | {{number_format($rank_holder['total_points'])}} | {{number_format($rank_holder['total_wins'])}} |
@endforeach
@endcomponent        

@if($not_played->count() > 0)
# The below contstants have not predicted any game and are not eligible for ranking:

@foreach($not_played as $player)
1. {{$player->name}}
@endforeach

*The above leaderboard was refreshed after the last game results were updated. Any chnages to points or wins after that will not be reflected in this mail until the next game results are published.*

@endif

To see the current standing on who is the King Predictor, Queen Predictor and Top Predictor, please click on the button below.

@component('mail::button', ['url' => route('leaderboard')])
View Online
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
