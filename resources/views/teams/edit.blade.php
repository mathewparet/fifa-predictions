@extends('layouts.authenticated')
@section('title')
Edit Team {{$team->name}}
@endsection
@section('main')

    <div class="row" id="teamApp">
        <div class="col-md">
            <div class="card mb-3">
                <div class="card-header">Edit Team {{$team->name}}</div>

                <div class="card-body">
                     <form class="form-horizontal" method="POST" action="{{ route('teams.update',['team'=>$team->id]) }}">
                        @csrf
                        {{ method_field('PUT') }}

                        <div class="form-group row">
                            <label for="group" class="col-md-4 col-form-label text-md-right">Group</label>

                            <div class="col-md-6">
                                <input id="group" type="text" class="form-control{{ $errors->has('group') ? ' is-invalid' : '' }}" name="group" value="{{ old('group')?:$team->group }}"  autofocus required>
                                @if ($errors->has('group'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('group') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

                            <div class="col-md-6">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <label class="btn-secondary btn" style="height: 40px;">
                                            <img ref='flagPreview' @if($team->flag) src="{{Storage::url($team->flag)}}" @endif> <i class="fab fa-font-awesome-flag"></i> <input type="file" ref="flagFile" hidden @change.prevent="chooseFlag()" >  
                                            <input type="hidden" ref="flagValue" name="flag">
                                        </label>
                                    </div>
                                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name')?:$team->name }}"  placeholder="Team Name" required style="height: 40px">
                                </div>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                   Update Team
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
           </div>
        </div>
    </div>

@endsection

@section('breadcrumbs', Breadcrumbs::render('teams.edit',$team))
@push('bottomJs')
    <script type="text/javascript">
        Vue.config.devtools = true;
        const teamAppp = new Vue({
            el: '#teamApp'
            , data: {}
            , methods: {
                chooseFlag: function() {
                     //Set the extension for the file 
                     var fileExtension = /image.*/; 
                     //Get the file object 
                     var fileTobeRead = this.$refs.flagFile.files[0];
                     var fileReader = new FileReader();
                     self = this; 
                     fileReader.onload = function (e) 
                     { 
                        if (/^image\/\w+/.test(fileTobeRead.type)) 
                        {
                            $(self.$refs.flagPreview)
                                .attr('src', e.target.result)
                                .width(30)
                                .height(30);
                            $(self.$refs.flagValue).val(e.target.result);
                        }
                        else 
                        {
                            window.alert('Please choose an image file.');
                        }
                     } 
                     fileReader.readAsDataURL(this.$refs.flagFile.files[0]); 
                }
            }
        });
    </script>
@endpush