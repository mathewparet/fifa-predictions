@extends('layouts.authenticated')
@section('title')
Add Teams
@endsection
@section('main')

    <div class="row" id="teamApp">
        <div class="col-md-12">
            <div class="card mb-3">
                <div class="card-header">Add Teams</div>
                <div class="card-body">
                    @include('layouts.partials.status.custom',['panel'=>'teams.create'])
                    <form class="form-horizontal" method="POST" action="{{ route('teams.index') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="group" class="col-md-4 col-form-label text-md-right">Group</label>

                            <div class="col-md-6">
                                <input id="group" type="text" class="form-control{{ $errors->has('group') ? ' is-invalid' : '' }}" name="group" value="{{ old('group', request('group')) }}" placeholder="Group name (1 letter)" autofocus>
                                @if ($errors->has('group'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('group') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row form-group" v-for="(team, index) in teams">
                            <label for="team" class="col-md-4 col-form-label text-md-right">Team @{{index+1}}</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <label class="btn-secondary btn" style="height: 40px;">
                                            <img ref='flagPreview'> <i class="fab fa-font-awesome-flag"></i> <input type="file" ref="flagFile" hidden @change.prevent="chooseFlag(index)" >  
                                            <input type="hidden" ref="flagValue" name="team[flag][]">
                                        </label>
                                    </div>
                                    <input id="name" type="text" class="form-control" style="height: 40px" v-bind:class="{ 'is-invalid': team.nameHasError }" name="team[name][]" v-model="team.name" @keyup.prevent="newTeam()" placeholder="Team Name">
                                </div>
                                <span class="col-md invalid-feedback" style="display: block;">
                                    <strong>@{{ team.nameError }}</strong>
                                </span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Add Team
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
           </div>
        </div>
    </div>

@endsection

@section('breadcrumbs', Breadcrumbs::render('teams.create'))

@push('bottomJs')
    <script type="text/javascript">
        Vue.config.devtools = true;

        const teamApp = new Vue({
            el: '#teamApp'
            , data: {
                teams: [
                    @if(null !== old('team'))
                        @foreach(old('team.name') as $team)
                            {
                                name: "{{null !== old('team.name.'.$loop->index) ? old('team.name.'.$loop->index) : ''}}"
                                , nameHasError: {{$errors->has('team.name.'.$loop->index) ? 'true' : 'false'}}
                                , nameError: "{{$errors->first('team.name.'.$loop->index)}}"
                            }
                            @if(!$loop->last)
                                ,
                            @endif
                        @endforeach
                    @else
                    {
                        name: ''
                        , flag: ''
                        , nameHasError: false
                        , nameError: ''
                    }
                    @endif
                ]
            }
            , methods: {
                newTeam: function() {
                    needNew = true;
                    this.teams.forEach((team) => {
                        if(team.name.length == 0) needNew=false;
                    });

                    if(needNew)
                    {
                        this.teams.push({
                                            name: ''
                                            , flag: ''
                                            , nameHasError: false
                                            , nameError: ''
                                        });
                    }
                }
                , chooseFlag: function(index) {
                     //Set the extension for the file 
                     var fileExtension = /image.*/; 
                     //Get the file object 
                     var fileTobeRead = this.$refs.flagFile[index].files[0];
                     var fileReader = new FileReader();
                     self = this; 
                     fileReader.onload = function (e) 
                     { 
                        if (/^image\/\w+/.test(fileTobeRead.type)) 
                        {
                            $(self.$refs.flagPreview[index])
                                .attr('src', e.target.result)
                                .width(30)
                                .height(30);
                            $(self.$refs.flagValue[index]).val(e.target.result);
                        }
                        else 
                        {
                            window.alert('Please choose an image file.');
                        }
                     } 
                     fileReader.readAsDataURL(this.$refs.flagFile[index].files[0]); 
                }
            }
        });
    </script>
@endpush
