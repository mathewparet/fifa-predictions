@extends('layouts.authenticated')
@section('title')
Teams
@endsection
@section('main')
<div class="container">
    <div class="row">
        <div class="col-md">
            @include('layouts.partials.status.custom',['panel'=>'teams.index'])
        </div>
    </div>
    <div class="row mb-3" id="group-list">
        <div class="col-md">
            <div class="card">
                <div class="card-body">
                    @can('team_manage') [ <a href="{{route('teams.create')}}">New Team</a> ] @endcan[ Groups: @foreach($groups as $group => $teams) <a href="#group-{{$group}}">{{$group}}</a> @if(!$loop->last) | @endif @endforeach ]
                </div>
            </div>
        </div>
    </div>

    @foreach($groups as $group => $teams)
        <div class="row mb-3" id="group-{{$group}}">
            <div class="col-md">
                <div class="card">
                    <div class="card-header">Group: {{$group}}</div>
                    <div class="card-body">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Flag</th>
                                    @can('team_manage')
                                        <th>Actions</th>
                                    @endcan
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($teams as $team)
                                <tr>
                                    <td>{{ $team->name }}</td>
                                    <td><img src="{{Storage::url($team->flag)}}" height="32"></td>
                                    @can('team_manage')
                                        <td>
                                            <div class="btn-group">
                                                <a class="btn btn-sm btn-secondary" href="{{ route('teams.edit',['team'=>$team->id]) }}">
                                                    <span class="fas fa-user-edit"></span>
                                                </a>
                                                <form id="delete-team-{{$team->id}}" action="{{ route('teams.destroy',['team'=>$team->id]) }}" method="POST" style="display:none;">
                                                    @csrf
                                                    {{ method_field('DELETE') }}
                                                </form>
                                                <a class="btn btn-sm btn-danger" href="{{ route('teams.destroy',['team'=>$team->id]) }}" onclick="event.preventDefault(); $('#delete-team-{{$team->id}}').submit();">
                                                    <span class="fas fa-trash-alt"></span>
                                                </a>
                                            </div>
                                        </td>
                                    @endcan
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer">
                        @can('team_manage')[ <a href="{{route('teams.create', ['group'=>$group])}}">Add more to Group {{$group}}</a> ]@endcan 
                        @can('generate', App\Game::class)[ <a href="{{route('games.generate', ['group'=>$group])}}">Generate Games for {{$group}}</a> ]@endcan 
                        [ <a href="#group-list">Top</a> ]
                    </div>
               </div>
            </div>
        </div>
    @endforeach
</div>
@endsection
@section('breadcrumbs', Breadcrumbs::render('teams.index'))
