@extends('layouts.authenticated')
@section('title')
Add Invitations
@endsection
@section('main')

    <div class="row">
        <div class="col-md-12">
            <div class="card mb-3">
                <div class="card-header">Add Invitations</div>
                <div class="card-body">
                    @include('layouts.partials.status.custom',['panel'=>'invitations.create'])
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            There are errors in your submission. Please clear them.
                        </div>
                    @endif
                    <form  id='invitesApp' class="form-horizontal" method="POST" action="{{ route('invitations.index') }}">
                        @csrf
                        <p class="text-muted">If it is just a few email IDs, you can enter the ID in the below text box. As and when you enter it, a new text box will appear for you to enter the next one.</p>
                        <div class="form-group row" v-for="(email, index) in emails">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Email @{{index+1}}</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" v-bind:class="{ 'is-invalid': email.hasError }" name="email[]" v-model="email.email" autofocus placeholder="@if(config('app.fifa.domain_restriction') !== false)Any {{'@'.config('app.fifa.domain_restriction')}} email ID @else Any email ID @endif"  @keyup.prevent="newEmail()">
                                    <span class="invalid-feedback">
                                        <strong>@{{ email.error }}</strong>
                                    </span>
                            </div>
                        </div>
                        <p class="text-muted">Alternatively, if the list is huge, you may choose a plain text file with one email ID per line.</p>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Bulk Upload</label>

                            <div class="col-md-6">
                                <div class="custom-file form-control" v-bind:class="{ 'is-invalid': fileError }">
                                    <input type="file" class="custom-file-input" id="customFile" ref="customFile" @change.prevent="bulkLoad()">
                                    <label class="custom-file-label" for="customFile">Choose a plain text file</label>
                                </div>
                                <span class="form-text text-muted">
                                    Upload a plain text file with one email ID per line
                                </span>
                                <span class="invalid-feedback">
                                    <strong>@{{ fileErrorMessage }}</strong>
                                </span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Invite
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
           </div>
        </div>
    </div>

@endsection

@section('breadcrumbs', Breadcrumbs::render('invitations.create'))
@push('bottomJs')
    <script type="text/javascript">
        Vue.config.devtools = true;
        const invitesApp = new Vue({
            el: '#invitesApp'
            , data: {
                emails: [
                    @if(null !== old('email'))
                        @foreach(old('email') as $e)
                            {
                                email: "{{null !== old('email.'.$loop->index) ? old('email.'.$loop->index) : ''}}"
                                , hasError: {{$errors->has('email.'.$loop->index) ? 'true' : 'false'}}
                                , error: "{{$errors->first('email.'.$loop->index)}}"
                            }
                            @if(!$loop->last)
                                ,
                            @endif
                        @endforeach
                     @else
                        {
                            email: ''
                            , hasError: false
                            , error: ''
                        }
                    @endif
                        ]
                , fileError: false
                , fileErrorMessage: ''
            }
            , methods: {
                newEmail: function() {
                    needNew = true;
                    this.emails.forEach((email) => {
                        if(email.email.length == 0) needNew=false;
                    });

                    if(needNew)
                    {
                        this.emails.push({email: ''});
                    }
                }
                , bulkLoad: function() {
                     //Set the extension for the file 
                     var fileExtension = /text.*/; 
                     //Get the file object 
                     var fileTobeRead = this.$refs.customFile.files[0];
                    //Check of the extension match 
                     if (fileTobeRead.type.match(fileExtension)) 
                     { 
                         invitesApp.fileErrorMessage = '';
                         invitesApp.fileError = false;
                         //Initialize the FileReader object to read the 2file 
                         var fileReader = new FileReader(); 
                         fileReader.onload = function (e) 
                         { 
                             invitesApp.emails.pop();
                             emails = fileReader.result.split("\n");
                             $.each(emails, function(index){
                                invitesApp.emails.push({email: emails[index]});
                             });
                         } 
                         fileReader.readAsText(fileTobeRead); 
                     } 
                     else 
                     { 
                         invitesApp.fileError = true; 
                         invitesApp.fileErrorMessage = 'You can use only a plain text file.'
                     }
                }
            }
        });
    </script>
@endpush