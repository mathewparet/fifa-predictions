@extends('layouts.authenticated')
@section('title')
Manage Invitations
@endsection
@section('main')
<div class="container">
    <div class="row">
        <div class="col-md">
            <div class="card">
                <div class="card-header">Manage Invitations</div>

                <div class="card-body">
                     @include('layouts.partials.status.custom',['panel'=>'invitations.index'])


                     <!-- Let's load the table -->
                     <table class="table table-hover">
                         <thead>
                            <tr>
                                <th>Email</th>
                                <th class="d-none d-md-table-cell">Invitation Code</th>
                                <th class="d-none d-lg-table-cell">Created</th>
                                <th class="hidden-xs">Actions</th>
                            </tr>
                        </thead>
                     	<tbody>
                     		@forelse($invitations as $invite)
                                <tr>
                                    <td title="{{$invite->email}}">{{str_limit($invite->email, 20)}}</td>
                                    <td class="d-none d-md-table-cell">{{$invite->invitation_code}}</td>
                                    <td class="d-none d-lg-table-cell">{{$invite->created_at->diffInDays()>1?$invite->created_at->format(config('app.long_date_format')):$invite->created_at->diffForHumans()}}</td>
                                    <td class="hidden-xs">
                                        <form id="delete-resend-{{$invite->invitation_code}}" action="{{ route('invitations.resend',['invite'=>$invite->email]) }}" method="POST" style="display:none;">
                                            @csrf
                                            {{ method_field('POST') }}
                                        </form>
                                        <form id="delete-invite-{{$invite->invitation_code}}" action="{{ route('invitations.destroy',['invite'=>$invite->email]) }}" method="POST" style="display:none;">
                                            @csrf
                                            {{ method_field('DELETE') }}
                                        </form>
                                        <div class="btn-group">
                                            <a class="btn btn-sm btn-secondary" href="{{ route('invitations.resend',['invite'=>$invite->email]) }}" onclick="event.preventDefault(); $('#delete-resend-{{$invite->invitation_code}}').submit();">
                                                <span class="fas fa-envelope"></span>
                                            </a>
                                            <a class="btn btn-sm btn-danger" href="{{ route('invitations.destroy',['invite'=>$invite->email]) }}" onclick="event.preventDefault(); $('#delete-invite-{{$invite->invitation_code}}').submit();">
                                                <span class="fas fa-trash-alt"></span>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan='6' class="text-center">No Invitations Found</td>
                                </tr>
                            @endforelse
                     	</tbody>
                     </table>
                     <div class="text-center">
                         {{ $invitations->links() }}
                     </div>
                </div>
                <div class="card-footer">
                    <a href="{{route('invitations.create')}}">Invite People</a>
                </div>
           </div>
        </div>
    </div>
</div>
@endsection
@section('breadcrumbs', Breadcrumbs::render('invitations.index'))
