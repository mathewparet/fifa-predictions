@extends('layouts.authenticated')
@section('title', 'Bulk Add Points')
@section('main')

    <div class="row" id="pointBulkAdd">
        <div class="col-md-12">
            <div class="card mb-3">
                <div class="card-header">Bulk Add Points</div>
                <div class="card-body">
                    @include('layouts.partials.status.custom',['panel'=>'points.bulk'])
                    <form novalidate="novalidate" class="form-horizontal" method="POST" action="{{ route('points.bulkStore') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="comments" class="col-md-4 col-form-label text-md-right">Description</label>

                            <div class="col-md-6">
                                <input id="comments" type="text" class="form-control{{ $errors->has('comments') ? ' is-invalid' : '' }}" name="comments" value="{{ old('comments') }}"  autofocus placeholder="Points for..." required>
                                @if ($errors->has('comments'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('comments') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="points" class="col-md-4 col-form-label text-md-right">Points</label>

                            <div class="col-md-6">
                                <div class="input-group">
                                    <input id="points" type="number" class="text-right form-control{{ $errors->has('points') ? ' is-invalid' : '' }}" name="points" value="{{old('points')}}" required min=0 step="1">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><span class="fas fa-coins text-danger">&nbsp;</span></span>
                                    </div>
                                </div>
                                @if ($errors->has('points'))
                                    <span class="invalid-feedback" style="display: block;">
                                        <strong>{{ $errors->first('points') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="userSearch" class="col-md-4 col-form-label text-md-right">Users</label>

                            <div class="col-md-6">
                                <input id="userSearch" type="text" class="form-control" name="userSearch" placeholder="Search for user or group...">
                            </div>
                        </div>

                        <table class="table table-hover">
                            <tbody>
                                <tr v-for="(contestant, index) in contestants">
                                    <td>
                                        <img v-bind:src="contestant.avatar" class="rounded-circle" height="32" width="32">
                                        <input type="hidden" name="contestants[user_id][]" v-model="contestant.user_id">
                                        <input type="hidden" name="contestants[type][]" v-model="contestant.type">
                                        <input type="hidden" name="contestants[role_name][]" v-model="contestant.role_name">
                                        <input type="hidden" name="contestants[avatar][]" v-model="contestant.avatar">
                                        <input type="hidden" name="contestants[label][]" v-model="contestant.label">
                                    </td>
                                    <td>
                                        @{{contestant.label}}
                                        <span v-if="contestant.hasError" class="invalid-feedback" style="display: block;"><br>
                                            @{{contestant.errorText}}
                                        </span>
                                    </td>
                                    <td>
                                        <button class="btn btn-sm btn-danger" @click.prevent="removeContestant(index)"><span class="fas fa-trash-alt"></span></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="confirmation"> I confirm that points have to be added to the above people.
                                    </label>
                                </div>
                                @if ($errors->has('confirmation'))
                                    <span class="invalid-feedback" style="display: block;">
                                        <strong>{{ $errors->first('confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Add Points
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
           </div>
        </div>
    </div>

@endsection

@push('topCss')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />
@endpush

@section('breadcrumbs', Breadcrumbs::render('points.bulkAdd'))
@push('bottomJs')
    <script type="text/javascript">
        Vue.config.devtools = true;
        var pointBulkAdd = new Vue({
            el: '#pointBulkAdd'
            , data: {
                contestants: [
                    @if(null !== old('contestants'))
                        @foreach(old('contestants.type') as $e)
                            {
                                user_id: {{null !== old('contestants.user_id.'.$loop->index) ? old('contestants.user_id.'.$loop->index) : 'null'}}
                                , type: "{{old('contestants.type.'.$loop->index)}}"
                                , role_name: "{{old('contestants.role_name.'.$loop->index)}}"
                                , avatar: "{{old('contestants.avatar.'.$loop->index)}}"
                                , label: "{{old('contestants.label.'.$loop->index)}}"
                                , hasError: @if($errors->has('contestants.user_id.'.$loop->index) || $errors->has('contestants.role_name.'.$loop->index)) true @else false @endif
                                , errorText: "{{$errors->first('contestants.user_id.'.$loop->index)}}{{$errors->first('contestants.role_name.'.$loop->index)}}"
                            }
                            @if(!$loop->last)
                                ,
                            @endif
                        @endforeach
                    @endif
                ]
            }
            , methods: {
                removeContestant: function(index)
                {
                    this.contestants.splice(index, 1);
                }
                , addContestant: function(contestant)
                {
                    this.contestants.push(contestant);
                }
            }
        });
    </script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#userSearch").autocomplete({
                source: '{{route("users.list")}}'
                , minLength: 3
                , select: function(event, ui) {
                    window.pointBulkAdd.addContestant({
                        user_id: ui.item.user_id
                        , avatar: ui.item.avatar
                        , type: ui.item.type
                        , label: ui.item.label
                        , role_name: ui.item.role_name
                    });
                    ui.item.value = null;
                    $("#userSearch").val(null);
                    return false;
                }
            });
        });
    </script>
@endpush
