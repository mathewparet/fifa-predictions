@extends('layouts.authenticated')
@section('title')
Feedback Received
@endsection
@section('main')
<div class="container" id="pointsApp">
    @foreach($feedback as $comment)
        <div class="row">
            <div class="col-md mb-3">
                <div class="card">
                    <div class="card-body">
                        <div class="card-text">
                            <blockquote class="blockquote">
                                <p class="mb-0">{{$comment->comments}}</p>
                                <footer class="blockquote-footer">{{$comment->name}}</footer>
                            </blockquote>
                        </div>
                    </div>
                    <div class="card-footer">
                        Portal: 
                        @component('layouts.components.rating')
                            @slot('rating',$comment->portal_rating)
                        @endcomponent
                         / Initiative: 
                        @component('layouts.components.rating')
                            @slot('rating',$comment->intiative_rating)
                        @endcomponent
                        <span class="float-right">
                            @ {{$comment->created_at->diffInDays()>1?$comment->created_at->format(config('app.long_date_format')):$comment->created_at->diffForHumans()}}
                        </span>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    <div class="row">
        {{$feedback->links()}}
    </div>
</div>
@endsection
@section('breadcrumbs', Breadcrumbs::render('feedback.index'))
