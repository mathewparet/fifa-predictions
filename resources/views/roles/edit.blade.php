@extends('layouts.authenticated')
@section('title')
Edit Role {{$role->label}}
@endsection
@section('main')

    <div class="row">
        <div class="col-md">
            <div class="card mb-3">
                <div class="card-header">Edit Role {{$role->label}}</div>

                <div class="card-body">
                     <form class="form-horizontal" method="POST" action="{{ route('roles.update',['role'=>$role->name]) }}">
                        @csrf
                        {{ method_field('PUT') }}

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name')?:$role->name }}"  autofocus placeholder="sales_manager" required>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="label" class="col-md-4 col-form-label text-md-right">Description</label>

                            <div class="col-md-6">
                                <input id="label" type="text" class="form-control{{ $errors->has('label') ? ' is-invalid' : '' }}" name="label" value="{{ old('label')?:$role->label }}" placeholder="Manage close sales">
                                @if ($errors->has('label'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('label') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <fieldset>
                            <legend>{{__('Roles')}}</legend>
                                <div class="form-group row">
                                    <div class="offset-4 col-md col-form-label text-md-justify">
                                        @foreach($permissions as $permission)
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="permissions[]" value="{{$permission->id}}" @if($role->permissions->contains('id',$permission->id)) checked @endif> {{$permission->label}} ({{$permission->name}})
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                        </fieldset>

                        <div class="form-group row">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                   Update Role
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
           </div>
        </div>
    </div>

@endsection

@section('breadcrumbs', Breadcrumbs::render('roles.edit',$role))
