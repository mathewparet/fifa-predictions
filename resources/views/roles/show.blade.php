@extends('layouts.authenticated')
@section('title')
Role details - {{$role->label}}
@endsection
@section('main')

    <div class="row">
        <div class="col-md-12">
            <div class="card mb-3">
                <div class="card-header">Role details - {{$role->label}}</div>
                <div class="card-body">
                    @include('layouts.partials.status.custom',['panel'=>'roles.show'])
                    <div class="row">
                        <div class="col-md-6">
                            <dl class="dl-horizontal">
                                <dt>Name</dt>
                                <dd>{{ $role->name }}</dd>
                                <dt>Description</dt>
                                <dd>{{$role->label}}</dd>
                            </dl>
                        </div>
                        <div class="col-md-6">
                            <dl class="dl-horizontal">
                                <dt>Created</dt>
                                <dd>{{$role->created_at->diffInDays()>1?$role->created_at->format(config('app.long_date_format')):$role->created_at->diffForHumans()}}</dd>
                                <dt>Last Updated</dt>
                                <dd>{{$role->updated_at->diffInDays()>1?$role->updated_at->format(config('app.long_date_format')):$role->updated_at->diffForHumans()}}</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                @can('role_manage')
                    <div class="card-footer">
                        [ <a href="{{route('roles.edit',['role'=>$role->name])}}">Edit Role / Modify Associated Permissions</a> ] 
                    </div>
                @endcan
           </div>
           <div class="card mb-3">
               <div class="card-header">Associated Permissions</div>
               <div class="card-body">
                   <div class="row">
                        <div class="col-md-12">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Description</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($role->permissions as $permission)
                                        <tr>
                                            <td>{{$permission->name}}</td>
                                            <td>{{$permission->label}}</td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="2" class="text-muted text-center">No permissions assigned</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
               </div>
           </div>
        </div>
    </div>
@endsection
@section('breadcrumbs', Breadcrumbs::render('roles.show',$role))
