@extends('layouts.authenticated')
@section('title')
Manage Roles
@endsection
@section('main')
<div class="container">
    <div class="row">
        <div class="col-md">
            <div class="card">
                <div class="card-header">Manage Roles</div>

                <div class="card-body">
                     @include('layouts.partials.status.custom',['panel'=>'roles.index'])

                     <table class="table table-hover">
                        <thead>
                            <tr>
                                <th class="d-table-cell d-none">Name</th>
                                <th class="d-none d-sm-table-cell">Label</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($roles as $role)
                            <tr>
                                <td class="d-table-cell d-none">{{ $role->name }}</a></td>
                                <td class="d-none d-sm-table-cell">{{ $role->label }}</a></td>
                                <td>
                                    <div class="btn-group">
                                        @can('permission_view')
                                            <a class="btn btn-sm btn-secondary" href="{{ route('roles.show',['role'=>$role->name]) }}">
                                                <span class="fas fa-eye"></span>
                                            </a>
                                        @endcan
                                        @can('role_manage')
                                            <a class="btn btn-sm btn-secondary" href="{{ route('roles.edit',['role'=>$role->name]) }}">
                                                <span class="fas fa-user-edit"></span>
                                            </a>
                                            @if($role->id > 2)
                                                <form id="delete-role-{{$role->id}}" action="{{ route('roles.destroy',['role'=>$role->name]) }}" method="POST" style="display:none;">
                                                    @csrf
                                                    {{ method_field('DELETE') }}
                                                </form>
                                                <a class="btn btn-sm btn-danger" href="{{ route('roles.destroy',['role'=>$role->name]) }}" onclick="event.preventDefault(); $('#delete-role-{{$role->id}}').submit();">
                                                    <span class="fas fa-trash-alt"></span>
                                                </a>
                                            @endif
                                        @endcan
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="text-center">{{ $roles->links() }}</div>
                </div>
                @can('role_manage')
                    <div class="card-footer">
                        <a href="{{route('roles.create')}}">Add Role</a>
                    </div>
                @endcan
           </div>
        </div>
    </div>
</div>
@endsection
@section('breadcrumbs', Breadcrumbs::render('roles.index'))
