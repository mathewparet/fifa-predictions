@extends('layouts.app')
@section('content')
    <div class="container">
        @if(session('orig_user'))
            <div class="row mb-3">
                <div class="col-sm">
                    <div class="bg-warning">
                        You are {{session('orig_user')->name}}, impersonating {{auth()->user()->name}} / <a href="{{route('users.orig')}}">Switch back</a>
                    </div>
                </div>
            </div>
        @endif
        
        <div class="row">
            <div class="col-lg-3 mb-3">
                <!-- side bar -->
                @include('layouts.partials.sidebar')
                @yield('sidebar')
            </div>
            <div class="col-lg">
                <!-- main content -->
                @yield('main')
            </div>
        </div>
    </div>
@endsection
