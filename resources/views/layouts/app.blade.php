<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') - {{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    @stack('topCss')
    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

    @stack('topJs')
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-lg navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{url('/logo.png')}}">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li><a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></li>
                            <li><a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a></li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <img src="{{ Auth::user()->avatar }}" class="rounded-circle" height="32" width="32"><span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown"  style="width:400px;">
                                    <div class="row" style="margin-top: 1em;">
                                        <div class="col-md-3 ml-3 mb-3">
                                            <img src="{{ Auth::user()->avatar }}" class="rounded-circle center-block" height="96" width="96">
                                        </div>
                                        <div class="col-md-7 col-md-offset-1">
                                            <p>
                                                <strong>{{ Auth::user()->name }}</strong><br>
                                                {{ Auth::user()->email }}<br>
                                                <em>{{ __('Registered :date', ['date' => Auth::user()->created_at->diffForHumans()]) }}</em>
                                            </p>
                                        </div>
                                    </div>
                                    <a class="dropdown-item" href="{{route('home')}}">Dashboard</a>
                                    <a class="dropdown-item" href="{{route('users.show', ['user'=> auth::user()->id])}}">My Profile</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            <div class="container">
                <div class="row">
                    <div class="col">
                        @yield('breadcrumbs')
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col">
                        @include('layouts.partials.status.general')
                    </div>
                </div>
            </div>

            @yield('content')
            @if(auth::check())
                @if(isset($_COOKIE['feedbackDone_'.Auth::user()->id]) != "yes" && config('app.feedback.typeform')!==false)
                    <a class="typeform-share button" href="{{config('app.feedback.typeform')}}" data-mode="drawer_right" data-auto-open=true target="_blank"> </a> <script> (function() { var qs,js,q,s,d=document, gi=d.getElementById, ce=d.createElement, gt=d.getElementsByTagName, id="typef_orm_share", b="https://embed.typeform.com/"; if(!gi.call(d,id)){ js=ce.call(d,"script"); js.id=id; js.src=b+"embed.js"; q=gt.call(d,"script")[0]; q.parentNode.insertBefore(js,q) } })() </script>

                    <script type="text/javascript">
                        function setCookieDays(cname, cvalue, exdays) {
                            setCookieMinutes(cname, cvalue, exdays*24*60);
                        }

                        function setCookieMinutes(cname, cvalue, exminutes) {
                            var d = new Date();
                            d.setTime(d.getTime() + (exminutes*60*1000));
                            var expires = "expires="+ d.toUTCString();
                            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
                        }

                        function getCookie(cname) {
                            var name = cname + "=";
                            var decodedCookie = decodeURIComponent(document.cookie);
                            var ca = decodedCookie.split(';');
                            for(var i = 0; i <ca.length; i++) {
                                var c = ca[i];
                                while (c.charAt(0) == ' ') {
                                    c = c.substring(1);
                                }
                                if (c.indexOf(name) == 0) {
                                    return c.substring(name.length, c.length);
                                }
                            }
                            return "";
                        }

                        window.addEventListener('message', function(event){
                            console.log(event.data);
                            if(event.data == 'form-submit')
                                setCookieDays("feedbackDone_{{Auth::user()->id}}","yes",90);
                            if(event.data == "form-close")
                            {
                                if(getCookie("feedbackDone_{{Auth::user()->id}}") != "yes")
                                {
                                    if(confirm("Would you like to be asked for feedback again? Press 'OK' if you would liked to be asked again for feedback in this browser."))
                                        setCookieMinutes("feedbackDone_{{Auth::user()->id}}","yes",30);
                                    else
                                        setCookieDays("feedbackDone_{{Auth::user()->id}}","yes",90);
                                }
                            }
                        }, false);
                    </script>
                @endif
            @endif
        </main>
        <footer>
            <div class="container text-center footer">
                Developed with lots of <i class="far fa-heart"></i> by Mathew Paret. | Rendered in {{ round(microtime(true) - LARAVEL_START,2) }} seconds.
            </div>
        </footer>
    </div>
    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}"></script>
    @stack('bottomJs')
</body>
</html>
