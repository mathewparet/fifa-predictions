@isset($panel)
	@if(!empty(session('status.'.$panel)))
		@foreach(session('status.'.$panel) as $type => $message)
	        @component('layouts.components.alert')
	            @slot('type',$type)
	            {{$message}}
	        @endcomponent
	    @endforeach
	@endif
@endisset