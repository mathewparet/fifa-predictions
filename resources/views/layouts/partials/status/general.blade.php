@if(session('status.general'))
    @foreach(session('status.general') as $type => $message)
        @component('layouts.components.alert')
            @slot('type',$type)
            {{$message}}
        @endcomponent
    @endforeach
@endif