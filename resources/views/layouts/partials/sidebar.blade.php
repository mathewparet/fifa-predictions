<div class="container">
    <div class="row">
        <div class="col-lg">
            <div class="card">
                <div class="navbar navbar-expand-lg navbar-light navbar-laravel  pl-0 pr-0">
                    <div class="container pl-0 pr-0">
                        <div class="card-heading ml-2">
                            <button class="navbar-toggler ml-2" type="button" data-toggle="collapse" data-target="#sidebarNav" aria-controls="sidebarNav" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse" id="sidebarNav">
                            <div class="card-body">
                                <h5 class="card-title">Navigation</h5>
                                <p class="card-text">
                                    <div class="list-group list-group-flush"> 
                                        @if(auth::check())
                                            <a class="list-group-item list-group-item-action @if(str_is('home',Route::getCurrentRoute()->uri)) active @endif" href="{{route('home')}}"><span class="fas fa-home">&nbsp;</span>Dashboard</a>

                                            <a class="list-group-item list-group-item-action @if(str_is('users/{user}',Route::getCurrentRoute()->uri)) active @endif" href="{{route('users.show', ['user'=> auth::user()->id])}}"><span class="fas fa-user-circle">&nbsp;</span>My Profile</a>

                                            <a class="list-group-item list-group-item-action @if(str_is('users/{user}*/points',Route::getCurrentRoute()->uri)) active @endif" href="{{route('users.points.index', ['user'=> auth::user()->id])}}"><span class="fas fa-coins text-warning">&nbsp;</span>Points <span class="badge badge-warning">{{number_format(auth::user()->wallet_points)}}</span></a>

                                            @can('exchange', App\Point::class)
                                                <a class="list-group-item list-group-item-action @if(str_is('users/{user}*/points/buy',Route::getCurrentRoute()->uri)) active @endif" href="{{route('users.points.buy.form', ['user'=> auth::user()->id])}}"><i class="fas fa-exchange-alt"></i> Get more points</a>
                                            @endcan                                            
                                        @endif

                                        @can('view',App\Game::class)
                                            <a class="list-group-item list-group-item-action @if(strpos(Route::getCurrentRoute()->uri,'games')!==false) active @endif" href='{{route("games.index")}}'><span class="fas fa-futbol">&nbsp;</span>Games</a>
                                        @endcan
                                        
                                        <a class="list-group-item list-group-item-action @if(strpos(Route::getCurrentRoute()->uri,'leaderboard')!==false) active @endif" href='{{route("leaderboard")}}'><span class="fas fa-trophy">&nbsp;</span>Leaderboard</a>

                                        @can('user_view')
                                            <a class="list-group-item list-group-item-action @if(str_is('users*',Route::getCurrentRoute()->uri)!==false) active @endif" href='{{route("users.index")}}'><span class="fas fa-users">&nbsp;</span>Users</a>
                                        @endcan

                                        @can('permission_view')
                                            <a class="list-group-item list-group-item-action @if(strpos(Route::getCurrentRoute()->uri,'roles')!==false) active @endif" href='{{route("roles.index")}}'><span class="fab fa-keycdn">&nbsp;</span>Roles</a>
                                        @endcan

                                        @can('invitation_create')
                                            <a class="list-group-item list-group-item-action @if(strpos(Route::getCurrentRoute()->uri,'invitations')!==false) active @endif" href='{{route("invitations.index")}}'><span class="fas fa-key">&nbsp;</span>Invitations</a>
                                        @endcan

                                        @can('team_view')
                                            <a class="list-group-item list-group-item-action @if(strpos(Route::getCurrentRoute()->uri,'teams')!==false) active @endif" href='{{route("teams.index")}}'><span class="fas fa-flag">&nbsp;</span>Teams</a>
                                        @endcan

                                        @can('add', App\Point::class)
                                            <a class="text-danger list-group-item list-group-item-action @if(str_is('points/bulkAdd',Route::getCurrentRoute()->uri)) active @endif" href="{{route('points.bulkAdd')}}"><span class="fas fa-coins">&nbsp;</span>Add Points</a>
                                        @endcan
                                        @if(config('app.feedback.typeform')!==false)
                                            <a class="list-group-item list-group-item-action @if(str_is('feedback',Route::getCurrentRoute()->uri)) active @endif" href="{{route('feedback.index')}}"><i class="fas fa-comments">&nbsp;</i>Feedback</a>
                                        @endif

                                        <a target="__blank" class="list-group-item list-group-item-action @if(strpos(Route::getCurrentRoute()->uri,'rules')!==false) active @endif" href='{{route("rules")}}'><span class="fas fa-hand-point-right">&nbsp;</span>Rules</a>
                                    </div>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>