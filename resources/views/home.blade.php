@extends('layouts.authenticated')
@section('title','Dashboard')
@section('main')
<div class="container" id="pointsApp">
    <div class="row">
        <div class="col-md mb-3">
            <div class="card text-white bg-success">
                <div class="card-header">Total Wins</div>
                <div class="card-body">
                    <h3><span class="fas fa-trophy text-warning">&nbsp;</span> 
                        <animate-number
                          from="0" 
                          to="{{$user->total_wins}}" 
                          duration="1000" 
                          easing="easeOutQuad"
                          :formatter="formatter"
                        >                                                
                        </animate-number>
                    </h3>
                </div>
            </div>
        </div>
        <div class="col-md mb-3">
          <div class="card text-white bg-info">
              <div class="card-header">Overall Rank</div>
              <div class="card-body">
                @if(is_numeric($user->rank))
                  <h3><span class="fas fas fa-crown text-warning">&nbsp;</span>
                      <animate-number
                        from="0" 
                        to="{{$user->rank}}" 
                        duration="1000" 
                        easing="easeOutQuad"
                        :formatter="formatter"
                      >                                                
                      </animate-number>
                  </h3>
                @else
                  <h3><span class="fas fas fa-crown text-warning">&nbsp;</span> {{ucfirst($user->rank)}}
                @endif
              </div>
          </div>
        </div>
        <div class="col-md mb-3 align-items-center">
          <div id="chart_div"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md mb-3">
            <div class="card text-white bg-primary">
                <div class="card-header">Wallet Points</div>
                <div class="card-body">
                    <h3><span class="fas fa-coins text-warning">&nbsp;</span> 
                        <animate-number
                          from="0" 
                          to="{{$user->wallet_points}}" 
                          duration="1000" 
                          easing="easeOutQuad"
                          :formatter="formatter"
                        >                                                
                        </animate-number>
                    </h3>
                </div>
            </div>
        </div>
        <div class="col-md mb-3">
            <div class="card bg-info text-white">
                <div class="card-header">Alloted to Games</div>
                <div class="card-body">
                    <h3><span class="fas fa-coins text-warning">&nbsp;</span> 
                        <animate-number
                          from="0" 
                          to="{{$user->points_in_prediction}}" 
                          duration="1000" 
                          easing="easeOutQuad"
                          :formatter="formatter"
                        >                                                
                        </animate-number>
                    </h3>
                </div>
            </div>
        </div>
        <div class="col-md mb-3">
            <div class="card text-white bg-success">
                <div class="card-header">Total Points</div>
                <div class="card-body">
                    <h3><span class="fas fa-coins text-warning">&nbsp;</span> 
                        <animate-number
                          from="0" 
                          to="{{$user->total_points}}" 
                          duration="1000" 
                          easing="easeOutQuad"
                          :formatter="formatter"
                        >                                                
                        </animate-number>
                    </h3>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md mb-3">
            <div class="card text-white bg-success">
                <div class="card-header">Points Gained</div>
                <div class="card-body">
                    <h3><span class="fas fa-coins text-warning">&nbsp;</span> 
                        <animate-number
                          from="0" 
                          to="{{$user->points()->credit()->sum('points')}}" 
                          duration="1000" 
                          easing="easeOutQuad"
                          :formatter="formatter"
                        >                                                
                        </animate-number>
                    </h3>
                </div>
            </div>
        </div>
        <div class="col-md mb-3">
          <div class="card text-white bg-danger">
              <div class="card-header">Points Lost</div>
              <div class="card-body">
                  <h3><span class="fas fa-coins text-warning">&nbsp;</span>
                      <animate-number
                        from="0" 
                        to="{{$user->points()->debit()->sum('points') - $user->points_in_prediction}}" 
                        duration="1000" 
                        easing="easeOutQuad"
                        :formatter="formatter"
                      >                                                
                      </animate-number>
                  </h3>
              </div>
          </div>
        </div>
        <div class="col-md mb-3">
          <div class="card text-white bg-secondary">
              <div class="card-header">No. of Predictions</div>
              <div class="card-body">
                  <h3><span class="fas fa-futbol">&nbsp;</span>
                      <animate-number
                        from="0" 
                        to="{{$user->predictions->count()}}" 
                        duration="1000" 
                        easing="easeOutQuad"
                        :formatter="formatter"
                      >                                                
                      </animate-number>
                  </h3>
              </div>
          </div>
        </div>
        @can('point_exchange')
          <div class="col-md mb-3">
            <div class="card text-white bg-warning">
                <div class="card-header">Wins Exchanged</div>
                <div class="card-body">
                    <h3><span class="fas fa-trophy">&nbsp;</span>
                        <animate-number
                          from="0" 
                          to="{{$user->wins_exchanged}}" 
                          duration="1000" 
                          easing="easeOutQuad"
                          :formatter="formatter"
                        >                                                
                        </animate-number>
                    </h3>
                </div>
            </div>
          </div>
        @endcan
    </div>
</div>
@endsection
@section('breadcrumbs', Breadcrumbs::render('dashboard'))
@push('bottomJs')
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
   <script type="text/javascript">
      google.charts.load('current', {'packages':['gauge']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Label', 'Value'],
          ['Form', 0]
        ]);

        var options = {
          width: 400, height: 120,
          greenFrom: 67, greenTo: 100,
          redFrom: 0, redTo: 33,
          yellowFrom:34, yellowTo: 66,
          minorTicks: 5
        };

        var chart = new google.visualization.Gauge(document.getElementById('chart_div'));

        chart.draw(data, options);

        setInterval(function() {
          data.setValue(0, 1, {{$user->prediction_form}});
          chart.draw(data, options);
        }, 100);
      }
    </script>
  </head>
@endpush