@extends('layouts.app')
@section('title','Register new account')
@section('content')
<div class="container" id="userRegistrationApp">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form novalidate="novalidate" method="POST" action="{{ route('register') }}" id="registration_form">
                        @csrf
                        @if($errors->has('g-recaptcha-response'))
                            <div class="alert alert-danger">
                                {{ $errors->first('g-recaptcha-response') }}
                            </div>
                        @endif
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{old('name')}}" required autofocus placeholder="E.g. John Doe">

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <!-- <label for="name" class="col-md-4 col-form-label text-md-right">Gender</label> -->

                            <div class="col-md-6 offset-md-4">
                                <span class="{{ $errors->has('email') ? ' is-invalid' : '' }}">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input required class="form-check-input" type="radio" name="gender" id="genderMale" value="male">
                                            <span class="fas fa-male"></span> Male
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input required class="form-check-input" type="radio" name="gender" id="genderFemale" value="female" >
                                            <span class="fas fa-female"></span> Female
                                        </label>
                                    </div>
                                </span>
                                @if ($errors->has('gender'))
                                    <span class="invalid-feedback" style="display: block;">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">

                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{old('email',request('email'))}}" required placeholder="@if(config('app.fifa.domain_restriction') !== false) {{'john.doe@'.config('app.fifa.domain_restriction')}} @else john.doe@example.com @endif">
                                @if(config('app.fifa.domain_restriction') !== false)
                                    <span class="form-text text-muted">Only {{'@'.config('app.fifa.domain_restriction')}} email IDs are accepted</span>
                                @endif
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="invitation_code" class="col-md-4 col-form-label text-md-right">{{ __('Invitation Code') }}</label>

                            <div class="col-md-6">
                                <input id="invitation_code" type="text" class="form-control{{ $errors->has('invitation_code') ? ' is-invalid' : '' }}" name="invitation_code" value="{{old('invitation_code', request('invitation_code'))}}" required>
                                @if ($errors->has('invitation_code'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('invitation_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class=" btn btn-primary g-recaptcha"
                                    data-sitekey="{{config('app.recaptcha.site_key')}}"
                                    data-callback="onSubmit">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@if(App::environment('production'))
    @push('topJs')
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <script>
            function onSubmit(token) {
                document.getElementById("registration_form").submit();
            }
        </script>
    @endpush
@endif
@section('breadcrumbs', Breadcrumbs::render('register'))