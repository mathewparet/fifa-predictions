<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Invitation;

class GenerateInvitationCodeTest extends TestCase
{
	use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGenerateInvitationCodeIsGenerated()
    {
        $invite = Invitation::create(['email'=>'mathews.paret@accenture.com']);
        $this->assertDatabaseHas('invitations',['email'=>'mathews.paret@accenture.com']);
        $this->assertNotEmpty($invite->invitation_code);
    }

    public function testGenerateInvitationCodeValidatesIfCorrect()
    {
        $invite = Invitation::find('mathew.paret@accenture.com');
        $this->assertFalse($invite->isValid($invite->invitation_code.'s'));
        $this->assertTrue($invite->isValid($invite->invitation_code));
    }
}
