<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;


class UserRegistrationTest extends TestCase
{
	use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testLoadRegistrationPage()
    {
        $response = $this->get(route('register'));

        $response->assertSuccessful()
        	->assertSeeText('Register');
    }

}
