<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\User;
use App\Point;

class PointsTest extends TestCase
{
	use RefreshDatabase;

	private $user;
	private $point;

	public function setUp()
	{
		parent::setUp();
		$this->user = factory(User::class)->create();
		$this->point = factory(Point::class)->make();
	}
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCredit()
    {
    	$this->assertTrue($this->user->addPoints($this->point)->multiplier == 1);
    }

    public function testDebit()
    {
    	$this->assertTrue($this->user->deductPoints($this->point)->multiplier == -1);
    }

    public function testTxTypeCredit()
    {
    	$point = $this->user->addPoints($this->point);
    	$this->assertTrue($point->transaction_type == 'Credit');
    }

    public function testTxTypeDebit()
    {
    	$point = $this->user->deductPoints($this->point);
    	$this->assertTrue($point->transaction_type == 'Debit');
    }
}
