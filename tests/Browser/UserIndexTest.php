<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use Tests\Browser\Pages\UserIndexPage;

use App\User;

class UserIndexTest extends DuskTestCase
{
    use DatabaseMigrations;

    private $user;
    /**
     * A Dusk test example.
     *
     * @return void
     */

    public function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->create(['email_key'=>"Ssss"]);
    }

    public function testEmailIsTrimmed()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->user)
                    ->visit(new UserIndexPage($this->user))
                    ->assertSee(str_limit($this->user->email,20));
            ;
        });
    }

    public function testNameIsDisplayed()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->user)
                    ->visit(new UserIndexPage)
                    ->assertSee($this->user->name);
            ;
        });
    }

    public function testActiveSuspendedBadge()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->user)
                    ->visit(new UserIndexPage)
                    ->assertSee('Active');

            $this->user->suspended = true;
            $this->user->save();

            $browser->loginAs($this->user)
                    ->visit(new UserIndexPage)
                    ->assertSee('Suspended');
            ;
        });
    }

    public function testVerifiedUnverifiedBadge()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->user)
                    ->visit(new UserIndexPage)
                    ->assertSee('Pending');

            $this->user->verify();

            $browser->loginAs($this->user)
                    ->visit(new UserIndexPage)
                    ->assertSee('Verified');
            ;
        });
    }
}
