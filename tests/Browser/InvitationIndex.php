<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\InvitationIndexPage;

use App\User;
use App\Invitation;

class InvitationIndex extends DuskTestCase
{
    use DatabaseMigrations;

    private $user;
    private $invite;
    /**
     * A Dusk test example.
     *
     * @return void
     */

    public function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
        $this->invite = factory(Invitation::class)->create();
    }

    public function testEmailIsTrimmed()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->user)
                    ->visit(new InvitationIndexPage())
                    ->assertSee(str_limit($this->invite->email,20));
            ;
        });
    }

    public function testInvitationCodeIsShown()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->user)
                    ->visit(new InvitationIndexPage())
                    ->assertSee($this->invite->invitation_code);
            ;
        });
    }
}
