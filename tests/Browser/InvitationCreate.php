<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use Tests\Browser\Pages\InvitationCreatePage;
use App\Invitation;
use App\User;

class InvitationCreate extends DuskTestCase
{
    use DatabaseMigrations;

    protected $invitation;
    protected $user;

    public function setUp()
    {
        parent::setUp();
        $this->invitation = factory(Invitation::class)->make();
        $this->user = factory(User::class)->create();
    }

    /**
     * @group fields
    */
    public function testEmailFieldHasValidEmail()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->user)
                    ->visit(new InvitationCreatePage)
                    ->type('email[]',$this->user->name)
                    ->press('Invite')
                    ->assertSelfUrl()
                    ->assertSee('The email.0 must be a valid email address')
                    ->type('email[]',$this->user->email)
                    ->press('Invite')
                    ->assertSelfUrl()
                    ->assertDontSee('The email.0 must be a valid email address')
                    ;
        });
    }

    public function testEmailFieldMustBeAccenture()
    {
        $newUser = factory(User::class)->make(['email'=>'example@accenture.com']);
        $this->browse(function (Browser $browser) use($newUser) {
            $browser->loginAs($this->user)
                    ->visit(new InvitationCreatePage)
                    ->type('email[]',$this->invitation->email)
                    ->press('Invite')
                    ->assertSelfUrl()
                    ->assertSee('Email should be @accenture.com')
                    ->type('email[]',$newUser->email)
                    ->press('Invite')
                    ->assertRouteIs('invitations.index')
                    ->assertDontSee('Email should be @accenture.com')
                    ;
        });
    }


    public function testEmailCannotBeExistingUser()
    {
        $newUser = factory(User::class)->make();
        $this->browse(function (Browser $browser) use($newUser) {
            $browser->loginAs($this->user)
                    ->visit(new InvitationCreatePage)
                    ->type('email[]',$this->user->email)
                    ->press('Invite')
                    ->assertSelfUrl()
                    ->assertSee('The email.0 has already been taken')
                    ->type('email[]',$newUser->email)
                    ->press('Invite')
                    ->assertSelfUrl()
                    ->assertDontSee('The email.0 has already been taken')
                    ;
        });
    }

    public function testEmailCannotBeExistingInvite()
    {
        $this->invitation->save();
        $newUser = factory(User::class)->make();
        $this->browse(function (Browser $browser) use($newUser) {
            $browser->loginAs($this->user)
                    ->visit(new InvitationCreatePage)
                    ->type('email[]',$this->invitation->email)
                    ->press('Invite')
                    ->assertSelfUrl()
                    ->assertSee('The email.0 has already been taken')
                    ->type('email[]',$newUser->email)
                    ->press('Invite')
                    ->assertSelfUrl()
                    ->assertDontSee('The email.0 has already been taken')
                    ;
        });
    }

    public function testBulkUpload()
    {
        $this->browse(function (Browser $browser){
            $browser->loginAs($this->user)
                    ->visit(new InvitationCreatePage)
                    ->attach('#customFile',__DIR__."/files/emails.txt")
                    ->press('Invite')
                    ->assertRouteIs('invitations.index')
                    ->assertSee('4 invitations successfully generated')
                    ;
        });
    }
}
