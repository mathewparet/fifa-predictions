<?php

namespace Tests\Browser;

use App\User;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\UserProfilePage;

class UserProfile extends DuskTestCase
{
    use DatabaseMigrations;
    
    protected $user;

    public function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
    }

    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testProfile()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->user)
                ->visit(new UserProfilePage($this->user))
                ->assertSee($this->user->name)
            ;
        });
    }

    public function testEmailIsTruncated()
    {
        $this->browse(function(Browser $browser) {
            $browser->loginAs($this->user)
                ->visit(new UserProfilePage($this->user))
                ->assertSee(str_limit($this->user->email, 20));
        });
    }

    public function testGender()
    {
        $this->user->gender = 'male';
        $this->user->save();
        
        $this->browse(function(Browser $browser) {
            $browser->loginAs($this->user)
                ->visit(new UserProfilePage($this->user))
                ->assertSee($this->user->gender);

        });

        $this->user->gender = 'female';
        $this->user->save();
        
        $this->browse(function(Browser $browser) {
            $browser->loginAs($this->user)
                ->visit(new UserProfilePage($this->user))
                ->assertSee($this->user->gender);

        });
    }
}
