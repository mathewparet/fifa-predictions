<?php

namespace Tests\Browser;

use Tests\DuskTestCase;

use App\User;
use App\Role;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use Tests\Browser\Pages\UserRegistrationPage;

use App\Invitation;

class NewUserRegisterTest extends DuskTestCase
{
    use DatabaseMigrations;

    protected $user;

    public function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->make();
    }

    public function testInvitationIsRequired()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit(new UserRegistrationPage)
                    ->press('Register')
                    ->assertRouteIs('register')
                    ->assertSee('The invitation code field is required')
                    ->type('invitation_code',$this->user->name)
                    ->press('Register')
                    ->assertRouteIs('register')
                    ->assertDontSee('The invitation code field is required')
                    ;
        });
    }

    public function testInvitationIsValid()
    {
        $newUser = factory(User::class)->make(['email'=>'demo@accenture.com']);
        $invite = Invitation::create(['email'=>$newUser->email]);
        $badInvite = Invitation::create(['email'=>$this->user->email]);

        $this->browse(function (Browser $browser) use($invite, $newUser, $badInvite) {
            $browser->visit(new UserRegistrationPage)
                    ->press('Register')
                    ->type('invitation_code',$newUser->name)
                    ->press('Register')
                    ->assertRouteIs('register')
                    ->assertSee('Invalid invitation code')
                    ->type('email', $newUser->email)
                    ->type('invitation_code',$badInvite->invitation_code)
                    ->press('Register')
                    ->assertRouteIs('register')
                    ->assertSee('Invalid invitation code')
                    ->type('email', $newUser->email)
                    ->type('invitation_code',$invite->invitation_code)
                    ->press('Register')
                    ->assertRouteIs('register')
                    ->assertDontSee('Invalid invitation code')
                    ;
        });
    }

    /**
     * @group fields
    */
    public function testNameFieldIsRequired()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit(new UserRegistrationPage)
                    ->press('Register')
                    ->assertRouteIs('register')
                    ->assertSee('The name field is required')
                    ->type('name',$this->user->name)
                    ->press('Register')
                    ->assertRouteIs('register')
                    ->assertDontSee('The name field is required')
                    ;
        });
    }

    public function testGenderIsRequired()
    {
        $this->browse(function (Browser $browser) {
                    $browser->visit(new UserRegistrationPage)
                            ->press('Register')
                            ->assertRouteIs('register')
                            ->assertSee('The gender field is required')
                            ->radio('gender','male')
                            ->press('Register')
                            ->assertRouteIs('register')
                            ->assertDontSee('The gender field is required')
                            ;
                });
    }

    /**
     * @group fields
    */
    public function testEmailFieldIsRequired()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit(new UserRegistrationPage)
                    ->press('Register')
                    ->assertRouteIs('register')
                    ->assertSee('The email field is required')
                    ->type('email',$this->user->email)
                    ->press('Register')
                    ->assertRouteIs('register')
                    ->assertDontSee('The email field is required')
                    ;
        });
    }

    public function testEmailFieldHasValidEmail()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit(new UserRegistrationPage)
                    ->type('email',$this->user->name)
                    ->press('Register')
                    ->assertRouteIs('register')
                    ->assertSee('The email must be a valid email address')
                    ->type('email',$this->user->email)
                    ->press('Register')
                    ->assertRouteIs('register')
                    ->assertDontSee('The email must be a valid email address')
                    ;
        });
    }

    public function testEmailFieldMustBeAccenture()
    {
        $newUser = factory(User::class)->make(['email'=>'example@accenture.com']);
        $this->browse(function (Browser $browser) use($newUser) {
            $browser->visit(new UserRegistrationPage)
                    ->type('email',$this->user->email)
                    ->press('Register')
                    ->assertRouteIs('register')
                    ->assertSee('Email should be @accenture.com')
                    ->type('email',$newUser->email)
                    ->press('Register')
                    ->assertRouteIs('register')
                    ->assertDontSee('Email should be @accenture.com')
                    ;
        });
    }

    public function testEmailFieldHasNewEmail()
    {
        $this->user->save();
        $newUser = factory(User::class)->make();
        $this->browse(function (Browser $browser) use($newUser) {
            $browser->visit(new UserRegistrationPage)
                    ->type('email',$this->user->email)
                    ->press('Register')
                    ->assertRouteIs('register')
                    ->assertSee('The email has already been taken')
                    ->type('email',$newUser->email)
                    ->press('Register')
                    ->assertRouteIs('register')
                    ->assertDontSee('The email has already been taken')
                    ;
        });
    }


    /**
     * @group fields
    */
    public function testPasswordFieldIsRequired()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit(new UserRegistrationPage)
                    ->press('Register')
                    ->assertRouteIs('register')
                    ->assertSee('The password field is required')
                    ->type('password','test')
                    ->press('Register')
                    ->assertDontSee('The password field is required')
                    ;
        });
    }

    public function testPasswordFieldHasAtLeast6Characters()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit(new UserRegistrationPage)
                    ->type('password','test')
                    ->press('Register')
                    ->assertSee('The password must be at least 6 characters')
                    ->type('password','secret')
                    ->press('Register')
                    ->assertDontSee('The password must be at least 6 characters')
                    ;
        });
    }

    public function testPasswordFieldMatchesConfirmation()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit(new UserRegistrationPage)
                    ->type('password','secret123')
                    ->press('Register')
                    ->assertSee('The password confirmation does not match')
                    ->type('password','secret123')
                    ->type('password_confirmation','secret')
                    ->press('Register')
                    ->assertSee('The password confirmation does not match')
                    ->type('password','secret123')
                    ->type('password_confirmation','secret123')
                    ->press('Register')
                    ->assertDontSee('The password confirmation does not match')
                    ;
        });
    }

    /**
     * @group otp
    */
    public function testUserRegistrationActiveInvitationCode()
    {
        $this->user->email = "example@accenture.com";

        $this->browse(function (Browser $browser) {
            $browser->visit(new UserRegistrationPage)
                    ->type('name',$this->user->name)
                    ->type('email', $this->user->email)
                    ->type('invitation_code',Invitation::create(['email'=>$this->user->email])->invitation_code)
                    ->type('password','secret')
                    ->radio('gender', $this->user->gender)
                    ->type('password_confirmation','secret')
                    ->press('Register')
                    ->assertPathIs('/login');

            $this->assertDatabaseHas('users', ['name'=>$this->user->name, 'email'=>$this->user->email]);

            $user = User::whereEmail($this->user->email)->first();

            $browser->visit(route('users.verify',['user'=>$user->id, 'email_key'=>$user->email_key]))
                ->assertRouteIs('login')
                ->assertSee('Email address successfully verified');

            $this->assertDatabaseMissing('users', ['name'=>$this->user->name, 'email'=>$this->user->email, 'email_key'=>$user->email_key]);

            $this->assertTrue(User::whereEmail($this->user->email)->first()->roles->contains('name', 'customer'));

        });

        
    }

}
