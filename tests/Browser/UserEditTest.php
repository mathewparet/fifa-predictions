<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Hash;

use App\User;
use App\Role;
use App\Permission;

use Tests\Browser\Pages\UserEditPage;

class UserEditTest extends DuskTestCase
{
    use DatabaseMigrations;

    protected $current_user;
    protected $super_user;
    protected $temp_user;

    public function setUp()
    {
        parent::setUp();
        $this->super_user = factory(User::class)->create(['password'=>Hash::make('super_secret')]);
        $this->current_user = factory(User::class)->create();
        $this->temp_mem_user = factory(User::class)->make();
    }

    public function testOnlyLoggedInUserCanAccessPage()
    {        
        $this->browse(function (Browser $browser) {
            $browser->visit(route('users.edit',['user'=>$this->current_user->id]))
                    ->assertRouteIs('login');
        });
    }

    public function testOnlyOwnUserCanAccessPage()
    {
        $this->temp_mem_user->save();

        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->temp_mem_user)
                    ->visit(new UserEditPage($this->current_user))
                    ->assertSee('This action is unauthorized')
                    ->loginAs($this->current_user)
                    ->visit(new UserEditPage($this->current_user))
                    ->assertSelfUrl();
        });
    }

    public function testSuperAdminCanAccessPage()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->super_user)
                    ->visit(new UserEditPage($this->current_user))
                    ->assertSelfUrl();
        }); 
    }

    public function testUserWithUser_ViewCanAccessPage()
    {
        $this->temp_mem_user->save();
        $role = Role::create(['name'=>'user_view','label'=>'user_view']);
        $permission = Permission::whereName('user_view')->first()->id;
        $role->givePermission([$permission]);
        $this->temp_mem_user->assignRole($role);

        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->temp_mem_user)
                    ->visit(new UserEditPage($this->current_user))
                    ->assertSelfUrl();
        }); 
    }

    public function testFormNameFieldIsRequired()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->current_user)
                    ->visit(new UserEditPage($this->current_user))
                    ->type('name','')
                    ->press('Update Profile')
                    ->assertSelfUrl()
                    ->assertSee('The name field is required')
                    ->type('name',$this->current_user->name)
                    ->press('Update Profile')
                    ->assertSelfUrl()
                    ->assertDontSee('The name field is required')
                    ;
        });
    }

    
    public function testCurrentPasswordFieldIsRequired()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->current_user)
                    ->visit(new UserEditPage($this->current_user))
                    ->press('Update Profile')
                    ->assertSelfUrl()
                    ->assertSee('The current password field is required')
                    ->type('current_password','secret1')
                    ->press('Update Profile')
                    ->assertSelfUrl()
                    ->assertDontSee('The current password field is required')
                    ;
        });
    }

    public function testCurrentPasswordFieldIsCorrect()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->current_user)
                    ->visit(new UserEditPage($this->current_user))
                    ->type('current_password','secret1')
                    ->press('Update Profile')
                    ->assertSelfUrl()
                    ->assertSee('Current password supplied is not correct')
                    ->type('current_password','secret')
                    ->press('Update Profile')
                    ->assertRouteIs('users.show',['user'=>$this->current_user->id])
                    ->assertSee('User profile Successfully updated')
                    ;
        });
    }

    public function testNewPasswordFieldIsOptional()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->current_user)
                    ->visit(new UserEditPage($this->current_user))
                    ->press('Update Profile')
                    ->assertSelfUrl()
                    ->assertDontSee('The password must be at least 6 characters.')
                    ;
        });
    }

    public function testNewPasswordMustBeAtLeast6Characters()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->current_user)
                    ->visit(new UserEditPage($this->current_user))
                    ->type('password','abcd')
                    ->press('Update Profile')
                    ->assertSelfUrl()
                    ->assertSee('The password must be at least 6 characters.')
                    ->assertMissing('password_confirmation')
                    ->type('password','secret')
                    ->press('Update Profile')
                    ->assertSelfUrl()
                    ->assertDontSee('The password must be at least 6 characters.')
                    ;
        });
    }

    public function testNewPasswordMustMatchCofirmation()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->current_user)
                    ->visit(new UserEditPage($this->current_user))
                    ->type('password','secret')
                    ->press('Update Profile')
                    ->assertSelfUrl()
                    ->assertSee('The password confirmation does not match')
                    ->type('password','secret')
                    ->assertMissing('password_confirmation')
                    ->type('password_confirmation','secret')
                    ->press('Update Profile')
                    ->assertSelfUrl()
                    ->assertDontSee('The password confirmation does not match')
                    ;
        });
    }

    public function testConfirmPasswordIsAvailableOnlyOnPasswordChange()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->current_user)
                    ->visit(new UserEditPage($this->current_user))
                    ->assertMissing('#password_confirmation')
                    ->type('password','abcd')
                    ->assertVisible('#password_confirmation')
                    ;
        });
    }

    public function testUserDeletePasswordFieldIsMandatory()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->super_user)
                    ->visit(new UserEditPage($this->current_user))
                    ->press('Delete Account')
                    ->assertSelfUrl()
                    ->assertSee('The delete password field is required')
                    ->type('delete_password','abcd')
                    ->press('Delete Account')
                    ->assertSelfUrl()
                    ->assertDontSee('The delete password field is required')
                    ;
        });
    }

    public function testUserDeletePasswordFieldIsCorrect()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->super_user)
                    ->visit(new UserEditPage($this->current_user))
                    ->type('delete_password','abcd')
                    ->press('Delete Account')
                    ->assertSelfUrl()
                    ->assertSee('Current password supplied is not correct')
                    ->type('delete_password','super_secret')
                    ->press('Delete Account')
                    ->assertRouteIs('home')
                    ;
        });
    }

    public function testSuperUserCantDeleteOwnAccount()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->super_user)
                    ->visit(new UserEditPage($this->super_user))
                    ->assertSelfUrl()
                    ->type('delete_password','super_secret')
                    ->press('Delete Account')
                    ->assertSelfUrl()
                    ->assertSee('Cannot delete super admin account: '.$this->super_user->email)
                    ;
        });
    }

    public function testUserCanDeleteOnlyOwnAccount()
    {
        $this->temp_mem_user->save();
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->current_user)
                    ->visit(new UserEditPage($this->temp_mem_user))
                    ->assertSee('This action is unauthorized')
                    ;
        });
    }

    public function testSuperUserCanDeleteAnyAccount()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->super_user)
                    ->visit(new UserEditPage($this->current_user))
                    ->type('delete_password','super_secret')
                    ->press('Delete Account')
                    ->assertRouteIs('home')
                    ->assertSee('Successfully deleted account: '.$this->current_user->email)
                    ;
        });
    }

    public function testUserCantVerifySelf()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->current_user)
                    ->visit(new UserEditPage($this->current_user))
                    ->assertDontSee('Verified');
        });
    }

    public function testUserCantSuspendSelf()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->current_user)
                    ->visit(new UserEditPage($this->current_user))
                    ->assertDontSee('Suspended');
        });
    }

    public function testVerifyUser()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->current_user)
                    ->visit(new UserEditPage($this->current_user))
                    ->assertDontSee('Verified');

            $browser->loginAs($this->super_user)
                    ->visit(new UserEditPage($this->current_user))
                    ->assertSee('Verified')
                    ->check('verified')
                    ->type('current_password','super_secret')
                    ->press('Update Profile')
                    ->assertRouteIs('users.show',['user'=>$this->current_user->id])
                    ->visit(new UserEditPage($this->current_user))
                    ->assertChecked('verified')
                    ->assertDisabled('verified')
                    ;
        });
    }

    public function testSuspendUser()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->current_user)
                    ->visit(new UserEditPage($this->current_user))
                    ->assertDontSee('Suspended');

            $browser->loginAs($this->super_user)
                    ->visit(new UserEditPage($this->current_user))
                    ->assertSee('Suspended')
                    ->check('suspended')
                    ->type('current_password','super_secret')
                    ->press('Update Profile')
                    ->assertRouteIs('users.show',['user'=>$this->current_user->id])
                    ->visit(new UserEditPage($this->current_user))
                    ->assertChecked('suspended')
                    ;
        });
    }

    public function testUnSuspendUser()
        {
            $this->browse(function (Browser $browser) {
                $browser->loginAs($this->current_user)
                        ->visit(new UserEditPage($this->current_user))
                        ->assertDontSee('Suspended');

                $browser->loginAs($this->super_user)
                        ->visit(new UserEditPage($this->current_user))
                        ->assertSee('Suspended')
                        ->uncheck('suspended')
                        ->type('current_password','super_secret')
                        ->press('Update Profile')
                        ->assertRouteIs('users.show',['user'=>$this->current_user->id])
                        ->visit(new UserEditPage($this->current_user))
                        ->assertNotChecked('suspended')
                        ;
            });
        }


    public function testChangeUserGroup()
    {
        $this->browse(function(Browser $browser) {
            $browser->loginAs($this->super_user)
                    ->visit(new UserEditPage($this->current_user))
                    ->check("input[name='role[]'][value='2']")
                    ->type('current_password','super_secret')
                    ->press('Update Profile')
                    ->assertRouteIs('users.show',['user'=>$this->current_user->id])
                    ->assertSee('Staff Readonly Access')
                    ->visit(new UserEditPage($this->current_user))
                    ->assertChecked("input[name='role[]'][value='2']")
                    ->uncheck("input[name='role[]'][value='2']")
                    ->type('current_password','super_secret')
                    ->press('Update Profile')
                    ->assertRouteIs('users.show',['user'=>$this->current_user->id])
                    ->assertDontSee('Staff Readonly Access')
                    ;
        });
    }
}
